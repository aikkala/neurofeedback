function ratios = compare_sri_alertness(sessions_path, condition_query, last_session_only)
% Return ratio of alert epochs to all epochs -- as classified by Sri's
% drowsiness algorithm

% TODO 
% - handle missing conditions / poor queries that result in too many
% conditions returned

addpath(genpath('/home/aleksi/Workspace/software/eeglab14_1_2b'));
addpath('/home/aleksi/Workspace/software/fieldtrip-20181116');
micro_meas_path = '/home/aleksi/Workspace/software/microMeasAlertness_HumanEEG';
addpath(genpath(micro_meas_path));

% Get sessions
sessions = dir(sessions_path);
sessions = string({sessions.name}');
sessions(1:2) = [];

% Loop through sessions
if nargin == 3 && last_session_only
    sess_idxs = numel(sessions):numel(sessions);
else
    sess_idxs = 1:numel(sessions);
end

ratios = nan(numel(sessions),1);
for sess_idx = sess_idxs
    
    % Find queried condition
    current_session = fullfile(sessions_path, sessions(sess_idx));
    condition = find_condition(current_session, condition_query{:});
    
    % Skip if condition wasn't found
    if isempty(condition)
        warning('Condition [%s] wasn''t found from path [%s]', strjoin(condition_query, ' / '), sessions_path);
        continue;
    end

    % Use Sri's algorithm
    epochs = micro_measure_alertness(fullfile(current_session, condition), micro_meas_path);
    
    % Calculate ratio of alert epochs to all epochs
    if isstruct(epochs)
        nepochs_drowsy = numel(epochs.drowsymild) + numel(epochs.drowsysevere);
        ratios(sess_idx) =  numel(epochs.alert) / (nepochs_drowsy + numel(epochs.alert));
    end
end

ratios = ratios(sess_idxs);

end