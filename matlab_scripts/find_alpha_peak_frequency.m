function out = find_alpha_peak_frequency(data, fs, showplots)

% Filter first
data = butter_filter(data, fs, 'bandpass', [0.5,45]);

% Calculate PSD
[pxx, f] = pwelch(data, 4*fs, [], [], fs);

% Grab some data points on which we fit a 1/f curve
[~, idx_1] = closest(f,1);
[~, idx_40] = closest(f,40);
xi = f(idx_1:idx_40);

pxx = mean(pxx,2);

% Do each channel separately
out = nan(size(pxx,2),2);
for channel_idx = 1:size(pxx,2)
    
    px = pxx(idx_1:idx_40, channel_idx);

    % Fit the 1/f curve
    lin_fit = fit(xi, log(px), 'poly1', 'robust', 'bisquare');
    
    
    f_fit = exp(f*lin_fit.p1 + lin_fit.p2);

    f_sub = pxx(:, channel_idx) - f_fit;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if showplots
        % Plot frequency fit for presentation.
        figure()
        set(gcf,'color','white','position',[897   795   615   218])
    %         freq = linspace(4,45,length(Finterp));
        plot(f,pxx(:,channel_idx),'b')
        xlabel('Frequency')
        ylabel('Power')
        hold on
        plot(f,f_fit,'m')
        hold off
        figure()
        set(gcf,'color','white','position',[897   795   615   218])
        plot(f,f_sub,'b')
        xlabel('Frequency')
        ylabel('Power')
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Fit a few models with different starting values, choose the best one
    start_points = [8, 9, 10, 11, 12];
    mdls = cell(1, length(start_points));
    adjRsquared = zeros(1, length(start_points));
    % Use only range 5-15Hz
    [~, x8] = closest(f, 8);
    [~, x12] = closest(f,12);


    for idx = 1:length(start_points)

        mdl = fit(f, f_sub, 'gauss1', 'Lower', [0, 7, 0.05], 'Upper', [Inf, 13, 3], 'StartPoint', [max(f_sub(x8:x12)), start_points(idx), 0.5]);
    %         [mdl, gof] = fit(xi', Finterp', 'gauss1', 'Lower', [0, 7, 0.01], 'Upper', [Inf, 13, 3], 'StartPoint', [max(Finterp), start_points(idx), 0.5]);
        mdls{idx} = mdl;

        % Evaluate the model at points in xi
        xp = feval(mdl,f);

        % Calculate the range of peak (mean +- 3*std)
        low = mdl.b1 - 2*mdl.c1;
        [~, low_idx] = closest(f, low);
        hi = mdl.b1 + 2*mdl.c1;
        [~, hi_idx] = closest(f, hi);

        % Estimate the fit of the peak
        fit_mdl = fitlm(f_sub(low_idx:hi_idx), xp(low_idx:hi_idx));

        % Save adjusted R squared for model comparison
        % R squared can be relatively low for a good fit if the residuals 
        % have some structure (i.e. aren't gaussian distributed)
        adjRsquared(idx) = fit_mdl.Rsquared.Adjusted;

        % Check confidence intervals of the estimates. If CI of peak's
        % height contains zero, it implies poor fit => reduce R squared
        %ci = confint(mdl);
        %if ci(1,1) <= 0
        %    adjRsquared(idx) = 0;
        %end
        % Punish for large confidence interval
    %         adjRsquared(idx) = adjRsquared(idx) - (ci(2,1)-ci(1,1))/max(Fsub(x8:x12));
        % Reward higher peaks
    %         height = 1 - (mdl.a1/max(Fsub(x8:x12)));
    %         adjRsquared(idx) = mean([adjRsquared(idx), (mdl.a1/max(Fsub(x8:x12)))]);
    %         adjRsquared(idx) = adjRsquared(idx) - (ci(2,2) - ci(1,2));

    end
    
    % Choose the model with highest adjusted R squared
    [~, idx] = max(adjRsquared);
    mdl = mdls{idx};

    if showplots
        % Evaluate the model at points in xi (for plotting)
        xp = feval(mdl,f);

        figure();
    %     plot(xi,Fsub), hold on, plot(xi, xp); plot([O.cv(1) O.cv(1)], [0, max(Fsub)]);
        plot(f,f_sub),hold on, plot(f, xp); 

        title_msg = ['adjRsquared: ' num2str(adjRsquared(idx)) ', b1: ', num2str(mdl.b1)];
        title(title_msg);

    end

    out(channel_idx, :) = [mdl.b1, adjRsquared(idx)];

end
end
