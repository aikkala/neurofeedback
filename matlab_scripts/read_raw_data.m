function data = read_raw_data(arg, metric)

% If nargin < 2 then first argument should contain a file
% Otherwise read mapping corresponding to metric from a log file

if nargin > 1
    % Read buffer's log file
    txt = read_gz_file(fullfile(arg, 'log', 'ATBuffer.dat.gz'));
    
    % Find mapping for queried metric
    channel = find_mapping(txt, metric); 
    
    data_file = fullfile(arg, 'dat', 'ATBuffer.dat.gz');
else
    data_file = arg;
end

% Read data file
data = read_dat_gz(data_file);

% Need to convert into double array

% Return only channel? and timepoints

end