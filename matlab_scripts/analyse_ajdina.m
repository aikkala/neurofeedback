function analyse_ajdina

% Define data file
%data_file = '/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/data/piloting-2019-01-23/ajdina_last_session/dat/DataBuffer.dat';
%idx = [40000, 80000];
%data_file = '/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/data/piloting-2019-01-28/ajdina-ta-no-smoothing/dat/DataBuffer.dat';
%idx = [80000, 100000];
%data_file = '/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/data/piloting-2019-02-04/ajdina-ta-louder/dat/DataBuffer.dat';
%idx = [1, Inf];

% Grab period of good data
%data = importfile(data_file, idx(1), idx(2));

% Read only gelled sensors
d = read_gz_dat('DataBuffer.dat.gz', [5,6,12,70,75,83]);

% Use only gelled sensors
%data = d.data(:, [5, 6, 12, 70, 75, 83]);

% Playback PSD
plot_PSD(d.data, ['b','b','b','r','r','r']);

% Compute alpha, theta, and theta/alpha in beginning and end of rest
%compare_powers(data, (1:3)', (4:6)')

end



function data = importfile(filename, startRow, endRow)
% Initialize variables.
delimiter = ' ';
if nargin<=2
    startRow = 1;
    endRow = Inf;
end

% Format for each line of text:
% For more information, see the TEXTSCAN documentation.
formatSpec = '%*s%*s%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%[^\n\r]';

% Open the text file.
fileID = fopen(filename,'r');

% Read columns of data according to the format.
dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1, 'Delimiter', delimiter, 'MultipleDelimsAsOne', true, 'TextType', 'string', 'EmptyValue', NaN, 'HeaderLines', startRow(1)-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
for block=2:length(startRow)
    frewind(fileID);
    dataArrayBlock = textscan(fileID, formatSpec, endRow(block)-startRow(block)+1, 'Delimiter', delimiter, 'MultipleDelimsAsOne', true, 'TextType', 'string', 'EmptyValue', NaN, 'HeaderLines', startRow(block)-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
    for col=1:length(dataArray)
        dataArray{col} = [dataArray{col};dataArrayBlock{col}];
    end
end

% Close the text file.
fclose(fileID);

% Create output variable
data = [dataArray{1:end-1}];
end