function [path, info] = find_condition(session, varargin)
% Returns a path to queried condition, or an empty array if no such
% condition is found

% Get all conditions (recordings) in the session
conditions = dir(session);
conditions = string({conditions.name}');

% Remove this and parent folder, and all hidden files/folders
conditions(startsWith(conditions, '.')) = [];

% Parse queried parameters
if mod(nargin-1, 2) ~= 0
    error('Number of input parameters should be even');
end
query = struct();
for key_idx = 1:2:nargin-1
    query.(varargin{key_idx}) = varargin{key_idx+1};
end

% Loop through conditions until you find one that fits the query
path = [];
for condition = conditions'
    info = get_condition_info(fullfile(session, condition));
    if matches_query(info, query)
        path = [path; condition];
    end
end

end

function match = matches_query(info, query)
fields = fieldnames(query);
for field = fields'
    field = char(field);
    if info.(field) ~= query.(field)
        match = false;
        return
    end
end
match = true;
end