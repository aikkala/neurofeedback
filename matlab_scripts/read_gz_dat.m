function D = read_gz_dat(file, channels)
% Read data array from GZIP file

% Try to open gzip file
streams = open_gz_file(file);

% Don't continue if reader has not been opened
if ~isfield(streams, 'reader')
    D = struct();
    return;
end

% Ignore header
while 1
    line = char(streams.reader.readLine());
    if isempty(line) || line(1) ~= '#'
        break;
    end
end

% Return if there are no data
if isempty(line)
    return;
end

% Calculate number of channels
nchannels = numel(strsplit(line, ' '))-3;

if nargin < 2 || isempty(channels)
    channels = (1:nchannels)';
end

% We could check here whether the second element in 'line' is a string or
% not, and then create a format_spec accordingly. But thus far we've only
% needed this function to read SharedBuffer outputs, and they always have a
% string as the second element.

% Create format spec
format_spec = '%u64 %s';
for idx = 1:nchannels
    format_spec = sprintf('%s %%f', format_spec);
end

% Read lines as strings
initialised_size = 10000;
current_line = 1;
D = struct('timestamp', nan(initialised_size,1), 'data', nan(initialised_size,numel(channels)));


while ~isempty(line)
    % Make txt larger if needed
    if current_line >= initialised_size
        initialised_size = initialised_size*2;
        timestamp_tmp = nan(initialised_size,1);
        data_tmp = nan(initialised_size, numel(channels));
        timestamp_tmp(1:current_line-1) = D.timestamp(1:current_line-1);
        data_tmp(1:current_line-1,:) = D.data(1:current_line-1,:);
        D.timestamp = timestamp_tmp;
        D.data = data_tmp;
        clear timestamp_tmp data_tmp
    end
    
    % Parse line
    parsed = textscan(line, format_spec);
    
    D.timestamp(current_line) = parsed{1};
    for channel_idx = 1:numel(channels)
        D.data(current_line, channel_idx) = parsed{2+channels(channel_idx)};
    end
    
    % Read next line
    line = char(streams.reader.readLine());
    current_line = current_line+1;
end

% Close streams
close_gz_file(streams);

% Remove empty rows
D.timestamp(current_line:end) = [];
D.data(current_line:end,:) = [];
end