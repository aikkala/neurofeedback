function power = compare_psd_epochs(sessions_path, condition_query, bands, sensors)
% Estimate PSD for a queried condition in each session; return power in given
% bands per epoch

% Define epoch length in seconds
epoch_length = 2;

% TODO 
% - handle missing conditions / poor queries that result in too many
% conditions returned

if nargin < 4
    sensors = [];
end

% Get sessions
sessions = dir(sessions_path);
sessions = string({sessions.name}');

% Remove this folder, parent folder, and any hidden files/folders
sessions(startsWith(sessions, '.')) = [];

% Do only first and last session
sessions = sessions([1; numel(sessions)]);

% Sometimes we are a couple of samples short of 300 seconds, so let's
% just forget the last epoch
nepochs = floor(300/epoch_length)-1;
power = nan(size(bands,1), numel(sessions), nepochs);

% Loop through sessions
for sess_idx = 1:numel(sessions)
        
    % Find [TA ratio / decrease] condition
    current_session = fullfile(sessions_path, sessions(sess_idx));
    condition = find_condition(current_session, condition_query{:});
    
     % Skip if condition wasn't found
    if isempty(condition)
        warning('Condition [%s] wasn''t found from path [%s]', strjoin(condition_query, ' / '), sessions_path);
        continue;
    end
    
    % Grab condition info
    current_condition = fullfile(current_session, condition);
    info = get_condition_info(current_condition);
    
    % Get (expected) sensor labels
    labels = arrayfun(@(x) string(sprintf('E%d', x)), info.channels);
    labels = replace_broken_sensor_labels(labels);
        
    % Grab data
    d = get_experiment(current_condition, info.channels);

    % Get sampling frequency
    fs = info.downsample_rate;

    % Filter; remember to start from second sample if filtering all data
    % (unlike data recorded only during experiment as we do here)
    f = butter_filter(d.data, fs, 'bandpass', [0.5,35]);

    % Number of samples in an epoch
    nsamples = fs*epoch_length;

    % Reshape data into epochs
    f = reshape(f(1:nepochs*nsamples,:), nsamples, nepochs, size(f,2));

    % Loop over epochs
    for epoch_idx = 1:nepochs

        % Estimate PSD
        [pxx, fxx] = pwelch(squeeze(f(:,epoch_idx,:)), nsamples, 0, [], fs);

        % Calculate powers
        for band_idx = 1:size(bands,1)

            % Find indices
            [~, band_start] = closest(fxx, bands(band_idx, 1));
            [~, band_end] = closest(fxx, bands(band_idx, 2));
        
            % Find indices that are smaller or larger than the band that is
            % given
            if fxx(band_start) > bands(band_idx,1)
                band_start = band_start-1;
            end
            if fxx(band_end) < bands(band_idx,2)
                band_end = band_end+1;
            end
        
            % Grab only a subset of sensors if required
            if ~isempty(sensors)
                if numel(sensors) > 1
                    subset = sensors{band_idx};
                else
                    subset = sensors;
                end
                channel_indices = ismember(labels, subset);
            else
                channel_indices = true(size(pxx,2),1);
            end

            % Estimate power
            power(band_idx, sess_idx, epoch_idx) = mean(sum(pxx(band_start:band_end, channel_indices)));

        end

    end

end

% Permute power array so it's easier to do stats testing
power = permute(power, [3,2,1]);

end
