function compare_latest_sessions(subject_folder)

% Get latest sessions
[earlier, later] = get_latest_sessions(subject_folder);
fprintf('Earlier session is %s, later session is %s\n', earlier, later);

% Collect ratio/variance increase/decrease conditions into these structs
earlier_conditions = struct();
later_conditions = struct();

% Get increase and decrease conditions for theta-alpha ratio
earlier_conditions.ratio = get_increase_decrease_conditions(earlier, 'theta-alpha ratio');
later_conditions.ratio = get_increase_decrease_conditions(later, 'theta-alpha ratio');

% Get increase and decrease conditions for theta-alpha variance
earlier_conditions.variance = get_increase_decrease_conditions(earlier, 'theta-alpha variance');
later_conditions.variance = get_increase_decrease_conditions(later, 'theta-alpha variance');

% Now we need to get the actual data for each condition
earlier_conditions = get_condition_values(earlier_conditions);
later_conditions = get_condition_values(later_conditions);

% Do some magic with those values, like print out medians

% Theta-alpha ratio, decrease condition
ratio_decrease = compare_medians(earlier_conditions.ratio.decrease.data, later_conditions.ratio.decrease.data, 'theta-alpha ratio', 'decrease');

% Theta-alpha variance, decrease condition
variance_decrease = compare_medians(earlier_conditions.variance.decrease.data, later_conditions.variance.decrease.data, 'theta-alpha variance', 'decrease');

% Theta-alpha ratio, increase condition
ratio_increase = compare_medians(earlier_conditions.ratio.increase.data, later_conditions.ratio.increase.data, 'theta-alpha ratio', 'increase');

% Theta-alpha variance, increase condition
variance_increase = compare_medians(earlier_conditions.variance.increase.data, later_conditions.variance.increase.data, 'theta-alpha variance', 'increase');

% Calculate combined improvement estimate
final_improve_num = ratio_decrease + variance_decrease - ratio_increase - variance_increase;

fprintf('Final improvement (positive means good!): %4.2f%%\n', final_improve_num);

end


% Returns two latest sessions, 'earlier' is a path to session that occurred
% chronologically earlier, and 'later' is a path to the later sessions
function [earlier, later] = get_latest_sessions(subject_folder)

% Get folders
folders = dir(subject_folder);
folders(1:2) = [];
folders = string({folders.name}');

if numel(folders) < 2
    error('There aren''t enough sessions! At least two are required');
end

% Convert folder names into datetimes
dt = datetime(folders);

% Sort datetimes
[~, sort_idx] = sort(dt, 'descend');
folders = folders(sort_idx);
dt = dt(sort_idx);

% Ask which dates we should use
while 1
    [indx,tf] = listdlg('ListString',dt, 'PromptString', 'Select folders:', 'ListSize', [160, 150]);
    if numel(indx) ~= 2 || tf == 0
        warning('You must choose two dates!');
    else
        break
    end
end

% Return two of the latest sessions
later = fullfile(subject_folder, folders(indx(1)));
earlier = fullfile(subject_folder, folders(indx(2)));

end

% Returns paths to increase and decrease conditions for the queried measure
function conditions = get_increase_decrease_conditions(session, measure)

% Initialise a struct that holds the paths and will hold the data
conditions = struct();
conditions.increase = struct('path', [], 'data', []);
conditions.decrease = struct('path', [], 'data', []);

% Find increase condition
conditions.increase.path = fullfile(session, ...
    find_condition(session, 'measure', measure, 'goal', 'increase', 'audio', 'on', 'randomise', 'off'));

% Find decrease condition
conditions.decrease.path = fullfile(session, ...
    find_condition(session, 'measure', measure, 'goal', 'decrease', 'audio', 'on', 'randomise', 'off'));

% Make sure we found those conditions
if numel(conditions.increase.path) > 1
    error('Found too many (%d) conditions for [%s] [increase] condition in session %s', ...
        numel(conditions.increase.path), measure, session);
elseif conditions.increase.path == session
    %fprintf('Warning! Could not find [%s] [increase] condition for session %s\n', measure, session);
    conditions.increase.path = [];
end
if numel(conditions.decrease.path) > 1
    error('Found too many (%d) conditions for [%s] [decrease] condition in session %s', ...
        numel(conditions.decrease.path), measure, session);
elseif conditions.decrease.path == session
    %fprintf('Warning! Could not find [%s] [decrease] condition for session %s\n', measure, session);
    conditions.decrease.path = [];
end

end

% Returns conditions filled with data
function conditions = get_condition_values(conditions)
if ~isempty(conditions.ratio.increase.path)
    t = get_experiment(conditions.ratio.increase.path, [], 'TrackBuffer.dat.gz');
    conditions.ratio.increase.data = t.data;
end

if ~isempty(conditions.ratio.decrease.path)
    t = get_experiment(conditions.ratio.decrease.path, [], 'TrackBuffer.dat.gz');
    conditions.ratio.decrease.data = t.data;
end

if ~isempty(conditions.variance.increase.path)
    t = get_experiment(conditions.variance.increase.path, [], 'TrackBuffer.dat.gz');
    conditions.variance.increase.data = t.data;
end

if ~isempty(conditions.variance.decrease.path)
    t = get_experiment(conditions.variance.decrease.path, [], 'TrackBuffer.dat.gz');
    conditions.variance.decrease.data = t.data;
end
end

% Print out and visualise medians in given conditions
function improve_num = compare_medians(earlier_condition, later_condition, measure, condition)

% If some data is missing return zero
if isempty(earlier_condition) || isempty(later_condition)
    fprintf('Warning! Data missing for [%s] [%s], this condition will be ignored\n', measure, condition);
    improve_num = 0;
    return
end

% Get medians
median_earlier = median(earlier_condition);
median_later = median(later_condition);

% Print out medians
%fprintf('Median for [%s] [%s] condition in earlier and later recording: %f %f\n', ...
%    measure, condition, median_earlier, median_later);

figure; bar([median_earlier, median_later]);
title({sprintf('%s', measure), sprintf('%s condition', condition)});
xticklabels({'Earlier', 'Later'});

improve_num = (median_later / median_earlier - 1) *100;

end