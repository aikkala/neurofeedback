
% Set filename and sensors
filename = '/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/data/recorded/piloting-2019-02-21/fluc_ratio_quieter-2019-02-21T10:54Z/dat/DataBuffer.dat.gz';
%filename = '/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/data/recorded/piloting-2019-02-21/fluc_ratio_louder-2019-02-21T11:09Z/dat/DataBuffer.dat.gz';
sensors = unique([44, 107, 101, 114, 99, 32, 121, 10, 69, 74, 82, 35, 103, 89]) + 1;

% Read the data
d = read_gz_dat(filename, sensors);

% Use Sri's drowsiness classifier
[theta_trials, alpha_trials] = compare_power_variance(d.data);

% Calculate TA ratio with separated channels
[theta, alpha] = calculate_theta_alpha_powers(d.data, [1,2,14], [5,6,7]);
separated_at = mean(theta,2)./mean(alpha,2);

% Calculate TA ratio with all channels
[theta, alpha] = calculate_theta_alpha_powers(d.data, 1:14, 1:14);
all_at = mean(theta,2)./mean(alpha,2);

% Figure out which TA ratio samples fall into which trials
trials = zeros(100,338);
trials(:,alpha_trials) = 2;
trials(:,theta_trials) = 1;
trials = trials(:);

% Cut TA ratio values because there's an artefact in the end
trials(33001:end) = [];
separated_at(33001:end) = [];
all_at(33001:end) = [];

figure, h=histogram(separated_at(trials==1));
hold on; histogram(separated_at(trials==2), h.BinEdges)
set(gca,'yscale','log')
legend('Theta trials', 'Alpha trials');
title('Separate channels');
xlabel('TA ratio');

figure, h=histogram(all_at(trials==1));
hold on; histogram(all_at(trials==2), h.BinEdges)
set(gca,'yscale','log')
legend('Theta trials', 'Alpha trials');
title('Average channels');
xlabel('TA ratio');

figure, plot(separated_at); hold on; plot(all_at); legend('Separate channels', 'Averaged channels');
ylabel('TA ratio');

figure, plot(movmedian(separated_at,500)); hold on; plot(movmedian(all_at,500)); legend('Separate channels', 'Averaged channels');
ylabel('TA ratio');