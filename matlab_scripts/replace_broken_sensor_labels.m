function labels = replace_broken_sensor_labels(labels)

% Replace the label in first column with a label in the second column
replace = [
    "E109", "E115";
    "E5", "E11";
    "E72", "E75";
    "E84", "E90";
    "E116", "E122";
    "E96", "E100";
    "E16", "E11";
    "E95", "E100";
    "E123", "E122";
    "E82", "E83"];

for replace_idx = 1:size(replace,1)
    idx = ismember(labels, replace(replace_idx,1));
    if sum(idx)
        labels(idx) = replace(replace_idx,2);
    end
end

end