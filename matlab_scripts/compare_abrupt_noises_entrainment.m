function compare_abrupt_noises_entrainment()

path = '/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/data/piloting-2019-03-05/';
path_theta = fullfile(path, 'ajdina-theta-4hz-2019-03-05T16:33Z');
path_alpha = fullfile(path, 'ajdina-alpha-9hz-2019-03-05T16:56Z');
path_constant = fullfile(path, 'ajdina-no-modulation-2019-03-05T16:44Z');

% Last minutes is now [8,9] instead of [9,10] because one of the recordings
% is a bit short (~30 seconds short of full 10 minutes). This is because
% the audio file wasn't on loop and switched in the end of recording
compare_modulations(path_theta, path_alpha, path_constant, [8,9]);

end