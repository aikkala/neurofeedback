function check_conditions(session_path)
% Check that all required conditions have been recorded

% Define conditions
conditions = { ....
    {'measure', 'theta-alpha ratio', 'goal', 'increase', 'audio', 'on', 'randomise', 'off'}
    {'measure', 'theta-alpha ratio', 'goal', 'decrease', 'audio', 'on', 'randomise', 'off'}
    {'measure', 'theta-alpha variance', 'goal', 'increase', 'audio', 'on', 'randomise', 'off'}
    {'measure', 'theta-alpha variance', 'goal', 'decrease', 'audio', 'on', 'randomise', 'off'}
    {'randomise', 'on', 'audio', 'on', 'goal', 'increase'}
    {'randomise', 'on', 'audio', 'on', 'goal', 'decrease'}
    {'audio', 'off', 'goal', 'increase'}
    {'audio', 'off', 'goal', 'decrease'}
};

% Loop through conditions and check which ones are present
present = false(size(conditions));
for condition_idx = 1:numel(conditions)
    condition = conditions{condition_idx};
    if ~isempty(find_condition(session_path, condition{:}))
        present(condition_idx) = true;
    end
end

% Print conditions that are missing
for present_idx = 1:numel(present)
    if ~present(present_idx)
        fprintf('Condition missing! Parameters: %s\n', strjoin(conditions{present_idx}, ' / '));
    end
end
end