function compare_amplitude_modulation()

path = '/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/data/piloting-2019-02-28/';
path_theta = fullfile(path, 'theta-modulation-4Hz-2019-02-28T12:04Z');
path_alpha = fullfile(path, 'alpha-modulation-9.87Hz-2019-02-28T11:42Z');
path_constant = fullfile(path, 'no-modulation-2019-02-28T11:53Z');

compare_modulations(path_theta, path_alpha, path_constant, [9,10]);

end