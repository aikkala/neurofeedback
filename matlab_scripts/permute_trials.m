function [permuted_trials1, permuted_trials2] = permute_trials(trials1, trials2)

L = 500;
fs = 250;
ntrials1 = floor(size(trials1,1)/L);
ntrials2 = floor(size(trials2,1)/L);
nsensors = size(trials1,2);

if nsensors ~= size(trials2,2)
    error('There must be equal number of sensors in both datasets');
end

% filter data first so we don't get discontnuities
trials1 = butter_filter(trials1, fs, 'bandpass', [0.5, 40]);
trials2 = butter_filter(trials2, fs, 'bandpass', [0.5, 40]);

% Concatenate
all_data = [trials1(1:L*ntrials1,:); trials2(1:L*ntrials2,:)];

% Reshape
all_data = reshape(all_data,L,ntrials1+ntrials2,nsensors);
%all_data = reshape(all_data,L,(ntrials1+ntrials2)*nsensors);

% Shuffle
idx = randperm(ntrials1+ntrials2);
all_data = all_data(:,idx,:);
%idx = randperm(size(all_data,2));
%all_data = all_data(:,idx);

% Reshape back
all_data = reshape(all_data,L*(ntrials1+ntrials2),nsensors);

permuted_trials1 = all_data(1:L*ntrials1,:);
permuted_trials2 = all_data(L*ntrials1+1:end,:);

end