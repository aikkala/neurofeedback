function compare_modulations(path_theta, path_alpha, path_constant, last_minutes)

addpath /home/aleksi/Workspace/software/matlab/plotSpread

sensors = unique([44, 107, 101, 114, 99, 32, 121, 10, 69, 74, 82, 35, 103, 89]) + 1;
%sensors = [70, 75, 83];
%sensors = [11, 33, 122];

fs = 250;
L = 4*fs;
theta_band = [4,4];
alpha_band = [10,10];

% Define paths
%path = '/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/data/piloting-2019-02-28/';
%path_theta = fullfile(path, 'theta-modulation-4Hz-2019-02-28T12:04Z');
%path_alpha = fullfile(path, 'alpha-modulation-9.87Hz-2019-02-28T11:42Z');
%path_constant = fullfile(path, 'no-modulation-2019-02-28T11:53Z');

%path = '/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/data/piloting-2019-03-05/';
%path_theta = fullfile(path, 'ajdina-theta-4hz-2019-03-05T16:33Z');
%path_alpha = fullfile(path, 'ajdina-alpha-9hz-2019-03-05T16:56Z');
%path_constant = fullfile(path, 'ajdina-no-modulation-2019-03-05T16:44Z');
%path_theta = fullfile(path, 'andres-theta-4hz-2019-03-05T17:07Z');
%path_alpha = fullfile(path, 'andres-alpha-9hz-2019-03-05T17:20Z');


% Get experiments
theta = get_experiment(path_theta, sensors);
alpha = get_experiment(path_alpha, sensors);
constant = get_experiment(path_constant, sensors);

% Filter data
ftheta = butter_filter(theta.data, fs, 'bandpass', [0.5,40]);
falpha = butter_filter(alpha.data, fs, 'bandpass', [0.5,40]);
fconstant = butter_filter(constant.data, fs, 'bandpass', [0.5,40]);

% Plot minute averaged/aggregated timecourses
[theta_powers, real_theta_band, real_alpha_band] = get_powers(ftheta, L, fs, theta_band, alpha_band);
alpha_powers = get_powers(falpha, L, fs, theta_band, alpha_band);
constant_powers = get_powers(fconstant, L, fs, theta_band, alpha_band);

plot_averaged_timecourses(theta_powers.theta, alpha_powers.theta, constant_powers.theta, ...
    sprintf('Band [%.1f, %.1f] Hz', real_theta_band(1), real_theta_band(2)), (60*250)/L);
plot_averaged_timecourses(theta_powers.alpha, alpha_powers.alpha, constant_powers.alpha, ...
    sprintf('Band [%.1f, %.1f] Hz', real_alpha_band(1), real_alpha_band(2)), (60*250)/L);

% Plot first two minutes
first_minutes = [1,2];
idxs = minutes_to_idxs(first_minutes, fs);
plot_spreads(ftheta(idxs,:), falpha(idxs,:), fconstant(idxs,:), fs, L, theta_band, alpha_band, first_minutes);

% Plot last two minutes
%first_minutes = [9,10];
idxs = minutes_to_idxs(last_minutes, fs);
plot_spreads(ftheta(idxs,:), falpha(idxs,:), fconstant(idxs,:), fs, L, theta_band, alpha_band, last_minutes);


% Plot average PSD; Last two minutes
%plot_spectra(ftheta, falpha, fconstant, L, fs, sprintf('Last %d minutes', minutes));

end


function plot_averaged_timecourses(theta, alpha, constant, title_msg, nepochs)

mins = 10;
req_nepochs = mins*nepochs;

% Add some nans if necessary
if numel(theta) < req_nepochs
    theta(numel(theta):req_nepochs) = nan;
end
if numel(alpha) < req_nepochs
    alpha(numel(alpha):req_nepochs) = nan;
end
if numel(constant) < req_nepochs
    constant(numel(constant):req_nepochs) = nan;
end

% Plot averaged powers
figure; hold on;
theta_avg = nanmedian(reshape(theta, nepochs, mins));
alpha_avg = nanmedian(reshape(alpha, nepochs, mins));
constant_avg = nanmedian(reshape(constant, nepochs, mins));

plot(1:mins, theta_avg, 'b', 'linewidth', 2);
plot(1:mins, alpha_avg, 'r', 'linewidth', 2);
plot(1:mins, constant_avg, 'k', 'linewidth', 2);
xlabel('Minute'); ylabel('Power');
title({'Median timecourses', title_msg});
legend('Theta modulated', 'Alpha modulated', 'No modulation', 'location', 'best');

end


function plot_spreads(ftheta, falpha, fconstant, fs, L, theta_band, alpha_band, minutes)

% Extract powers
[theta_powers, real_theta_band, real_alpha_band] = get_powers(ftheta, L, fs, theta_band, alpha_band);
alpha_powers = get_powers(falpha, L, fs, theta_band, alpha_band);
constant_powers = get_powers(fconstant, L, fs, theta_band, alpha_band);

% Remove outliers
theta_powers_clean = remove_outliers(theta_powers);
alpha_powers_clean = remove_outliers(alpha_powers);
constant_powers_clean = remove_outliers(constant_powers);

% Plot histograms of theta and alpha band powers
p = kruskalwallis([theta_powers.theta; alpha_powers.theta; constant_powers.theta], ...
    [ones(numel(theta_powers.theta),1); 2*ones(numel(alpha_powers.theta),1); 3*ones(numel(constant_powers.theta),1)], 'off');

figure; plotSpread({theta_powers.theta, alpha_powers.theta, constant_powers.theta}, 'showMM', 1, ...
    'xNames', {"Theta mod", "Alpha mod", "Constant"});
title({sprintf('Minutes: %s', num2str(minutes)), sprintf('Theta powers: [%.1f, %.1f] Hz', real_theta_band(1), real_theta_band(2)), sprintf('Kruskal-Wallis p = %f', p)});


p = kruskalwallis([theta_powers.alpha; alpha_powers.alpha; constant_powers.alpha], ...
    [ones(numel(theta_powers.alpha),1); 2*ones(numel(alpha_powers.alpha),1); 3*ones(numel(constant_powers.alpha),1)], 'off');

figure; plotSpread({theta_powers.alpha, alpha_powers.alpha, constant_powers.alpha}, 'showMM', 1, ...
    'xNames', {"Theta mod", "Alpha mod", "Constant"})
title({sprintf('Minutes: %s', num2str(minutes)), sprintf('Alpha powers: [%.1f, %.1f] Hz', real_alpha_band(1), real_alpha_band(2)), sprintf('Kruskal-Wallis p = %f', p)});


% Plot those histograms without outliers
p = kruskalwallis([theta_powers_clean.theta; alpha_powers_clean.theta; constant_powers_clean.theta], ...
    [ones(numel(theta_powers_clean.theta),1); 2*ones(numel(alpha_powers_clean.theta),1); 3*ones(numel(constant_powers_clean.theta),1)], 'off');

figure; plotSpread({theta_powers_clean.theta, alpha_powers_clean.theta, constant_powers_clean.theta}, 'showMM', 1, ...
    'xNames', {"Theta mod", "Alpha mod", "Constant"});
title({sprintf('Minutes: %s', num2str(minutes)), sprintf('Theta powers: [%.1f, %.1f] Hz (outliers removed)', real_theta_band(1), real_theta_band(2)), sprintf('Kruskal-Wallis p = %f', p)});

p = kruskalwallis([theta_powers_clean.alpha; alpha_powers_clean.alpha; constant_powers_clean.alpha], ...
    [ones(numel(theta_powers_clean.alpha),1); 2*ones(numel(alpha_powers_clean.alpha),1); 3*ones(numel(constant_powers_clean.alpha),1)], 'off');

figure; plotSpread({theta_powers_clean.alpha, alpha_powers_clean.alpha, constant_powers_clean.alpha}, 'showMM', 1, ...
    'xNames', {"Theta mod", "Alpha mod", "Constant"})
title({sprintf('Minutes: %s', num2str(minutes)), sprintf('Alpha powers: [%.1f, %.1f] Hz (outliers removed)', real_alpha_band(1), real_alpha_band(2)), sprintf('Kruskal-Wallis p = %f', p)});

end


function idxs = minutes_to_idxs(minutes, fs)
idxs = arrayfun(@(x) (((x-1)*60*fs)+1):x*60*fs, minutes, 'uniformoutput', false);
idxs = cat(2,idxs{:})';
end

function powers = remove_outliers(powers)
powers.theta(isoutlier(powers.theta,'quartiles')) = [];
powers.alpha(isoutlier(powers.alpha,'quartiles')) = [];
end


function [powers, real_theta_band, real_alpha_band] = get_powers(data, L, fs, theta_band, alpha_band)

% Go through data in non-overlapping windows
start_idx = 1;
end_idx = L;

theta_idxs = [];
alpha_idxs = [];

alpha_power = [];
theta_power = [];

while (end_idx <= size(data,1))
    
    % Calculate average PSD over all sensors
    [pxx,f] = pwelch(data(start_idx:end_idx,:), L, 0, [], fs);
    pxx = mean(pxx,2);
    
    % Calculate band indices if they're empty
    if isempty(theta_idxs)
        theta_idxs = [nearest_idx(f, theta_band(1)), nearest_idx(f, theta_band(2))];
    end
    if isempty(alpha_idxs)
        alpha_idxs = [nearest_idx(f, alpha_band(1)), nearest_idx(f, alpha_band(2))];
    end
    
    alpha_power = [alpha_power; sum(pxx(theta_idxs(1):theta_idxs(2)))];
    theta_power = [theta_power; sum(pxx(alpha_idxs(1):alpha_idxs(2)))];
    
    % Non-overlapping windows
    start_idx = start_idx+L; end_idx = end_idx + L;

end

powers = struct('alpha', alpha_power, 'theta', theta_power);
real_theta_band = f(theta_idxs);
real_alpha_band = f(alpha_idxs);

end


function plot_spectra(ftheta, falpha, fconstant, L, fs, title_msg)

[pt,ft,pct] = pwelch(ftheta, L, 0, [], fs, 'confidencelevel', 0.95);
[pa,fa,pca] = pwelch(falpha, L, 0, [], fs, 'confidencelevel', 0.95);
[pc,fc,pcc] = pwelch(fconstant, L, 0, [], fs, 'confidencelevel', 0.95);

conf_theta = [mean(pct(:,1:2:end),2), mean(pct(:,2:2:end),2)];
conf_alpha = [mean(pca(:,1:2:end),2), mean(pca(:,2:2:end),2)];
conf_constant = [mean(pcc(:,1:2:end),2), mean(pcc(:,2:2:end),2)];

% Plot without confidence interval
%figure; hold on; set(gca,'yscale','log')
%plot(ft, mean(pt,2), 'b', 'linewidth', 2);
%plot(fa, mean(pa,2), 'r', 'linewidth', 2);
%plot(fc, mean(pc,2), 'k', 'linewidth', 2);
%xlabel('Hz'); ylabel('log(magnitude squared)');
%legend('Theta modulated', 'Alpha modulated', 'Constant', 'location', 'best');
%title(title_msg);

% Plot with confidence interval
figure; hold on; set(gca,'yscale','log');
h1 = plot_ci(ft, [mean(pt,2), conf_theta],'patchalpha',0.3,'patchcolor','b','mainlinecolor','b','mainlinewidth',2);
h2 = plot_ci(fa, [mean(pa,2), conf_alpha],'patchalpha',0.3,'patchcolor','r','mainlinecolor','r','mainlinewidth',2);
h3 = plot_ci(fc, [mean(pc,2), conf_constant],'patchalpha',0.3,'patchcolor','k','mainlinecolor','k','mainlinewidth',2);
xlabel('Hz'); ylabel('log(magnitude squared');
legend([h1.Plot, h2.Plot, h3.Plot], 'Theta modulated', 'Alpha modulated', 'Constant', 'location', 'best');
title(title_msg);

end