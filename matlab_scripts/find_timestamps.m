function timepoints = find_timestamps(txt, query)

% Loop through txt and return timepoints that corresponds to query
timepoints = [];
for msg = txt'
    if contains(msg, query)
        split = strsplit(msg, ' ');
        try
            timepoints(end+1) = str2double(split{1});
        catch ex
            printf('Couldn''t find timepoint from message: %s\n', msg);
        end
    end
end

end