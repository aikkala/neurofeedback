function plot_PSD(signals, colors)

if nargin < 2
    colors = [];
end

L = 500;
fs = 250;

window = hamming(L);

start_idx = 1;end_idx = L;
while (end_idx < size(signals,1))
    pxxt = zeros(floor(L/2)+1,1);
    clf;
    
    subplot(3,1,1); hold on;
    for i = 1:size(signals,2)
        %[pxx,f] = pwelch(detrend(signals(start_idx:end_idx,i)),L,0,L,fs);
        %[pxx,f] = calculate_spectrogram(signals(start_idx:end_idx,i), L, fs);
        [pxx,f] = pwelch(detrend(signals(start_idx:end_idx,i)), 100, 50, L, fs);
        if isempty(colors)
            color = 'b';
        else
            color = colors(i);
        end
        plot(f,pxx, color);
        pxxt = pxxt + pxx;
    end
    
    title(sprintf('%.1f seconds', start_idx/250));
    %plot(f,pxxt./size(signals,2), 'k', 'linewidth', 2);
    axis([0,20,0,50])
    
    subplot(3,1,2);
    plot(1:500, butter_filter(detrend(signals(start_idx:end_idx,end-2:end)), 250, 'low', 30), 'r');
    axis([0,500,-50,50])

    subplot(3,1,3);
    %plot(1:500, butter_filter(window.*signals(start_idx:end_idx,1), 250, 'bandpass', [3,30]), 'b');
    plot(1:500, butter_filter(detrend(signals(start_idx:end_idx,1:3)), 250, 'low', 30), 'b');
    axis([0,500,-50,50])

    
    pause(0.00000001);
    start_idx = start_idx+5; end_idx = end_idx + 5;

end

end


function [pxxt,f] = calculate_spectrogram(signal, L, fs)

pxxt = zeros(floor(L/2)+1,1);

window_size = 100;
noverlap = 50;
start_idx = 1; end_idx = window_size;
i = 0;
while (end_idx <= numel(signal))
    [pxx,f] = pwelch(detrend(signal(start_idx:end_idx)), window_size, 0, L, fs);
    pxxt = pxxt + pxx;
    i = i+1;
    start_idx = end_idx-noverlap; end_idx = start_idx + window_size;
end

% Take the average
pxxt = pxxt./i;

end