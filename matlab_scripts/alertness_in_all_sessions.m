function [ratios, conditions] = alertness_in_all_sessions(subjects)
% Estimate alertness level in all subjects and in all conditions


% Define subjects
if nargin < 1
    subjects = ["AG", "CF", "CW", "GB", "JS"];
end
data_path = "/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/data/recorded/subjects/";

% Define conditions
conditions = { ...
    {'measure', 'theta-alpha ratio', 'goal', 'increase', 'audio', 'on', 'randomise', 'off'}
    {'measure', 'theta-alpha ratio', 'goal', 'decrease', 'audio', 'on', 'randomise', 'off'}
    {'measure', 'theta-alpha variance', 'goal', 'increase', 'audio', 'on', 'randomise', 'off'}
    {'measure', 'theta-alpha variance', 'goal', 'decrease', 'audio', 'on', 'randomise', 'off'}
    {'randomise', 'on', 'audio', 'on', 'goal', 'increase'}
    {'randomise', 'on', 'audio', 'on', 'goal', 'decrease'}
    {'audio', 'off', 'goal', 'increase'}
    {'audio', 'off', 'goal', 'decrease'}
};

% Estimate alertness in last session for each subject
ratios = struct();
for subject_idx = 1:numel(subjects)
    
    % Grab a subject
    subject = subjects{subject_idx};
    subject_ratios = cell(numel(conditions),1);
    
    % Go through all conditions
    for condition_idx = 1:numel(conditions)
        subject_ratios{condition_idx} = compare_sri_alertness(fullfile(data_path, subject), conditions{condition_idx});
    end
    
    % Save ratios into a struct
    ratios.(subject) = cat(2, subject_ratios{:});
end

% Convert conditions into a string array, easier to read
conditions = string(cellfun(@(x) strjoin(x, ' / '), conditions, 'uniformoutput', false));

end