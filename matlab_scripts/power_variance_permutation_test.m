function [real_diff, null_diff, pc, p, z] = power_variance_permutation_test(quiet_data, loud_data)

% Calculate the real difference in ratio
[theta_quiet, alpha_quiet] = compare_power_variance(quiet_data);
[theta_loud, alpha_loud] = compare_power_variance(loud_data);
quiet_ratio = sum(theta_quiet)/(sum(theta_quiet)+sum(alpha_quiet));
loud_ratio = sum(theta_loud)/(sum(theta_loud)+sum(alpha_loud));
real_diff = quiet_ratio - loud_ratio;

% Calculate null distribution for difference
nreps = 100;
null_diff = nan(nreps,1);
null_ratio = nan(2*nreps,1);
for rep = 1:nreps
    if mod(rep,10) == 0
        fprintf('%d\n', rep);
    end
    [perm_trial1, perm_trial2] = permute_trials(quiet_data, loud_data);
    [theta1, alpha1] = compare_power_variance(perm_trial1);
    [theta2, alpha2] = compare_power_variance(perm_trial2);
    ratio1 = sum(theta1)/(sum(theta1)+sum(alpha1));
    ratio2 = sum(theta2)/(sum(theta2)+sum(alpha2));
    null_diff(rep) = ratio1 - ratio2;
    null_ratio(2*(rep-1)+1:2*rep) = [ratio1, ratio2];
end

% Calculate stats
loc = find(real_diff >= sort(null_diff), 1, 'last');    
if isempty(loc)
    pc = 1/nreps;
elseif loc ~= nreps
    pc = loc/nreps;
else
    pc = 1-(1/nreps); % can't have 100th percentiles in here (which cannot be converted to a z score)
end
p = 1-abs(pc-0.5)*2;
z = norminv(pc, 0, 1);

figure, h=histogram(null_diff); hold on; plot([real_diff, real_diff], [0,max(h.Values)], 'k', 'linewidth', 2);
title({'Null distribution of differences', sprintf('p = %f, z = %f', p, z)});
xlabel('Ratio of drowsy epochs');

figure, h=histogram(null_ratio); hold on;
title('Null distribution');
h1=plot([quiet_ratio, quiet_ratio], [0,max(h.Values)], 'g', 'linewidth',2);
h2=plot([loud_ratio, loud_ratio], [0,max(h.Values)], 'r', 'linewidth',2);
legend([h1,h2], {'Trying to get drowsy', 'Trying to stay alert'}, 'location', 'best');
xlabel('Ratio of drowsy epochs')

end