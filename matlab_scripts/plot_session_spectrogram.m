function [s_tot, w] = plot_session_spectrogram(session_path, fig_axes)

% Get conditions
conditions = dir(session_path);
conditions = string({conditions.name}');
conditions(1:2) = [];

% Collect all data as well
data = cell(numel(conditions),1);

% We might need to ignore some broken channels
% NOTE! We assume that the same set of channels are used in each condition
% for the given session (i.e. cap isn't changed mid-session)
zero_channels = cell(numel(conditions),1);

% Loop through conditions
parfor cond_idx = 1:numel(conditions)
    
    % Get condition and condition info
    cond = fullfile(session_path, conditions{cond_idx});
    info = get_condition_info(cond);
    
    % Get data
    d = get_experiment(cond, info.channels);

    % Filter
    f = butter_filter(d.data(2:end, :), 250, 'bandpass', [0.5,35]);

    % Calculate PSD
    [pxx, fxx] = pwelch(f, 4*250, [], [], 250);
    
    % Get channels with zero power
    zero_channels{cond_idx} = mean(pxx)<1e-5;

    % Find maximum y-axis value
    %[~, idx2] = closest(fxx, 2);
    %maxy = 1.1*max(max(pxx(idx2:end,:)));
    
    % Plot
    %figure; title(strrep(cond, '_', ':')); xlabel('Hz'); ylabel('Power'); hold on;
    %plot(fxx,pxx);
    %plot(fxx, pxx(:,1:5),'b');
    %plot(fxx, pxx(:,6:8),'y');
    %plot(fxx, pxx(:,9:11),'r');
    %plot(fxx, pxx(:,12:13),'k');
    %plot(fxx, pxx(:,14),'g');
    %plot(fxx, mean(pxx,2), 'k', 'linewidth', 2);
    %axis([0, 20, 0, maxy]);
    
    % Collect all data
    data{cond_idx} = f;
end

% Get channels that had zero power in any of the conditions
zero_channels = any(cat(1, zero_channels{:}));
if any(zero_channels)
    warning('Removed %g channels with zero power for %s', sum(zero_channels), session_path);
end

% Calculate spectrogram
data = cat(1, data{:});
s_tot = cell(size(data,2),1);
for channel_idx = 1:size(data,2)
    [s,w,t] = spectrogram(data(:,channel_idx), 250*4, 0, [], 250);
    s_tot{channel_idx} = log(s.*conj(s));
end

% Concatenate data into a matrix
s_tot = cat(3, s_tot{:});

% Plot average PSD over all blocks for each channel
%s_time_avg = squeeze(mean(s_tot,2));
%figure, hold on
%plot(fxx, s_time_avg(:,1:5),'b');
%plot(fxx, s_time_avg(:,6:8),'y');
%plot(fxx, s_time_avg(:,9:11),'r');
%plot(fxx, s_time_avg(:,12:13),'k');
%plot(fxx, s_time_avg(:,14),'g');
%xlabel('Frequency (Hz)'); ylabel('Power');

% Calculate average power in freq/timepoint per channel
s_channel_avg = mean(s_tot(:,:,~zero_channels),3);

% Threshold values
threshold = prctile(s_channel_avg(:), 75);
zero_idxs = s_channel_avg < threshold;
s_channel_avg(zero_idxs) = nan;

% Plot the spectrogram and flip y-axis; use given figure or create a new
% one
if nargin < 2 || isempty(fig_axes)
    fig = figure();
    ax = axes(fig);
else
    ax = fig_axes;
end
imagesc(ax, t/60, w, s_channel_avg);
ax.YDir = 'normal';

% Set axis limits and titles etc
axis([0, t(end)/60, 1,30]);
title(session_path); ylabel('Frequency (Hz)'); xlabel('Time (mins)');

end
