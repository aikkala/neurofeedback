function close_gz_file(streams)

streams.reader.close();
streams.char.close();
streams.inflated.close();
streams.file.close();

end