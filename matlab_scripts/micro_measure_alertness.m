function epochs = micro_measure_alertness(path_to_condition, micro_meas_path)
% Estimate drowsiness level for a given condition

% Make sure necessary paths are set
addpath(genpath('/home/aleksi/Workspace/eeglab14_1_2b'));
addpath('/home/aleksi/Workspace/fieldtrip-20181231');
if nargin < 2 || isempty(micro_meas_path)
    micro_meas_path = '/home/aleksi/Workspace/microMeasAlertness_HumanEEG';
end
addpath(genpath(micro_meas_path));

% Epoch length in seconds; should be four seconds
epoch_length = 4;

% Read condition info
info = get_condition_info(path_to_condition);

% Get sampling rate and sensors
fs = info.downsample_rate;

% Define sensors and corresponding labels
sensors = unique(info.channels);
labels = arrayfun(@(x) string(sprintf('E%d', x)), sensors);

% Some broken sensors were replaced with a nearby sensor, change the labels
% of those sensors so that we can use them with Sri's algorithm
labels = replace_broken_sensor_labels(labels);

% Grab experiment data
d = get_experiment(path_to_condition, sensors);

% Apparently GB might have 1-2 broken sensors in some sessions. We need to
% replace those with other sensors to get anything sensible out of Sri's
% algorithm
d = replace_flat_signals(d, path_to_condition, labels);

% Need to convert into eeglab structure
EEG = convert_to_eeglab(d.data, labels, fs);

% Preprocess
EEG = preprocess(EEG, epoch_length*fs);

% Run Sri's algorithm
if ~isempty(EEG.data)
    epochs = classify_microMeasures(EEG, fullfile(micro_meas_path, 'models', 'model_collec64_.mat'), '128');
else
    epochs = nan;
end
end

function EEG = preprocess(EEG, pnts)

% Filter
EEG = pop_eegfiltnew(EEG, 1, 30);

% Epoch (have to do it manually because events are not defined)
EEG.trials = floor(EEG.pnts / pnts);
EEG.pnts = pnts;
EEG.data = reshape(EEG.data(:, 1:EEG.trials*EEG.pnts), EEG.nbchan, EEG.pnts, EEG.trials);
EEG.times = EEG.times(1:EEG.pnts);
EEG.xmax = EEG.times(end)/1000;

% Remove bad channels
% Interpolate

% Remove bad trials
[EEG, remove_trials] = pop_rejspec(EEG, 1, 'elecrange', 1:14, 'threshold', [-30, 30], 'freqlimits', [15,30], 'method','multitaper');

% If all trials are marked for rejection we can't use this subject
if sum(EEG.reject.rejfreq) == EEG.trials
    warning('No good trials, must skip this subject');
    EEG.data = nan(EEG.nbchan, EEG.pnts, 0);
    EEG.trials = 0;
else
    EEG = pop_select(EEG, 'notrial', remove_trials);
end

end

function d = replace_flat_signals(d, condition_path, labels)

% Check if there are any flat signals
devs = std(d.data);
broken_idxs = find(devs < 1e-6);
broken_labels = labels(broken_idxs);
if numel(broken_idxs) > 0
    warning('Flat signals found from recording %s | broken sensors: %s', condition_path, sprintf('%s ', broken_labels));
    %error('What to do with this subject?');
end

copied_labels = [""];
for broken_label = broken_labels
    if broken_label == "E122"
        alternatives = ["E33","E11"];
    elseif broken_label == "E11"
        alternatives = ["E33","E122"];
    elseif broken_label == "E75"
        alternatives = ["E70","E83"];
    elseif broken_label == "E90"
        alternatives = ["E83"];
    elseif broken_label == "E100"
        alternatives = ["E45", "E108", "E102", "E115"];
    elseif broken_label == "E115"
        alternatives = ["E45", "E108", "E102", "E100"];
    else
        error('Replacement has not been defined for sensor %s', broken_label);
    end
    
    % Remove sensors that've been copied once already from alternatives
    pure_alternatives = setdiff(alternatives, copied_labels);
    if numel(pure_alternatives) > 0
        alternatives = pure_alternatives;
    end

    valid_label = get_valid_sensor(broken_label, broken_labels, alternatives);
    fprintf('Replacing sensor %s with data from sensor %s\n', broken_label, valid_label);
    d.data(:,labels==broken_label) = d.data(:,labels==valid_label);

    % Try not to copy one sensor multiple times
    copied_labels = [copied_labels valid_label];
end

end

function found_label = get_valid_sensor(broken_label, broken_labels, valid_labels)

found_label = "";
for valid_label = valid_labels
    
    if ~ismember(valid_label, broken_labels)
        found_label = valid_label;
        break;
    end
    
end

% Check whether we found a working sensor
if found_label==""
    error('Couldn''t find any good sensors to replace sensor idx %d', broken_label);
end

end