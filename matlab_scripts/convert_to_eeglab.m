function EEG = convert_to_eeglab(data, labels, srate)

% Load template
tmpl = load('/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/matlab_scripts/eeglab_template_struct_geodesic_128.mat');
EEG = tmpl.EEG;

% Fill template with given info
EEG.nbchan = numel(labels);
EEG.srate = srate;
EEG.pnts = size(data,1);
EEG.trials = 1;

% Reformat data array into [channel x sample x trial]
EEG.data = data';

% Set times
EEG.xmin = 0;
EEG.xmax = (EEG.pnts-1)/EEG.srate;
EEG.times = EEG.xmin : 1000/EEG.srate : 1000*EEG.xmax;

% Get rid of unnecessary channels
tmpl_labels = {EEG.chanlocs.labels};
retain = false(numel(EEG.chanlocs),1);
for label = labels
    retain(label==tmpl_labels) = true;
end
EEG.chanlocs = EEG.chanlocs(retain);

end