function [theta, alpha] = calculate_theta_alpha_powers(signals, theta_channels, alpha_channels)

L = 500;
fs = 250;

f = fs*(0:(L/2))/L;
alpha_indices = nearest_idx(9,f):nearest_idx(11,f);
theta_indices = nearest_idx(3,f):nearest_idx(5,f);
%alpha = cell(numel(alpha_channels),1);
%theta = cell(numel(theta_channels),1);
alpha = [];
theta = [];

start_idx = 1;end_idx = L;
while (end_idx < size(signals,1))
    % Calculate alpha
    %for i = 1:numel(alpha_channels)
        %[pxx,f] = pwelch(detrend(signals(start_idx:end_idx,alpha_channels(i))), L, 0, L, fs);
        %alpha{i} = [alpha{i} sum(pxx(alpha_indices))];
    %end
    [pxx,f] = pwelch(detrend(signals(start_idx:end_idx,alpha_channels)), L, 0, L, fs);
    alpha = [alpha; sum(pxx(alpha_indices,:),1)];
    
    % Calculate theta
    %for i = 1:numel(theta_channels)
    %    [pxx,f] = pwelch(detrend(signals(start_idx:end_idx,theta_channels(i))), L, 0, L, fs);
    %    theta{i} = [theta{i} sum(pxx(theta_indices))];
    %end
    [pxx,f] = pwelch(detrend(signals(start_idx:end_idx,theta_channels)), L, 0, L, fs);
    theta = [theta; sum(pxx(theta_indices,:),1)];
    
    start_idx = start_idx+5; end_idx = end_idx + 5;
end

% Plot powers
%theta = cat(1, theta{:})';
%alpha = cat(1, alpha{:})';

end