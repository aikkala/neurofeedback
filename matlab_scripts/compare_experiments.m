function compare_experiments(folder1, folder2)

% Read calibration and experiment logs
log_cal1 = read_gz_file(fullfile(folder1, 'log', 'Calibration.log.gz'));
log_exp1 = read_gz_file(fullfile(folder1, 'log', 'Experiment.log.gz'));
log_cal2 = read_gz_file(fullfile(folder2, 'log', 'Calibration.log.gz'));
log_exp2 = read_gz_file(fullfile(folder2, 'log', 'Experiment.log.gz'));

% Read raw data, and calibration and experiment logs from given folders
d1 = read_gz_dat(fullfile(folder1, 'dat', 'ATBuffer.dat.gz'));
d2 = read_gz_dat(fullfile(folder2, 'dat', 'ATBuffer.dat.gz'));

% Find channel indices for AT ratio
at_index1 = find_mapping(read_gz_file(fullfile(folder1, 'log', 'ATBuffer.log.gz')), 'AT ratio');
at_index2 = find_mapping(read_gz_file(fullfile(folder2, 'log', 'ATBuffer.log.gz')), 'AT ratio');

% Find timestamps when calibrations and experiments start/stop
cal1_timepoints = [find_timepoints(log_cal1, 'Calibration has started'), find_timepoints(log_cal1, 'Calibration has stopped')];
exp1_timepoints = [find_timepoints(log_exp1, 'Tracking has started'), find_timepoints(log_exp1, 'Tracking has stopped')];
cal2_timepoints = [find_timepoints(log_cal2, 'Calibration has started'), find_timepoints(log_cal2, 'Calibration has stopped')];
exp2_timepoints = [find_timepoints(log_exp2, 'Tracking has started'), find_timepoints(log_exp2, 'Tracking has stopped')];

% Find equivalent indices
cal1_idxs = [find(d1.timepoint==cal1_timepoints(1)), find(d1.timepoint==cal1_timepoints(2))];
exp1_idxs = [find(d1.timepoint==exp1_timepoints(1)), find(d1.timepoint==exp1_timepoints(2))];
cal2_idxs = [find(d2.timepoint==cal2_timepoints(1)), find(d2.timepoint==cal2_timepoints(2))];
exp2_idxs = [find(d2.timepoint==exp2_timepoints(1)), find(d2.timepoint==exp2_timepoints(2))];

% Extract at values during calibrations and experiments
cal1 = d1.data(cal1_idxs(1):cal1_idxs(2), at_index1);
exp1 = d1.data(exp1_idxs(1):exp1_idxs(2), at_index1);
cal2 = d2.data(cal2_idxs(1):cal2_idxs(2), at_index2);
exp2 = d2.data(exp2_idxs(1):exp2_idxs(2), at_index2);



% Plot calibration values
figure, hold on;
bins = 0:0.05:max([max(cal1), max(cal2)]);
histogram(cal1, bins); histogram(cal2, bins);

% Plot experiment values
figure, hold on;
bins = 0:0.05:max([max(exp1), max(exp2)]);
h1 = histogram(exp1, bins); h2 = histogram(exp2, bins);

figure, hold on;
plot((h1.BinWidth/2) : h1.BinWidth : h1.BinLimits(2), log(h1.Values), 'bx');
plot((h2.BinWidth/2) : h2.BinWidth : h2.BinLimits(2), log(h2.Values), 'rx');

% Plot scaled experiment values
scaled_exp1 = exp1./median(cal1);
scaled_exp2 = exp2./median(cal2);
figure; hold on;
bins = 0:0.5:max([max(scaled_exp1), max(scaled_exp2)]);
h1 = histogram(scaled_exp1, bins); h2 = histogram(scaled_exp2, bins);

figure, hold on;
plot((h1.BinWidth/2) : h1.BinWidth : h1.BinLimits(2), log(h1.Values), 'bx');
plot((h2.BinWidth/2) : h2.BinWidth : h2.BinLimits(2), log(h2.Values), 'rx');

end