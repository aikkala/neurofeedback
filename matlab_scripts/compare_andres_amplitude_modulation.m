function compare_andres_amplitude_modulation()

path = '/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/data/piloting-2019-03-05/';
path_constant = fullfile(path, 'ajdina-no-modulation-2019-03-05T16:44Z');
path_theta = fullfile(path, 'andres-theta-4hz-2019-03-05T17:07Z');
path_alpha = fullfile(path, 'andres-alpha-9hz-2019-03-05T17:20Z');

compare_modulations(path_theta, path_alpha, path_constant);

end