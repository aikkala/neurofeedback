function avg_abs_error_per_sample = match_timeseries(ts1, ts2)
% MATCH_TIMESERIES Find shorter time series (ts2) from longer one (ts1)

% Make sure ts1 is longer
if numel(ts1) < numel(ts2)
    ts2_tmp = ts2;
    ts2 = ts1;
    ts1 = ts2_tmp;
end

% Determine on which indices we do the matching
idxs = 1:numel(ts1)-numel(ts2);

% Calculate absolute error between time series when ts1 is truncated 
% starting from different indices
e = nan(numel(idxs),1);
for i = 1:numel(idxs)
    idx = idxs(i);
    e(i) = sum(abs(ts2-ts1(idx:idx+numel(ts2)-1))); 
end

% Find indices of ts1 that correspond to ts2
[~,idx] = min(e);
start_idx = idxs(idx);
end_idx = start_idx+numel(ts2)-1;

figure('Position',[100,100,850,950]);
subplot(2,1,1); plot(idxs,e); ylabel('Sum of absolute error'); xlabel('Index'); hold on; plot(idx,e(idx),'ro')
subplot(2,1,2); plot(ts1); hold on; plot(start_idx:end_idx, ts2);

% Calculate average absolute error per sample (for the segment that
% minimises error)
avg_abs_error_per_sample = e(idx)./(end_idx-start_idx+1);

end