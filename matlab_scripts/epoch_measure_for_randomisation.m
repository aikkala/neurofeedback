function epochs = epoch_measure_for_randomisation(filename, varargin)

L = 1000;
epochs = [];

for set_idx = 1:nargin-1
    measure = varargin{set_idx};
    
    k = floor(numel(measure)/L);
    measure = measure(1:L*k);
    measure = reshape(measure, L, k);
    
    epochs = [epochs, measure];
end
    

% All epochs must begin and end with zero (or low values)
%w = hamming(L/10);
%w = w(1:L/20);
%w = [w; ones(L*9/10,1); w(end:-1:1)];
w = hamming(L);
epochs = epochs.*w;

% Remove outliers
max_vals = max(epochs);
out = isoutlier(max_vals, 'quartiles', 'thresholdfactor', 3);
epochs(:,out) = [];

epochs = epochs';

% Save with a space delimiter
dlmwrite(filename, epochs, ' ');

end