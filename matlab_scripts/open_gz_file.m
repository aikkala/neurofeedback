function streams = open_gz_file(file)

% Make sure file exists
if exist(file, 'file') ~= 2
    fprintf('Cannot find file %s\n', file);
    streams = struct();
    return;
end

streams = struct();

% Open java streams
try
    file_stream = javaObject('java.io.FileInputStream', file);
    inflated_stream = javaObject('java.util.zip.GZIPInputStream', file_stream);
    char_stream = javaObject('java.io.InputStreamReader', inflated_stream);
    reader = javaObject('java.io.BufferedReader', char_stream);
catch e
    warning('Warning: Could not open file %s: %s', file, e.message);
    return;
end

% Return streams and reader
streams = struct('file', file_stream, 'inflated', inflated_stream, 'char', char_stream, 'reader', reader);

end