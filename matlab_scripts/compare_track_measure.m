function [means, medians] = compare_track_measure(sessions_path, condition_query)
% Plot tracked measure for each session

% TODO 
% - handle missing conditions / poor queries that result in too many
% conditions returned

% Find the measure
measure_idx = condition_query == "measure";
if sum(measure_idx) == 0
    error('''measure'' must be defined in given condition query');
end
measure = condition_query{find(measure_idx)+1};

% Get sessions
sessions = dir(sessions_path);
sessions = string({sessions.name}');
sessions(1:2) = [];

% Loop through sessions
track = cell(numel(sessions),1);
for sess_idx = 1:numel(sessions)
        
    % Find queried condition
    current_session = fullfile(sessions_path, sessions(sess_idx));
    condition = find_condition(current_session, condition_query{:});
    
    % Grab tracked measure
    current_condition = fullfile(current_session, condition);
    t = get_experiment(current_condition, [], 'TrackBuffer.dat.gz');
    
    % Ignore first three seconds, that contains only initialisation noise
    track{sess_idx} = t.data;
end

% Do a violin plot of ratios
%figure; violin(ratios', 'xlabel', sessions');
%title(measure); xlabel('Date');

means = cellfun(@mean, track);
medians = cellfun(@median, track);

end