function d = get_experiment(folder, sensors, buffer)

% If no sensors provided use them all
if nargin < 2
    sensors = [];
end

% If no buffer provided use DataBuffer.dat.gz
if nargin < 3
    buffer = 'DataBuffer.dat.gz';
end

% Initialise data and log filenames
dat_file = fullfile(folder, 'dat', buffer);
log_file = fullfile(folder, 'log', 'Experiment.log.gz');

% Load data
d = read_gz_dat(dat_file, sensors);

% Load experiment log
log = read_gz_file(log_file);

% Get experiment start and end times
exp_start = find_timestamps(log, 'Experiment has started');
exp_end = find_timestamps(log, 'Experiment has stopped');

% If multiple start/end timestamps are returned, make sure there are equal
% number of starts/ends, and choose the longest duration
if numel(exp_start) > 1 || numel(exp_end) > 1

    % Each experiment start trigger should have a corresponding experiment
    % end trigger
    if numel(exp_start) ~= numel(exp_end)
        error('Experiments starts and ends do not match for %s; there are %d starts and %d ends', folder, numel(exp_start), numel(exp_end));
    end

    % Calculate longest duration
    durations = exp_end - exp_start;
    [max_duration, max_duration_idx] = max(durations);

    warning('Multiple experiments (%d) found for %s, choosing the experiment with longest duration (%.2f seconds)', ...
        numel(exp_start), folder, max_duration/1e6);
    exp_start = exp_start(max_duration_idx);
    exp_end = exp_end(max_duration_idx);
else
    fprintf('Grabbing an experiment with duration %.2f seconds for %s\n', (exp_end-exp_start)/1e6, folder);
end

% Make sure experiment starts earlier than ends
if exp_start >= exp_end
    error('Invalid timestamps for experiment start (%d) and end (%d)', exp_start, exp_end);
end

% If either not found return original data
if isempty(exp_start) || isempty(exp_end)
    warning('Couldn''t parse experiment start and end times from log file (%s), returning all data', log_file);
    return;
end

% Find indices that correspond to exp_start and exp_end timestamps; if
% there are multiple indices return longest range
exp_start_idx = find(d.timestamp==exp_start, 1, 'first');
exp_end_idx = find(d.timestamp==exp_end, 1, 'last');

% Make sure these timestamps are valid
if isempty(exp_start_idx) || isempty(exp_end_idx)
    error('Timestamps in log file (%s) are different than timestamps in data file (%s)', log_file, dat_file);
end

% Extract only experiment from d
d.data = d.data(exp_start_idx:exp_end_idx,:);
d.timestamp = d.timestamp(exp_start_idx:exp_end_idx);

end