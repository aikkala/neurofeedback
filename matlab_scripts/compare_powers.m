function compare_powers(data, frontal, occipital)

% Grab beginning and end of rest period
first = [1,round(0.25*size(data,1))];
last = [round(0.75*size(data,1)), size(data,1)];

% Calculate alpha, theta, and theta/alpha in both datasets, in frontal,
% occipital and all sensors
powers_first = calculate_power(data(first(1):first(2), :), frontal, occipital);
powers_last = calculate_power(data(last(1):last(2), :), frontal, occipital);

% Do some visualisations

% Alpha in frontal, occipital, and all sensors
plot_differences(powers_first, powers_last, 'alpha')

% Theta in frontal, occipital, and all sensors
plot_differences(powers_first, powers_last, 'theta')

% Theta/alpha ratio in frontal, occipital, and all sensors
plot_differences(powers_first, powers_last, 'ratio')

% Do theta/alpha ratio with frontal theta and occipital alpha
plot_histogram(powers_first.frontal.theta./powers_first.occipital.alpha, ...
    powers_last.frontal.theta./powers_last.occipital.alpha, 'frontal+occipital', 'ratio');

end


function powers = calculate_power(data, frontal, occipital)

fs = 250;
L = 500;

% Make a list of all sensors
sensors = struct('frontal', frontal, 'occipital', occipital, 'all', [frontal; occipital]);

% Loop over those sensors
sets = fieldnames(sensors);

% Find alpha and theta indices
freqs = fs*(0:(L/2))/L;
alpha = [8,12];
theta = [3,5];
alpha_indices = (nearest_idx(alpha(1), freqs):nearest_idx(alpha(2), freqs))';
theta_indices = (nearest_idx(theta(1), freqs):nearest_idx(theta(2), freqs))';

powers = struct();

for set = sets'
    
    set = char(set);
    selected = sensors.(set);
    powers.(set) = struct('alpha', [], 'theta', [], 'ratio', []);
    
    start_idx = 1; end_idx = L;
    while (end_idx < size(data,1))
        
        % Take average PSD over sensors
        pxx = mean(pwelch(detrend(data(start_idx:end_idx,selected)),L,0,L,fs),2);
                
        % Calculate powers
        powers.(set).alpha = [powers.(set).alpha; sum(pxx(alpha_indices))];
        powers.(set).theta = [powers.(set).theta; sum(pxx(theta_indices))];
        powers.(set).ratio = [powers.(set).ratio; powers.(set).theta(end)./powers.(set).alpha(end)];
        
        start_idx = start_idx + L;
        end_idx = end_idx + L;
    end
end

end


function plot_differences(powers_first, powers_last, band)

% Plot differences in frontal, occipital, and all sensors
sets = fieldnames(powers_first);

for set = sets'

    set = char(set);
    
    % Get the data
    first = powers_first.(set).(band);
    last = powers_last.(set).(band);
    
    % Plot
    plot_histogram(first, last, set, band);
    
end


end


function plot_histogram(first, last, set, band)
    
% Remove outliers
first_out = isoutlier(first,'quartiles');
first(first_out) = [];
last_out = isoutlier(last,'quartiles');
last(last_out) = [];

figure, hold on;
histogram(first); histogram(last);

% Try to calculate some metrics as well
p = ranksum(first, last);
perc = (median(last)./median(first))-1;

title({
    sprintf('%s: %s', band, set)
    sprintf('ranksum p = %f', p)
    sprintf('difference in medians (percentage) = %f', perc)
    });

legend(...
sprintf('First quarter, N = %d (#outliers = %d)', numel(first), sum(first_out)), ...
sprintf('Last quarter, N = %d (#outliers = %d)', numel(last), sum(last_out)) ...
);

end

