

% Load data (if not loaded already)
if exist('quieter', 'var') ~= 1
    quieter = read_gz_dat('/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/data/piloting-2019-02-04/ajdina-ta-quieter/dat/DataBuffer.dat.gz', [5,6,12,70,75,83]);
    quiet_data = quieter.data(34818-499:185932,:);
    quiet_calibration = quiet_data(11037-499:26164,:);
end
if exist('louder', 'var') ~= 1
    louder = read_gz_dat('/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/data/piloting-2019-02-04/ajdina-ta-louder/dat/DataBuffer.dat.gz', [5,6,12,70,75,83]);
    loud_data = louder.data(23556-499:174680,:);
    loud_calibration = loud_data(2412-499:17657,:);
end

% Do the comparisons
[real_diff, null_diff, pc, p, z] = power_variance_permutation_test(quiet_data, loud_data);