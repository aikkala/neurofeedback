
sensors = unique([44, 107, 101, 114, 99, 32, 121, 10, 69, 74, 82, 35, 103, 89]) + 1;

ignore_end = 10; % Ignore last 10 seconds
ignore_first = 5; % Ignore first 5 seconds
fs = 250;

% Load data (if not loaded already)
if exist('quieter', 'var') ~= 1
    quieter = read_gz_dat('/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/data/piloting-2019-02-21/fluc_ratio_quieter-2019-02-21T10:54Z/dat/DataBuffer.dat.gz', sensors);
    %quieter = read_gz_dat('/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/data/piloting-2019-02-21/fluc_ratio_quieter_again-2019-02-21T11:22Z/dat/DataBuffer.dat.gz', sensors);
    
    % Estimate calibration and experiment start and end
    cal_start = ignore_first*fs;
    cal_end = cal_start + 60*fs;
    exp_end = size(quieter.data,1) - ignore_end*fs;
    exp_start = exp_end - 10*60*fs;

    % Extract calibration and experiment
    quiet_experiment = quieter.data(exp_start:exp_end,:);
    quiet_calibration = quiet_experiment(cal_start:cal_end,:);
end
if exist('louder', 'var') ~= 1
    louder = read_gz_dat('/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/data/piloting-2019-02-21/fluc_ratio_louder-2019-02-21T11:09Z/dat/DataBuffer.dat.gz', sensors);

    % Estimate calibration and experiment start and end
    cal_start = ignore_first*fs;
    cal_end = cal_start + 60*fs;
    exp_end = size(louder.data,1) - ignore_end*fs;
    exp_start = exp_end - 10*60*fs;

    % Extract calibration and experiment
    loud_experiment = louder.data(exp_start:exp_end,:);
    loud_calibration = louder.data(cal_start:cal_end,:);

end

% Do the comparisons
[real_diff, null_diff, pc, p, z] = power_variance_permutation_test(quiet_experiment, loud_experiment);