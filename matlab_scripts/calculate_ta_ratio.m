function [ratio_detrend, ratio_filter] = calculate_ta_ratio(condition)

% Get condition info
info = get_condition_info(condition);

% Read data
d = get_experiment(condition, info.channels);

L = 500;
fs = info.downsample_rate;

% Find alpha and theta indices
freqs = fs*(0:(L/2))/L;
alpha = [8,12];
theta = [3,5];
alpha_indices = (nearest_idx(alpha(1), freqs):nearest_idx(alpha(2), freqs))';
theta_indices = (nearest_idx(theta(1), freqs):nearest_idx(theta(2), freqs))';

% Calculate TA ratio with detrending and with filtering
ratio_detrend = [];
ratio_filter = [];
start_idx = 1; end_idx = L;
while (end_idx < size(d.data,1))
    
    detrended = detrend(d.data(start_idx:end_idx,:));
    filtered = butter_filter(d.data(start_idx:end_idx,:), fs, 'bandpass', [0.5,30]);
    
    % Calculate power in detrended signal
    pxx_detrended = pwelch(detrended,L,0,L,fs);
    ratio_detrend(end+1) = sum(pxx_detrended(theta_indices)) / sum(pxx_detrended(alpha_indices));
    
    % Calculate power in filtered signal
    pxx_filtered = pwelch(filtered,L,0,L,fs);
    ratio_filter(end+1) = sum(pxx_filtered(theta_indices)) / sum(pxx_filtered(alpha_indices));
    
    start_idx = start_idx + 5;
    end_idx = end_idx + 5;
end


end