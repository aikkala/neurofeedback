function errors = check_data_correspondence(mff_path, session_path)
% CHECK_DATA_CORRESPONDENCE Checks that data recorded with Netstation is
% equivalent to data recorded with the NeuroFeedback software. Or actually
% just creates a bunch of plots that you have to visually inspect.
%
% NOTE! Especially later in the recordings an occasional sample is dropped here 
% and there in neurofeedback recordings. This happens most likely because we're
% doing online calculations, and thus drop samples that arrive late (e.g. if a 
% sample with timestamp 1000 arrives, then timestamp 1008, and then timestamp
% 1004, we will ignore the latest timestamp since it arrives after timestamp 
% 1008; units of timestamps in this example don't matter, but they could be e.g.
% milliseconds). Online calculations use two second windows, and (at least most
% of our) offline calculations use windows of two to four seconds, so the odd
% dropped sample shouldn't introduce a huge error.

% Load fieldtrip
addpath(genpath('/home/aleksi/Workspace/software/fieldtrip-20181116'));

% Load MFF data
hdr = ft_read_header(mff_path, 'headerformat', 'egi_mff_v3');
mff = ft_read_data(mff_path, 'header', hdr);

% Remove fieldtrip, messes up figures
rmpath(genpath('/home/aleksi/Workspace/software/fieldtrip-20181116'));

% Load corresponding data that's been recorded with neurofeedback software
conditions = dir(session_path);
conditions = string({conditions.name}');
conditions(1:2) = [];

% Get average absolute error per sample for each condition and channel
errors = cell(numel(conditions),1);

% Go through each condition and check for matching data
for condition_idx = 1:numel(conditions)

    % Read data for this condition
    condition = conditions(condition_idx);
    info = get_condition_info(fullfile(session_path, condition));
    d = get_experiment(fullfile(session_path, condition), info.channels);
    
    % Go through channels and make sure they are similar in MFF and in our
    % recordings
    channel_errors = nan(numel(info.channels),1);
    for channel_idx = 1:numel(info.channels)
        channel_errors(channel_idx) = match_timeseries(mff(info.channels(channel_idx),:)', d.data(:,channel_idx));
        title(sprintf('Channel %g', info.channels(channel_idx)));
    end
    
    errors{condition_idx} = channel_errors;
end

end
