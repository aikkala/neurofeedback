function idx = nearest_idx(value, vector)
[~, idx] = min(abs(vector-value));
end