function txt = read_gz_file(file)
% Reads a GZIP file as strings

% Try to open gzip file
streams = open_gz_file(file);

% Don't continue if reader has not been opened
if ~isfield(streams, 'reader')
    txt = [];
    return;
end

% Read lines as strings
initialised_size = 100;
current_line = 1;
txt = strings(initialised_size,1);

% Ignore header (first rows that begin with a #)
while true
    line = streams.reader.readLine();
    c = char(line);
    if isempty(line) || c(1) ~= '#'
        break;
    end
end

% Loop through rest of the file
while ~isempty(line)
    % Make txt larger if needed
    if current_line >= initialised_size
        initialised_size = initialised_size*2;
        txt_tmp = strings(initialised_size,1);
        txt_tmp(1:current_line-1) = txt(1:current_line-1);
        txt = txt_tmp;
        clear txt_tmp;
    end
    
    txt(current_line) = line;
    line = streams.reader.readLine();
    current_line = current_line+1;
end

% Close streams
close_gz_file(streams);

% Remove empty lines from txt
txt(current_line:end) = [];

end