function channel = find_mapping(txt, metric)

% Return empty array if a mapping is not found
channel = [];

% Loop through lines in txt and find which channel corresponds to queried
% metric

for msg = txt'
    if contains(msg, sprintf("corresponds to '%s'", metric))
        % Parse this line
        split = strsplit(msg, " ");
        try
            % Add 1 because matlab indexing
            channel = str2double(split(5))+1;
        catch ex
            fprintf('Couldn''t parse channel from message %s', msg);
            rethrow(ex);
        end
        
        return;
    end
end

end