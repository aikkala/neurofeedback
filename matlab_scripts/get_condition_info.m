function info = get_condition_info(session_path)
% Parses a ConditionInfo.log.gz file for a given recording

% Create a path to the CondifionInfo file
filename = fullfile(session_path, 'log', 'ConditionInfo.log.gz');

% Read condition info
txt = read_gz_file(filename);

if isempty(txt)
    warning('Failed to read ConditionInfo for recording %s', session_path);
    info = [];
    return;
end

% Parse lines into a struct
info = struct();

% parser defines how we parse each key
parser = struct( ...
    'session', struct('handle', @parse_string, 'args', []), ...
    'session_start', struct('handle', @parse_datetime, 'args', []), ...
    'goal', struct('handle', @parse_string, 'args', []), ...
    'measure', struct('handle', @parse_string, 'args', []), ...
    'source', struct('handle', @parse_string, 'args', []), ...
    'cap', struct('handle', @parse_string, 'args', []), ...
    'channels', struct('handle', @parse_vec, 'args', 14), ...
    'audio', struct('handle', @parse_string, 'args', []), ...
    'randomise', struct('handle', @parse_string, 'args', []), ...
    'amplitude_alteration', struct('handle', @parse_string, 'args', []), ...
    'amplitude_modulation', struct('handle', @parse_vec, 'args', 1), ...
    'downsample_rate', struct('handle', @parse_vec, 'args', 1), ...
    'psd_window_size', struct('handle', @parse_vec, 'args', 1), ...
    'psd_calculation_rate', struct('handle', @parse_vec, 'args', 1), ...
    'track_history_window_size', struct('handle', @parse_vec, 'args', 1), ...
    'temporal_smoothing_window_size', struct('handle', @parse_vec, 'args', 1), ...
    'timeout', struct('handle', @parse_vec, 'args', 1), ...
    'audio_maximum_amplitude', struct('handle', @parse_vec, 'args', 1), ...
    'audio_boundaries', struct('handle', @parse_vec, 'args', 3), ...
    'session_end', struct('handle', @parse_datetime, 'args', []) ...
);

% Loop through retrieved lines and parse them
for line = txt'
    
    % Find out how to parse this line
    [func, key, value, args] = find_function(parser, line);
    
    % Skip this line if no match was found from parser
    if isempty(func)
        continue;
    end
    
    % Parse given value
    if isempty(args)
        info.(key) = func(value);
    else
        info.(key) = func(value, args);
    end
end

end

function str = parse_string(value)
% Just get rid of spaces
str = deblank(value);
end

function dt = parse_datetime(value)
% Transform string into a datetime object
dt = datetime(strrep(strrep(value,'T',' '), 'Z ', ''), 'InputFormat', 'yyyy-MM-dd HH:mm:ss');
end

function vec = parse_vec(value, num)
% Parse string into a vector
formatter = '';
for i = 1:num
    formatter = sprintf('%s%%f ', formatter);
end
vec = textscan(value, formatter);
vec = cell2mat(vec);
end

function [func, key, value, args] = find_function(parser, line)
% Parse key and value from line
p = split(line, ': ');
key = lower(strrep(p(1), ' ', '_'));
value = p(2);

% Find corresponding function and arguments from parser
if ~isfield(parser, key)
    warning('A parsing function not defined for [%s]', key);
    func = [];
    key = [];
    value = [];
    args = [];
    return;
end
func = parser.(key).handle;
args = parser.(key).args;
end