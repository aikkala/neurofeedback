function [peaks, confs] = compare_alpha_peaks(sessions_path, condition_query)
% Return alpha peak frequencies

% TODO 
% - handle missing conditions / poor queries that result in too many
% conditions returned

% Get sessions
sessions = dir(sessions_path);
sessions = string({sessions.name}');
sessions(1:2) = [];

% Loop through sessions
peaks = nan(numel(sessions),1);
confs = nan(size(peaks));
for sess_idx = 1:numel(sessions)
        
    % Find queried condition
    current_session = fullfile(sessions_path, sessions(sess_idx));
    condition = find_condition(current_session, condition_query{:});
    
    % Grab condition info
    current_condition = fullfile(current_session, condition);
    info = get_condition_info(current_condition);
    
    % Grab data
    d = get_experiment(current_condition, info.channels);
    
    % Get alpha peak frequency and confidence
    out = find_alpha_peak_frequency(d.data, info.downsample_rate, false);
    peaks(sess_idx) = out(1);
    confs(sess_idx) = out(2);
end


end