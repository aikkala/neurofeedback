function [theta_trials, alpha_trials] = compare_power_variance(data)

L = 500;
fs = 250;

% Loop through data
% Find alpha and theta indices
%freqs = fs*(0:(L/2))/L;
alpha = [8,12];
theta = [3,5];
broadband = [0.5, 40];
alpha_indices = [];
theta_indices = [];
var_alpha = {};
var_theta = {};
broadband_indices = [];

% Calculate power envelopes

% Loop over signals
for signal_idx = 1:size(data,2)

    start_idx = 1; end_idx = L;
    filtered = butter_filter(data(:,signal_idx), fs, 'bandpass', [0.5, 40]);

    theta_power = {};
    alpha_power = {};
    broadband_power = {};
    
    while (end_idx <= size(data,1))
    
        [s,f] = spectrogram(filtered(start_idx:end_idx), L/5, L/10, [], fs);
        %[s,f] = spectrogram(filtered(start_idx:end_idx), 32, 27, [], fs);
        s = s.*conj(s);

        if isempty(alpha_indices)
            alpha_indices = (nearest_idx(alpha(1), f):nearest_idx(alpha(2), f))';
            theta_indices = (nearest_idx(theta(1), f):nearest_idx(theta(2), f))';
            broadband_indices = (nearest_idx(broadband(1), f):nearest_idx(broadband(2), f))';
        end
        
        
        theta_power{end+1} = sum(s(theta_indices,:));

        alpha_power{end+1} = sum(s(alpha_indices,:));

        broadband_power{end+1} = sum(s(broadband_indices,:));
        
        start_idx = start_idx + L;
        end_idx = end_idx + L;

    end
    
    theta_power = cat(1, theta_power{:})';
    alpha_power = cat(1, alpha_power{:})';
    broadband_power = cat(1, broadband_power{:})';
    
    % Calculate variance explained (?)
    var_alpha{end+1} = 100 - 100*var(broadband_power-alpha_power)./var(broadband_power);
    var_theta{end+1} = 100 - 100*var(broadband_power-theta_power)./var(broadband_power);
end

% Concatenate variances explained
var_alpha = cat(1, var_alpha{:});
var_theta = cat(1, var_theta{:});

% Threshold variances
var_alpha(var_alpha < 20) = 0;
var_theta(var_theta < 20) = 0;

max_alpha = var_alpha > var_theta;
max_theta = var_theta > var_alpha;

t_alpha = sum(max_alpha);
t_theta = sum(max_theta);

alpha_trials = t_alpha > t_theta;
theta_trials = t_theta > t_alpha;

end