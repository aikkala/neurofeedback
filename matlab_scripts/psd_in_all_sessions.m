function [powers, conditions] = psd_in_all_sessions(subjects)
% Estimate power in all subjects and in all conditions

% Define subjects
if nargin < 1
    subjects = ["AG", "CF", "CW", "GB", "JS"];
end
data_path = "/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/data/recorded/subjects/";

% Define conditions
conditions = { ...
    {'measure', 'theta-alpha ratio', 'goal', 'increase', 'audio', 'on', 'randomise', 'off'}
    {'measure', 'theta-alpha ratio', 'goal', 'decrease', 'audio', 'on', 'randomise', 'off'}
    {'measure', 'theta-alpha variance', 'goal', 'increase', 'audio', 'on', 'randomise', 'off'}
    {'measure', 'theta-alpha variance', 'goal', 'decrease', 'audio', 'on', 'randomise', 'off'}
    {'randomise', 'on', 'audio', 'on', 'goal', 'increase'}
    {'randomise', 'on', 'audio', 'on', 'goal', 'decrease'}
    {'audio', 'off', 'goal', 'increase'}
    {'audio', 'off', 'goal', 'decrease'}
};

% Estimate power in each session for all subjects
par_subject = cell(numel(subjects),1);
parfor subject_idx = 1:numel(subjects)
    
    % Grab a subject
    subject = subjects{subject_idx};
    subject_ratios = cell(numel(conditions),1);

    % Define here which sensors will be used. Use the expected labels (of 
    % possibly broken sensors) instead of the labels that were actually used 
    % (e.g. "E109" instead of "E115" which was used in some caps)
    if subject == "GB"
        frontal = ["E33"];
        occipital = ["E70"];
        all_but_weak = ["E45", "E108", "E102", "E115", "E100", "E33", "E70", "E90"];
    else
        frontal = ["E33", "E122"];
        occipital = ["E70", "E75", "E83"];
        all_but_weak = ["E45", "E108", "E102", "E115", "E100", "E33", "E122", "E70", "E75", "E83", "E90"];        
    end
    
    % Go through all conditions
    for condition_idx = 1:numel(conditions)
        subject_ratios{condition_idx} = compare_psd_epochs(fullfile(data_path, subject), conditions{condition_idx}, ...
            [3,5; 8,12; 13,21; 21,30], {frontal, occipital, all_but_weak, all_but_weak});
    end
    
    % Save ratios into a struct
    %par_subject{subject_idx} = permute(cat(3, subject_ratios{:}), [2,3,1]);
    par_subject{subject_idx} = subject_ratios;
end

% Transform cells into a struct (if there are multiple subjects)
if numel(par_subject) == 1
    powers = par_subject{1};
else
    powers = cell2struct(par_subject, subjects, 1);
end

% Convert conditions into a string array, easier to read
conditions = string(cellfun(@(x) strjoin(x, ' / '), conditions, 'uniformoutput', false));

end