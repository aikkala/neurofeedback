function plot_first_last_session_spectrograms()
% Loop through all subjects and plot their first and last sessions'
% spectrograms

subjects_folder = '/home/aleksi/Workspace/eclipse-workspace/NeuroFeedback/data/recorded/subjects';
cd(subjects_folder);
subjects = ["AG", "CF", "CW", "GB", "JS"];

for subject = subjects
    
    % Get all sessions for this subject
    folders = dir(fullfile(subjects_folder, subject));
    folders(1:2) = [];
    sessions = string({folders.name}');

    % Create a figure
    figure('Position', [100,100,1000,250]);
    
    % Plot spectrogram for first sessions
    ax = subplot(1,2,1);
    plot_session_spectrogram(fullfile(subject, sessions(1)), ax);
    
    % Plot spectrogram for last session
    ax = subplot(1,2,2);
    plot_session_spectrogram(fullfile(subject, sessions(end)), ax);

end

end