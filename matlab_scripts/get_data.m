function d = get_data(folder, sensors)

% If no sensors provided use them all
if nargin < 2
    sensors = [];
end

filename = 'DataBuffer.dat.gz';

% Check if 'DataBuffer.dat.gz' is in this folder
if exist(filename, 'file') ~= 2
    % If not, go into 'dat' subfolder
    d = read_gz_dat(fullfile(folder, 'dat', filename), sensors);
else
    d = read_gz_dat(fullfile(folder, filename), sensors);
end

end