/**@file
 *  Shared buffer for multiple threads
 *  @author Aleksi Ikkala
 *  @date 2018/12/14
 *  @remarks
 */

#include "SharedBuffer.h"

#include "fmt/format.h"

#include <cstring>
#include <numeric>
#include <cmath>

SharedBuffer::SharedBuffer(const uint64_t num_channels, const uint64_t buffer_size, const std::string buffer_name, Recorder* recorder)
    : buffer_name(buffer_name), num_channels(num_channels), buffer_size(buffer_size), recorder(recorder) {

    sample_count = timestamp = num_blocks = expected_sample_rate = 0;

    // Allocate memory to buffer
    buffer = new double*[num_channels];
    for (uint64_t channel = 0; channel < num_channels; ++channel) {
        buffer[channel] = new double[buffer_size];
        // Make sure everything is initialised to zero
        for (uint64_t sample = 0; sample < buffer_size; ++sample) {
            buffer[channel][sample] = 0;
        }
    }

    // Keep a running sum of each channel, easier to calculate mean etc
    running_sum = new double[num_channels];
    for (uint64_t channel_idx = 0; channel_idx < num_channels; ++channel_idx) {
        running_sum[channel_idx] = 0;
    }

    // Initialise weights in case someone wants to use a weighted average
    weights.resize(buffer_size);
    double weights_sum = 0;
    double threshold = 6;
    for (uint64_t sample_idx = 0; sample_idx < weights.size(); ++sample_idx) {
        double x = sample_idx*(threshold/weights.size());
        weights[weights.size()-(sample_idx+1)] = 1/(sqrt(2*M_PI)) * exp((-x*x)/2);
        weights_sum += weights[weights.size()-(sample_idx+1)];
    }

    // Need to normalise weights so that they sum to one
    std::transform(weights.begin(), weights.end(), weights.begin(),
                   [weights_sum](double sample) -> double{ return (sample/weights_sum); });

    // By default don't record data (but do record logs)
    record = record_channels = false;
    log_filename = buffer_name + ".log";
    dat_filename = buffer_name + ".dat";

    if (recorder) {
        recorder->open_file(log_filename, "log");
        recorder->add_string_to_queue(log_filename, "# timestamp message");
    }

    // Log estimated sample rate every 30 seconds
    log_interval = 30000000; // in microsecodns
    previous_log_time = previous_sample_count = 0;

}

SharedBuffer::~SharedBuffer() {
    // Deallocate memory
    for (uint16_t channel = 0; channel < num_channels; ++channel) {
        delete [] buffer[channel];
    }
    delete [] buffer;

    delete [] running_sum;
}

void SharedBuffer::add_sample(const uint64_t& channel, const double& sample) {
    // Set mutex guard to make sure other threads aren't writing to buffer at the same time
    std::lock_guard<std::mutex> guard(mutex);

    // Remove oldest (first) element from running sum, and add latest (last) element to running sum
    running_sum[channel] = running_sum[channel] - buffer[channel][0] + sample;

    // We need to push back all values by one, and insert the newest value to idx=buffer_size
    std::memmove(buffer[channel], buffer[channel]+1, sizeof(double)*(buffer_size-1));
    buffer[channel][buffer_size-1] = sample;
}

void SharedBuffer::add_sample(const std::string& channel_name, const double& sample) {
    add_sample(mappings[channel_name], sample);
}

void SharedBuffer::copy_channel(const uint64_t& channel, double* copy, const std::string process) {
    // Set mutex guard to make sure other threads aren't writing to buffer while reading it
    std::lock_guard<std::mutex> guard(mutex);

    // Call private method now that we have set the mutex
    _copy_channel(channel, copy, process);
}

void SharedBuffer::_copy_channel(const uint64_t& channel, double* copy, const std::string process) {
    // Note: lock guard should be set before calling this function

    // Deep copy a channel into the given array
    //std::memcpy(copy, buffer[channel], sizeof(double)*buffer_size);

    // Subtract mean or divide with sum if necessary
    double div = 1;
    double mean = 0;
    if (process == "zero_mean") {
        mean = running_sum[channel] / buffer_size;
    } else if (process == "sum_normalise" && abs(running_sum[channel]) > 1e-10) {
        div = running_sum[channel];
    }

    for (uint64_t sample = 0; sample < buffer_size; ++sample) {
        copy[sample] = (buffer[channel][sample] - mean) / div;
    }
}

void SharedBuffer::copy_channel(const std::string& channel_name, double* copy, const std::string process) {
    copy_channel(mappings[channel_name], copy, process);
}

double SharedBuffer::get_sample(const uint64_t& channel, const uint64_t& sample) {
    std::lock_guard<std::mutex> guard(mutex);
    return buffer[channel][sample];
}

double SharedBuffer::latest_sample(const uint64_t& channel) {
    std::lock_guard<std::mutex> guard(mutex);
    return buffer[channel][buffer_size-1];
}

double SharedBuffer::latest_sample(const std::string& channel_name) {
    return latest_sample(mappings[channel_name]);
}

void SharedBuffer::copy_buffer(double** copy, std::string process) {
    // Set mutex guard to make sure other threads aren't writing to buffer while reading it
    std::lock_guard<std::mutex> guard(mutex);

    // Now that mutex is set call the private function to copy channels
    for (uint64_t channel = 0; channel < num_channels; ++channel) {
        _copy_channel(channel, copy[channel], process);
    }
}

void SharedBuffer::update(const uint64_t& channel, const std::vector<double>& samples) {
    // Set mutex guard to make sure other threads aren't writing to buffer at the same time
    std::lock_guard<std::mutex> guard(mutex);

    std::copy(samples.begin(), samples.end(), buffer[channel]);
    running_sum[channel] = std::accumulate(samples.begin(), samples.end(), 0.0);
}

void SharedBuffer::update(const std::string& channel_name, const std::vector<double>& samples) {
    update(mappings[channel_name], samples);
}

double SharedBuffer::get_sum(const uint64_t& channel) {
    // Set mutex guard to make sure other threads aren't writing to buffer at the same time
    std::lock_guard<std::mutex> guard(mutex);
    return running_sum[channel];
}

double SharedBuffer::get_sum(const std::string& channel_name) {
    return get_sum(mappings[channel_name]);
}

double SharedBuffer::get_average(const uint64_t& channel) {
    // Set mutex guard to make sure other threads aren't writing to buffer at the same time
    std::lock_guard<std::mutex> guard(mutex);
    return running_sum[channel]/buffer_size;
}

double SharedBuffer::get_average(const std::string& channel_name) {
    return get_average(mappings[channel_name]);
}

double SharedBuffer::get_weighted_average(const uint64_t& channel) {
    // Set mutex guard to make sure other threads aren't writing to buffer at the same time
    std::lock_guard<std::mutex> guard(mutex);

    double weighted_avg = 0;
    for (uint64_t sample_idx = 0; sample_idx < buffer_size; ++sample_idx) {
        weighted_avg += weights[sample_idx] * buffer[channel][sample_idx];
    }

    return weighted_avg;
}

double SharedBuffer::get_weighted_average(const std::string& channel_name) {
    return get_weighted_average(mappings[channel_name]);
}



uint64_t SharedBuffer::get_timestamp() {
    std::lock_guard<std::mutex> guard(timestamp_mutex);
    return timestamp;
}

void SharedBuffer::set_mappings(const std::vector<std::string> channel_names) {
    if (channel_names.size() != num_channels) {
        recorder->add_string_to_queue(log_filename, timestamp,
                                      "[SharedBuffer::add_mappings] Error while setting mappings: There must be one name per channel");
    }

    for (uint64_t channel_idx = 0; channel_idx < num_channels; ++channel_idx) {
        add_mapping(channel_idx, channel_names[channel_idx]);
    }

    mappings_keys = channel_names;
}

void SharedBuffer::set_mappings(const std::set<std::string> channel_names) {
    std::vector<std::string> vec(channel_names.begin(), channel_names.end());
    set_mappings(vec);
}

void SharedBuffer::add_mapping(const uint64_t channel_idx, const std::string name) {
    mappings[name] = channel_idx;
    if (recorder) {
        recorder->add_string_to_queue(log_filename, timestamp, fmt::format("Added mapping: channel {} corresponds to '{}'", channel_idx, name));
    }
}

bool SharedBuffer::has_mapping(const std::string& name) {
    auto it = mappings.find(name);
    if (it != mappings.end()) {
        return true;
    } else {
        return false;
    }
}

void SharedBuffer::notify(const uint64_t& timestamp) {
    {
        // Not really sure whether this lock_guard is needed (or timestamp_mutex in general)
        std::lock_guard<std::mutex> guard(timestamp_mutex);

        // Increase sample count
        ++sample_count;

        // Set timestamp
        this->timestamp = timestamp;
    }

    // Notify subscribers that buffer has been updated
    cv.notify_all();

    // Record data
    if (record) {
        // Make sure no new data is pushed into buffer while pushing to recorder
        // lock_guard is kind of unnecessary here, since this function should always be used
        // in the same thread as where sample reading/updating happens (and thus blocks that thread)
        std::lock_guard<std::mutex> guard(mutex);

        if (record_channels) {
            // Loop through each channel and push their data to recorder
            for (uint64_t channel = 0; channel < num_channels; ++channel) {
                std::vector<double> vec(buffer[channel], buffer[channel]+buffer_size);
                recorder->add_vector_to_queue(dat_filename, this->timestamp, vec, fmt::format("channel_{}", channel));
            }

        } else {
            // Grab newest samples from each channel and push that to recorder
            std::vector<double> vec(num_channels);
            for (uint64_t channel = 0; channel < num_channels; ++channel) {
                vec[channel] = buffer[channel][buffer_size-1];
            }
            recorder->add_vector_to_queue(dat_filename, this->timestamp, vec, "all_channels");
        }
    }

    // Log debug statistics
    if (recorder && (timestamp >= previous_log_time + log_interval)) {

        // Initialise previous_batch_time when entering this clause for the first time
        if (previous_log_time == 0) {
            previous_log_time = timestamp;
            previous_sample_count = get_sample_count();
        } else {
            uint64_t current_sample_count = get_sample_count();
            double time_diff = (timestamp - previous_log_time) / 1000000.0;
            double estimated_sample_rate = (current_sample_count - previous_sample_count) / time_diff;
            previous_log_time = timestamp;
            previous_sample_count = current_sample_count;

            // Check whether this is close to expected sampling rate (if given)
            if (expected_sample_rate > 0 && abs(estimated_sample_rate-expected_sample_rate) > 0.01*expected_sample_rate) {
                std::string msg = fmt::format("[{}] Warning: estimated sample rate ({} Hz) is different from expected sample rate ({} Hz)",
                                              buffer_name, estimated_sample_rate, expected_sample_rate);
                printf("\n%s\n", msg.c_str());
                recorder->add_string_to_queue(log_filename, this->timestamp, msg);
            }

            // Log to a file
            std::string msg = fmt::format("[{}] Estimated sample rate from past {} seconds: {} Hz", buffer_name, time_diff, estimated_sample_rate);
            recorder->add_string_to_queue(log_filename, this->timestamp, msg);

        }
    }

    // Wait here if updates have been blocked
    uint64_t wait_time = 0;
    while (get_num_blocks()) {
        if (wait_time > 1e6) {
            fmt::print("Data updates have been stopped in buffer {} for a long time, have you forgotten to resume updates?\n", buffer_name);
            wait_time = 0;
        }
        ++wait_time;
    };
}


void SharedBuffer::block_updates() {
    std::lock_guard<std::mutex> guard(block_mutex);
    ++num_blocks;
}

void SharedBuffer::resume_updates() {
    std::lock_guard<std::mutex> guard(block_mutex);
    if (num_blocks) --num_blocks;
}

uint64_t SharedBuffer::get_num_blocks() {
    std::lock_guard<std::mutex> guard(block_mutex);
    return num_blocks;
}

bool SharedBuffer::wait_for_update(uint64_t& next_update_timestamp, const uint64_t& update_interval, const uint64_t& timeout) {

    std::unique_lock<std::mutex> ul(cv_mutex);
    if (!cv.wait_for(ul, std::chrono::seconds(timeout), [this, &next_update_timestamp, &update_interval] {

                     // Block data updates while we're checking whether we should continue or not
                     block_updates();

                     // Get current timestamp
                     uint64_t current_timestamp = get_timestamp();

                     // Check whether we should continue or not
                     if (current_timestamp >= next_update_timestamp) {
                         // Calculate next update timestamp
                         uint64_t increase_coeff = ((current_timestamp - next_update_timestamp)/update_interval) + 1;
                         next_update_timestamp += increase_coeff*update_interval;
                         return true;
                     } else {
                         resume_updates();
                         return false;
                     }}))
    {
        // If timeout has been reached return false
        return false;
    }

    // Unlock the cv_mutex
    ul.unlock();

    return true;
}

void SharedBuffer::set_expected_sample_rate(const uint64_t& expected_sample_rate) {
    this->expected_sample_rate = expected_sample_rate;
}

void SharedBuffer::start_recording(const std::string type, std::vector<std::string> labels) {
    recorder->open_file(dat_filename, "dat");

    std::string header = "# timestamp buffer_type ";

    uint64_t num_labels;
    std::string label_type;
    if (type == "channels") {
        this->record_channels = true;
        num_labels = buffer_size;
        label_type = "sample";
    } else if (type == "samples") {
        this->record_channels = false;
        num_labels = num_channels;
        label_type = "channel";
    } else {
        recorder->add_string_to_queue(log_filename, timestamp, fmt::format("[SharedBuffer::start_recording] Error: Unknown type [{}], will not record data", type));
        return;
    }

    // Add header; use given labels or mappings if set
    if (labels.size() == 0 && mappings.size() > 0) {
        labels.resize(mappings_keys.size());
        // Replace spaces with underscores
        for (uint64_t mappings_idx = 0; mappings_idx < mappings_keys.size(); ++mappings_idx) {
            std::string key = mappings_keys[mappings_idx];
            std::replace(key.begin(), key.end(), ' ', '_');
            labels[mappings_idx] = key;
        }
    }

    // Create the header
    for (uint64_t label_idx = 0; label_idx < num_labels; ++label_idx) {
        std::string label;
        if (labels.size() > 0)
            label = labels[label_idx];
        else
            label = fmt::format("{}_{}", label_type, label_idx+1);

        header = fmt::format("{}{} ", header, label);
    }
    recorder->add_string_to_queue(dat_filename, header);

    this->record = true;
}
