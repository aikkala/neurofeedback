/**@file
 *  NeuroFeedback program.
 *  @author Aleksi Ikkala
 *  @date 2018/12/18
 *  @remarks
 */

// A shared buffer for writing/reading data from multiple threads
#include "SharedBuffer.h"

// For calculating PSD
#include "PSDCalculator.h"

// For plotting
#include "plotterform.h"

// For running experiments
#include "Experiment.h"

// For reading data
#include "Source.h"
#include "VirtualSensor.h"
#include "Replay.h"

// Include EGIAmp.h only if we've built with EGI libraries/source code
#ifdef BUILD_WITH_EGI
#include "EGI/EGIAmp.h"
#endif

// For writing data into files
#include "Recorder.h"

// GUI
#include <QApplication>
#include "programform.h"
#include "actionform.h"

#include <thread>
#include <memory>


/**
 * Main function
 */
int main(int argc, char **args) {

    // Start QApplication
    QApplication app(argc, args);

    // Ask for program info
    QWidget program_widget;
    ProgramForm pf(&program_widget);

    // Register sources into pf
    std::vector<std::string> sources{"VirtualSensor", "Replay"};
    // Add EGIAmp as an alternative if we've built with EGI support
#ifdef BUILD_WITH_EGI
    sources.insert(sources.begin(), "EGIAmp");
#endif
    pf.register_sources(sources);

    // Register measures into pf
    pf.register_measures(std::vector<std::string>{"theta-alpha ratio", "theta-alpha variance"});

    // Register caps into pf (different caps use different channels)
    pf.register_caps(std::vector<std::string>{"default", "54-56", "56-58", "58-61"});

    // Wait until user exists ProgramForm window
    pf.activate_window();
    app.exec();

    // Create a recorder for writing data and logs into files
    Recorder recorder(pf.get_session(), pf.get_record_data_folder(), pf.get_source() == "Replay" ? true : false);

    // Write condition info
    pf.write_condition_info(&recorder);

    // Initialise source; use a unique_ptr so we don't need to worry about memory (de)allocation
    std::unique_ptr<Source> src;
    if (pf.get_source() == "EGIAmp") {
#ifdef BUILD_WITH_EGI
        src = std::unique_ptr<Source>(new EGIAmp(&recorder));
#else
        printf("Project has not been built with EGI support!");
        return -1;
#endif
    } else if (pf.get_source() == "Replay") {
        std::string session_path = fmt::format("{}{}/dat/DataBuffer.dat.gz", pf.get_record_data_folder(), pf.get_session());
        src = std::unique_ptr<Source>(new Replay(session_path));
    } else if (pf.get_source() == "VirtualSensor") {
        src = std::unique_ptr<Source>(new VirtualSensor);
    } else {
        printf("Source %s has not been defined\n", pf.get_source().c_str());
        return -1;
    }

    // Check that source has been initialised properly
    if (!src->is_initialised()) {
        printf("Source has not been initialised properly, will not run the program.\n");
        return -1;
    }

    // Initialise a shared buffer where raw data is stored
    uint64_t data_buffer_length = static_cast<uint64_t>(pf.get_downsample_rate() * pf.get_psd_window_size());
    SharedBuffer data_buffer(src->get_num_channels(), data_buffer_length, "DataBuffer", &recorder);
    data_buffer.set_expected_sample_rate(pf.get_downsample_rate());

    // Initialise a shared buffer where PSD values are stored
    std::vector<std::string> psd_mappings = pf.get_psd_mappings();
    SharedBuffer psd_buffer(psd_mappings.size(), static_cast<uint64_t>(data_buffer_length / 2) + 1, "PSDBuffer", &recorder);
    psd_buffer.set_mappings(psd_mappings);
    psd_buffer.set_expected_sample_rate(pf.get_psd_calculation_rate());

    // Save temporally smoothed tracked values
    uint64_t track_buffer_length = static_cast<uint64_t>(pf.get_psd_calculation_rate() * pf.get_track_history_window_size());
    SharedBuffer track_buffer(1, track_buffer_length, "TrackBuffer", &recorder);
    track_buffer.set_mappings(std::vector<std::string>{pf.get_measure()});
    track_buffer.set_expected_sample_rate(pf.get_psd_calculation_rate());

    // Create PSDCalculator
    PSDCalculator psd_calculator(&data_buffer, &psd_buffer, &track_buffer, &recorder, &pf);

    // Create Plotter
    QWidget plot_widget;
    PlotterForm plotter(&plot_widget);
    plotter.activate_window(&psd_buffer, &track_buffer, &psd_calculator, &pf);

    // Create Experiment
    Experiment experiment(&track_buffer, &plotter, &recorder, &pf);

    // Make sure setting up PortAudio has been successful
    if (!experiment.is_initialised()) {
        printf("Experiment has not been initialised properly, will not run the program.\n");
        return -1;
    }

    // Start recording thread
    data_buffer.start_recording("samples");
    psd_buffer.start_recording("channels", psd_calculator.get_frequencies_string());
    track_buffer.start_recording("samples");
    std::thread record_thread(&Recorder::deploy, &recorder);

    // Start uploading samples from source
    src->set_buffer(&data_buffer);
    src->set_sample_rate(pf.get_downsample_rate());
    std::thread src_thread(&Source::deploy, src.get());

    // Start PSDCalculator, it can run all the time
    std::thread psd_calculation_thread(&PSDCalculator::deploy, &psd_calculator);

    // Wait for a few seconds until buffers have been initialised with real data
    std::this_thread::sleep_for(std::chrono::seconds(static_cast<uint64_t>(pf.get_psd_window_size()+1)));

    // Create action window
    QWidget action_widget;
    ActionForm af(&action_widget, &plot_widget);

    // Wait until user exists action window
    af.activate_window(&experiment);
    app.exec();

    // Stop recorder first -- if there are problems with destroying rest of the objects, at least the files should be safe
    pf.write_session_end(&recorder);
    recorder.stop();
    record_thread.join();

    // Signal threads to stop
    psd_calculator.stop();
    psd_calculation_thread.join();

    // Stop source
    src->stop();
    src_thread.join();

    // Print this line if we exit cleanly
    printf("Bye bye\n");
    return 1;

}
