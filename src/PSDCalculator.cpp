#include "PSDCalculator.h"
#include "LeastSquares.h"

#include <cmath>
#include <cstring>
#include <sstream>
#include <algorithm>
#include <numeric>

PSDCalculator::PSDCalculator(SharedBuffer* data_buffer, SharedBuffer* psd_buffer, SharedBuffer* track_buffer, Recorder* recorder, ProgramForm* pf) {
    this->data_buffer = data_buffer;
    this->psd_buffer = psd_buffer;
    this->track_buffer = track_buffer;
    this->sampling_frequency = pf->get_downsample_rate();
    this->timeout = pf->get_timeout();
    this->tracked = pf->get_measure();

    // Calculate smoothing window size (in samples) for tracked measure
    this->track_window_size = std::max(static_cast<uint64_t>(pf->get_smooth_window_size() * pf->get_psd_calculation_rate()), 1lu);

    // Estimate update interval (in microseconds) based on psd_frequency
    update_interval = 1000000/pf->get_psd_calculation_rate();

    // Set channels
    set_channels(pf->get_channels());

    // Add functions that contain the actual calculations into the function pointer map
    calculations.emplace("theta-alpha variance", &PSDCalculator::calculate_ta_variance);
    calculations.emplace("theta-alpha ratio", &PSDCalculator::calculate_ta_ratio);
    calculations.emplace("alpha", &PSDCalculator::calculate_alpha);
    calculations.emplace("theta", &PSDCalculator::calculate_theta);

    // For logging
    this->recorder = recorder;
    log_filename = "PSDCalculator.log";
    recorder->open_file(log_filename, "log");

    initialise();
}

PSDCalculator::~PSDCalculator() {
    // When exiting deallocate memory
    fftw_destroy_plan(fft_plan);
    fftw_free(fft_in);
    delete[] fft_out;

    for (uint16_t channel = 0; channel < data_buffer->get_num_channels(); ++channel) {
        delete[] buffer_copy[channel];
    }
    delete[] buffer_copy;

    delete ls;
}


void PSDCalculator::calculate_frequencies() {
    uint64_t psd_size = psd_buffer->get_buffer_size();
    frequencies.reserve(psd_size);

    // Calculate frequencies corresponding to each index in psd_buffer
    double coeff = static_cast<double>(data_buffer->get_buffer_size() / sampling_frequency);
    for (uint64_t idx = 0; idx < psd_size; ++idx) {
        frequencies.push_back(idx / coeff);
    }

}


void PSDCalculator::initialise() {

    // Get number of channels and buffer size from data_buffer
    uint64_t num_channels = data_buffer->get_num_channels();
    buffer_size = data_buffer->get_buffer_size();
    psd_size = psd_buffer->get_buffer_size();

    // Initialise parameters needed for short-time fourier transform calculations
    // NOTE!! if you change these parameters you should make sure the calculations in
    // "calculate_ta_variance" are correct
    stft_noverlap = 50;
    stft_window_size = 100;
    // TODO not sure if formula below is always correct
    stft_nwindows = ((buffer_size - stft_window_size) / (stft_window_size - stft_noverlap)) + 1;

    if (tracked == "theta-alpha variance") {
        window_size = stft_window_size;
    } else {
        window_size = buffer_size;
    }

    // Calculate a Hamming window
    window = get_hamming_window(window_size);

    // Initialise least squares for detrending
    // NOTE: in short-time fourier transform the whole 2sec epoch is detrended, not the shorter windows
    ls = new LeastSquares(buffer_size);

    // Normalisation coefficient for FFT (multiply by two so that we get original amplitude)
    norm_coeff = (2.0 / window_size) * (2.0 / window_size);

    // Calculate frequency resolution
    calculate_frequencies();

    // Figure out indices for alpha and theta bands
    std::vector<double> alpha_band = { 8, 12 };
    std::vector<double> theta_band = { 3, 5 };
    std::vector<double> broad_band = { 0.5, 40 };
    alpha_indices = {find_index(alpha_band[0]), find_index(alpha_band[1])};
    theta_indices = {find_index(theta_band[0]), find_index(theta_band[1])};
    broadband_indices = {find_index(broad_band[0]), find_index(broad_band[1])};

    if (alpha_indices.size() == 0 || theta_indices.size() == 0) {
        printf("Poorly chosen frequency bands, cannot initialise PSDCalculator thread\n");
        return;
    }

    // Allocate memory for a copy of the shared buffer
    buffer_copy = new double*[num_channels];
    for (uint64_t channel = 0; channel < num_channels; ++channel) {
        buffer_copy[channel] = new double[buffer_size];
    }

    // Initialise fftw_plan
    fft_in = static_cast<double*>(fftw_malloc(sizeof(double) * buffer_size));
    uint64_t psd_size = psd_buffer->get_buffer_size(); // (int) (buffer_size/2) + 1;
    fft_out = new std::complex<double>[psd_size];
    fft_plan = fftw_plan_dft_r2c_1d(static_cast<int>(buffer_size), fft_in,
                                    reinterpret_cast<fftw_complex*>(fft_out), FFTW_EXHAUSTIVE);

    // Pad with zeros for better resolution (in STFT calculations)
    for (uint64_t idx = 0; idx < buffer_size; ++idx) {
        fft_in[idx] = 0;
    }

    // Make sure there's a function defined for tracked
    auto it = calculations.find(tracked);
    if (it == calculations.end()) {
        printf("[PSDCalculator] Calculations not defined for tracked value [%s], "
               "not running PSDCalculator\n", tracked.c_str());
        initialised = false;
        return;
    }

    initialised = true;

}


void PSDCalculator::set_channels(std::vector<uint64_t> input_channels) {
    uint64_t num_channels = data_buffer->get_num_channels();

    // If input channels is empty use all channels
    if (input_channels.size() == 0) {
        input_channels.resize(num_channels);
        std::iota(input_channels.begin(), input_channels.end(), 0);
    }

    // Add input channels to channels
    channels.reserve(input_channels.size());
    for (const uint64_t& channel_idx : input_channels) {
        // Make sure given channel indices are valid and unique
        if (channel_idx < num_channels && std::find(channels.begin(), channels.end(), channel_idx) == channels.end()) {
            channels.push_back(channel_idx);
        }
    }
}


std::vector<double> PSDCalculator::get_frequencies() {
    return frequencies;
}

std::vector<std::string> PSDCalculator::get_frequencies_string() {
    std::vector<std::string> frequencies_string(frequencies.size());
    for (uint64_t freq_idx = 0; freq_idx < frequencies.size(); ++freq_idx) {
        frequencies_string[freq_idx] = fmt::format("{}_Hz", frequencies[freq_idx]);
    }
    return frequencies_string;
}

void PSDCalculator::run() {

    // Check whether initialisation was successful
    if (!is_initialised()) {
        printf("PSDCalculator has not been successfully initialised\n");
        return;
    }

    // Save tracked value in this buffer for temporal smoothing
    SharedBuffer temporal_smoothing(1, track_window_size, "TrackRunningSum");

    // We need to grab timestamp from data_buffer when notifying
    uint64_t timestamp;
    uint64_t next_update_timestamp = 0;

    while (get_status()) {

        // Wait until data_buffer has been updated
        if (!data_buffer->wait_for_update(next_update_timestamp, update_interval, timeout)) {
            std::string msg = fmt::format("[PSDCalculator::run] Error: Timeout reached, data_buffer hasn't been updated in {} seconds.", timeout);
            recorder->add_string_to_queue(log_filename, data_buffer->get_timestamp(), msg);
            issue_timeout();
            stop();
        }

        // Copy data from buffer
        data_buffer->copy_buffer(buffer_copy);

        // Grab timestamp
        timestamp = data_buffer->get_timestamp();

        // Allow data buffer to update again
        data_buffer->resume_updates();

        // Do different calculations depending on what we're tracking
        double value = (this->*calculations[tracked])();

        // Add tracked value into running average buffer
        temporal_smoothing.add_sample(0, value);

        // Update AT, theta, and alpha into the actual shared buffer that contains history of X past samples
        track_buffer->add_sample(tracked, temporal_smoothing.get_average(0));

        // Notify that buffers have been updated
        psd_buffer->notify(timestamp);
        track_buffer->notify(timestamp);
    }
}

double calculate_variance(const std::vector<double>& values) {
    double sum = std::accumulate(std::begin(values), std::end(values), 0.0);
    double m = sum / values.size();

    double accum = 0.0;
    std::for_each (std::begin(values), std::end(values), [&](const double d) {
        accum += (d-m) * (d-m);
    });

    return sqrt(accum / (values.size()-1));
}

double PSDCalculator::calculate_alpha() {
    std::vector<double> mean_psd = calculate_mean_psd();
    psd_buffer->update(tracked, mean_psd);
    return calculate_band_power(mean_psd, alpha_indices);
}

double PSDCalculator::calculate_theta() {
    std::vector<double> mean_psd = calculate_mean_psd();
    psd_buffer->update(tracked, mean_psd);
    return calculate_band_power(mean_psd, theta_indices);
}

double PSDCalculator::calculate_ta_ratio() {

    // Calculate mean PSD over all channels
    std::vector<double> mean_psd = calculate_mean_psd();

    // Get alpha power
    double alpha_power = calculate_band_power(mean_psd, alpha_indices);

    // Get theta power
    double theta_power = calculate_band_power(mean_psd, theta_indices);

    // Update PSD buffer
    psd_buffer->update(tracked, mean_psd);

    // Return the ratio (negative one if alpha is zero)
    if (alpha_power < std::numeric_limits<double>::min())
        return -1;
    else
        return theta_power / alpha_power;
}

double PSDCalculator::calculate_ta_variance() {

    // Should we make these class variables and re-use them?
    std::vector<double> alpha_diff(stft_nwindows);
    std::vector<double> theta_diff(stft_nwindows);
    uint64_t theta_channels = 0, alpha_channels = 0;
    std::vector<double> mean_psd(psd_size, 0.0);

    // Loop through all sensors
    for (uint64_t channel_idx = 0; channel_idx < channels.size(); ++channel_idx) {

        // Make sums zero
        std::vector<double> alpha_power(stft_nwindows, 0.0);
        std::vector<double> theta_power(stft_nwindows, 0.0);
        std::vector<double> broadband_power(stft_nwindows, 0.0);

        // Detrend channel
        ls->update(buffer_copy[channels[channel_idx]]);
        double slope = ls->get_slope();
        double intercept = ls->get_intercept();
        for (uint64_t k = 0; k < buffer_size; ++k)
            buffer_copy[channels[channel_idx]][k] = buffer_copy[channels[channel_idx]][k] - (static_cast<double>(slope*k) + intercept);

        for (uint64_t window_idx = 0; window_idx < stft_nwindows; ++window_idx) {

            // Copy channel into "fft_in"
            // Note: If copying signals one by one into "fft_in" is too slow we could try to use fftw_plan_many_dft_r2c
            memcpy(fft_in, &buffer_copy[channels[channel_idx]][window_idx*(stft_window_size-stft_noverlap)],
                    sizeof(double) * stft_window_size);

            // Detrend (calculate least squares slope and intercept)
            // If you detrend here remember to change LeastSquares' size
            //ls->update(fft_in);
            //double slope = ls->get_slope();
            //double intercept = ls->get_intercept();

            // Multiply with a Hamming window
            for (uint64_t k = 0; k < window.size(); ++k) {
                fft_in[k] = fft_in[k] * window[k];
            }

            // Do FFT
            fftw_execute(fft_plan);

            // Normalise and calculate power (magnitude squared)
            // Note: if this is too slow, we could try to use Eigen
            for (uint64_t sample_idx = 0; sample_idx < mean_psd.size(); ++sample_idx) {
                double power = std::norm(fft_out[sample_idx])*norm_coeff;
                mean_psd[sample_idx] += power;

                // Add to corresponding powers
                if (sample_idx >= theta_indices[0] && sample_idx <= theta_indices[1]) {
                    theta_power[window_idx] += power;
                }
                if (sample_idx >= alpha_indices[0] && sample_idx <= alpha_indices[1]) {
                    alpha_power[window_idx] += power;
                }
                if (sample_idx >= broadband_indices[0] && sample_idx <= broadband_indices[1]) {
                    broadband_power[window_idx] += power;
                }
            }

            alpha_diff[window_idx] = broadband_power[window_idx] - alpha_power[window_idx];
            theta_diff[window_idx] = broadband_power[window_idx] - theta_power[window_idx];
        }

        // Calculate explained variance
        double broadband_var = calculate_variance(broadband_power);
        double alpha_var_explained = 100 - (100*calculate_variance(alpha_diff) / broadband_var);
        double theta_var_explained = 100 - (100*calculate_variance(theta_diff) / broadband_var);

        // Threshold variances
        if (alpha_var_explained < 20) alpha_var_explained = 0;
        if (theta_var_explained < 20) theta_var_explained = 0;

        if (theta_var_explained > alpha_var_explained) ++theta_channels;
        else if (alpha_var_explained > theta_var_explained) ++alpha_channels;

    }

    // Divide PSD by number of channels (and windows) to get average PSD
    uint64_t num_repeats = channels.size() * stft_nwindows;
    std::transform(mean_psd.begin(), mean_psd.end(), mean_psd.begin(),
                   [num_repeats](double sample) -> double{ return (sample/num_repeats); });

    psd_buffer->update(tracked, mean_psd);

    if (theta_channels == 0)
        return 0;
    else if (alpha_channels == 0)
        return 1;
    else
        return static_cast<double>(theta_channels) / (theta_channels + alpha_channels);


}


std::vector<double> PSDCalculator::get_hamming_window(uint64_t size) {

    // Initialise a vector
    std::vector<double> window(size);

    // Calculate window values
    double coeff = 2*M_PI/(size-1);
    for (uint64_t k = 0; k < size; ++k) {
        window[k] = 0.54 - 0.46*cos(k*coeff);
    }

    return window;
}

double PSDCalculator::calculate_band_power(std::vector<double>& mean_psd,
                                           std::vector<uint64_t>& band_idxs) {
    // TODO Maybe use a trapezoidal rule here?
    double band_power = 0;
    for (uint64_t idx = band_idxs[0]; idx <= band_idxs[1]; ++idx) {
        band_power += mean_psd[idx];
    }

    return band_power;
}

uint64_t PSDCalculator::find_index(double frequency) {

    // Get iterator of element equal or greater than requested frequency
    auto lower = std::lower_bound(frequencies.begin(), frequencies.end(), frequency);

    // Get index of that iterator
    uint64_t idx = lower - frequencies.begin();

    // Check which is closer, equal or greater than or the one before (smaller than)
    if (abs(frequencies[idx] - frequency) <= abs(frequencies[idx-1] - frequency)) {
        return idx;
    } else {
        return idx-1;
    }

}

std::vector<double> PSDCalculator::calculate_mean_psd() {

    std::vector<double> mean_psd(psd_size, 0.0);

    // Loop over channels
    for (const uint64_t& channel : channels) {

        // Copy channel into "fft_in"
        // Note: If copying signals one by one into "fft_in" is too slow we could try to use fftw_plan_many_dft_r2c
        memcpy(fft_in, buffer_copy[channel], sizeof(double) * buffer_size);

        // Detrend (calculate least squares slope and intercept)
        ls->update(fft_in);
        double slope = ls->get_slope();
        double intercept = ls->get_intercept();

        // Remove linear trend and multiply with a Hamming window
        for (uint64_t k = 0; k < buffer_size; ++k) {
            fft_in[k] = (fft_in[k] - (static_cast<double>(slope*k) + intercept)) * window[k];
        }

        // Do FFT
        fftw_execute(fft_plan);

        // Normalise and calculate power (magnitude squared)
        // Note: if this is too slow, we could try to use Eigen
        for (uint64_t psd_idx = 0; psd_idx < psd_size; ++psd_idx) {
            // Scale with length (norm_coeff) and take norm
            mean_psd[psd_idx] += std::norm(fft_out[psd_idx])*norm_coeff;
        }
    }

    // Calculate average mean_psd over channels
    uint64_t num_channels = channels.size();
    std::transform(mean_psd.begin(), mean_psd.end(), mean_psd.begin(),
                   [num_channels](double sample) -> double{ return (sample/num_channels); });

    return mean_psd;
}
