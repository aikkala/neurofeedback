#include "programform.h"
#include "ui_programform.h"

#include <iomanip>

ProgramForm::ProgramForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProgramForm)
{
    ui->setupUi(this);
    widget = parent;

    // Log file for condition info
    log_filename = "ConditionInfo.log";

    // Set some defaults for program parameters
    amplitude_modulation = 0;
    session = "";
    randomised_sound_frequency = 660;
    randomised_sound_amplitude = 0.4; // This is only for etymotic research ear plugs
    timeout = 5; // in seconds

    // Set "goal"
    ui->goal_chooser->addItem("increase");
    ui->goal_chooser->addItem("decrease");
    goal = "increase";

    // Output folder where all program related data should be;
    // note that PROJECT_SOURCE_DIR is defined in this project's CMakeLists.txt
    record_data_folder = fmt::format("{}{}", PROJECT_SOURCE_DIR, "/data/recorded/");
    program_data_folder = fmt::format("{}{}", PROJECT_SOURCE_DIR, "/data/program/");
    randomised_data_location = "";
    wav_file_location = "";

    audio_output = true;
    ui->audio_button->setCheckable(true);
    ui->audio_button->setChecked(audio_output);

    randomise = false;
    ui->randomise_button->setCheckable(true);
    ui->randomise_button->setChecked(randomise);

    amplitude_alteration = true;
    ui->amplitude_alteration_button->setCheckable(true);
    ui->amplitude_alteration_button->setChecked(amplitude_alteration);
}

ProgramForm::~ProgramForm()
{
    delete ui;
}

void ProgramForm::set_connections()
{
    // Set session name
    QObject::connect(ui->session_input, SIGNAL(editingFinished()), this, SLOT(set_session()));

    // Switch audio on/off
    QObject::connect(ui->audio_button, SIGNAL(clicked()), this, SLOT(switch_audio_output()));

    // Switch randomisation on/off
    QObject::connect(ui->randomise_button, SIGNAL(clicked()), this, SLOT(switch_randomise()));

    // Switch amplitude alteration on/off
    QObject::connect(ui->amplitude_alteration_button, SIGNAL(clicked()), this, SLOT(switch_amplitude_alteration()));

    // Set amplitude modulation
    QObject::connect(ui->amplitude_modulation_input, SIGNAL(editingFinished()), this, SLOT(set_amplitude_modulation()));

    // Update source
    QObject::connect(ui->source_chooser, SIGNAL(currentTextChanged(const QString&)), this, SLOT(set_source(const QString&)));

    // Update measure
    QObject::connect(ui->measure_chooser, SIGNAL(currentTextChanged(const QString&)), this, SLOT(set_measure(const QString&)));

    // Update cap
    QObject::connect(ui->cap_chooser, SIGNAL(currentTextChanged(const QString&)), this, SLOT(set_cap(const QString&)));

    // Update goal
    QObject::connect(ui->goal_chooser, SIGNAL(currentTextChanged(const QString&)), this, SLOT(set_goal(const QString&)));

    // Quit window when continue button pressed
    QObject::connect(ui->continue_button, SIGNAL(clicked()), widget, SLOT(close()));

}

void ProgramForm::activate_window() {
    set_connections();
    widget->show();
}

void ProgramForm::register_sources(std::vector<std::string> sources) {
    // Set alternative sources
    for (const std::string& source : sources)
        ui->source_chooser->addItem(QString::fromStdString(source));

    // Set default
    source = sources[0];
}

void ProgramForm::set_source(const QString& text) {
    source = text.toStdString();
}

void ProgramForm::register_measures(std::vector<std::string> measures) {
    // Set alternative measures
    for (const std::string& measure : measures)
        ui->measure_chooser->addItem(QString::fromStdString(measure));

    // Set default
    measure = measures[0];
    set_measure_params();
}

void ProgramForm::set_measure(const QString& text) {
    measure = text.toStdString();
    set_measure_params();
}

void ProgramForm::register_caps(std::vector<std::string> caps) {
    // Set alternative caps
    for (const std::string& cap : caps)
        ui->cap_chooser->addItem(QString::fromStdString(cap));

    // Set default
    cap = caps[0];
    set_channels();
}

void ProgramForm::set_cap(const QString& text) {
    cap = text.toStdString();
    set_channels();
}

void ProgramForm::set_goal(const QString& text) {
    goal = text.toStdString();
}

void ProgramForm::set_measure_params() {
    if (measure == "theta-alpha ratio") {
        sound_frequency = 440;
        sound_amplitude = 1.0;
        smooth_window_size = 1.0;
        // Note: File below assumes that TA ratio is calculated with a 50 Hz interval
        randomised_data_location = fmt::format("{}{}", program_data_folder, "ta_ratio_epochs.csv");
        audio_upper_boundary = 2.24;
        audio_lower_boundary = 0.0;
        audio_midpoint = 0.48;
    } else if (measure == "alpha") {
        sound_frequency = 440;
        sound_amplitude = 1.0;
        smooth_window_size = 1.0;
    } else if (measure == "theta") {
        sound_frequency = 440;
        sound_amplitude = 1.0;
        smooth_window_size = 1.0;
    } else if (measure == "theta-alpha variance") {
        sound_frequency = 880;
        sound_amplitude = 0.30; // This is only for Etymotic research ear plugs
        // Note: File below assumes theta-alpha variance is calculated with a 50 Hz interval
        randomised_data_location = fmt::format("{}{}", program_data_folder, "ta_variance_epochs.csv");
        smooth_window_size = 1.0;
        audio_upper_boundary = 1;
        audio_lower_boundary = 0;
        audio_midpoint = 0.5;
    } else {
        printf("Unrecognised measure %s! Should not be possible!\n", measure.c_str());
    }

    // Use a different frequency in randomised conditions
    if (randomise) {
        sound_frequency = randomised_sound_frequency;
        sound_amplitude = randomised_sound_amplitude;
    }

    psd_mappings = std::vector<std::string> {measure};
    wav_file_location = fmt::format("{}{}", program_data_folder, "SleepinessN.wav");

}

void ProgramForm::set_channels() {
    if (cap == "default") {
        channels = std::vector<uint64_t> {44, 107, 101, 114, 99, 32, 121, 10, 69, 74, 82, 35, 103, 89};
    } else if (cap == "54-56") {
        channels = std::vector<uint64_t> {44, 107, 101, 108, 94, 32, 121, 15, 69, 71, 82, 35, 103, 89};
    } else if (cap == "56-58") {
        channels = std::vector<uint64_t> {44, 107, 101, 108, 95, 32, 121, 10, 69, 74, 82, 35, 103, 89};
    } else if (cap == "58-61") {
        channels = std::vector<uint64_t> {44, 107, 101, 108, 99, 32, 122, 10, 69, 74, 81, 35, 103, 83};
    } else {
        printf("Unrecognised cap %s! Should not be possible!\n", cap.c_str());
    }
}

void ProgramForm::write_condition_info(Recorder* recorder) {
    // Don't write anything if we're not recording
    if (!recorder)
        return;

    // Open a file
    recorder->open_file(log_filename, "log");

    // Add header
    recorder->add_string_to_queue(log_filename, "# key: value");

    // Write all condition and program related info that might be useful in the future
    recorder->add_string_to_queue(log_filename, fmt::format("Session: {}", session));
    recorder->add_string_to_queue(log_filename, fmt::format("Session start: {}", recorder->get_datetime()));
    recorder->add_string_to_queue(log_filename, fmt::format("Goal: {}", goal));
    recorder->add_string_to_queue(log_filename, fmt::format("Measure: {}", measure));
    recorder->add_string_to_queue(log_filename, fmt::format("Source: {}", source));

    // Write which channels were used; note that channel indices start from one here
    std::vector<uint64_t> channels_tmp = channels;
    std::for_each(channels_tmp.begin(), channels_tmp.end(), [](uint64_t& u){++u;});
    std::stringstream channels_string;
    std::copy(channels_tmp.begin(), channels_tmp.end(), std::ostream_iterator<int>(channels_string, " "));
    recorder->add_string_to_queue(log_filename, fmt::format("Cap: {}", cap));
    recorder->add_string_to_queue(log_filename, fmt::format("Channels: {}", channels_string.str()));

    recorder->add_string_to_queue(log_filename, fmt::format("Audio: {}", audio_output ? "on" : "off"));
    recorder->add_string_to_queue(log_filename, fmt::format("Randomise: {}", randomise ? "on" : "off"));
    recorder->add_string_to_queue(log_filename, fmt::format("Amplitude alteration: {}", amplitude_alteration ? "on" : "off"));
    recorder->add_string_to_queue(log_filename, fmt::format("Amplitude modulation: {}", amplitude_modulation));

    recorder->add_string_to_queue(log_filename, fmt::format("Downsample rate: {}", downsample_rate));
    recorder->add_string_to_queue(log_filename, fmt::format("PSD window size: {}", psd_window_size));
    recorder->add_string_to_queue(log_filename, fmt::format("PSD calculation rate: {}", psd_calculation_rate));
    recorder->add_string_to_queue(log_filename, fmt::format("Track history window size: {}", track_history_window_size));
    recorder->add_string_to_queue(log_filename, fmt::format("Temporal smoothing window size: {}", smooth_window_size));
    recorder->add_string_to_queue(log_filename, fmt::format("Timeout: {}", timeout));
    recorder->add_string_to_queue(log_filename, fmt::format("Audio maximum amplitude: {}", sound_amplitude));
    recorder->add_string_to_queue(log_filename, fmt::format("Audio boundaries: {} {} {}", audio_lower_boundary, audio_midpoint, audio_upper_boundary));
}

void ProgramForm::write_session_end(Recorder* recorder) {
    // Write session's end time into the log file
    std::time_t unix_time = std::time(nullptr);
    std::tm unix_time_utc = *std::gmtime(&unix_time);
    std::ostringstream datetime_stream;
    datetime_stream << std::put_time(&unix_time_utc, "%Y-%m-%dT%H:%M:%SZ");
    recorder->add_string_to_queue(log_filename, fmt::format("Session end: {}", datetime_stream.str()));
}

void ProgramForm::set_session() {
    session = ui->session_input->text().toStdString();
}

void ProgramForm::switch_amplitude_alteration() {
    if (amplitude_alteration) {
        amplitude_alteration = false;
        ui->amplitude_alteration_button->setText("off");
    } else {
        amplitude_alteration = true;
        ui->amplitude_alteration_button->setText("on");
    }
}

void ProgramForm::switch_audio_output() {
    if (audio_output) {
        audio_output = false;
        ui->audio_button->setText("off");
    } else {
        audio_output = true;
        ui->audio_button->setText("on");
    }
}

void ProgramForm::switch_randomise() {
    if (randomise) {
        randomise = false;
        ui->randomise_button->setText("off");
    } else {
        randomise = true;
        ui->randomise_button->setText("on");
    }

    // randomise affects some of the measure params, need to set them again
    set_measure_params();
}

void ProgramForm::set_amplitude_modulation() {
    double value = ui->amplitude_modulation_input->value();
    if (value > 0) {
        amplitude_modulation = value;
    }
}

std::string ProgramForm::get_session() {
    return session;
}

std::string ProgramForm::get_cap() {
    return cap;
}

std::string ProgramForm::get_record_data_folder() {
    return record_data_folder;
}

bool ProgramForm::get_amplitude_alteration() {
    return amplitude_alteration;
}

double ProgramForm::get_amplitude_modulation() {
    return amplitude_modulation;
}

bool ProgramForm::get_audio_output() {
    return audio_output;
}

bool ProgramForm::get_randomise() {
    return randomise;
}

std::string ProgramForm::get_randomised_data_location() {
    return randomised_data_location;
}

std::string ProgramForm::get_wav_file_location() {
    return wav_file_location;
}

std::string ProgramForm::get_goal() {
    return goal;
}

double ProgramForm::get_sound_frequency() {
    return sound_frequency;
}

double ProgramForm::get_sound_amplitude() {
    return sound_amplitude;
}

double ProgramForm::get_audio_midpoint() {
    return audio_midpoint;
}

double ProgramForm::get_audio_lower_boundary() {
    return audio_lower_boundary;
}

double ProgramForm::get_audio_upper_boundary() {
    return audio_upper_boundary;
}

std::string ProgramForm::get_source() {
    return source;
}

std::string ProgramForm::get_measure() {
    return measure;
}

std::vector<std::string> ProgramForm::get_psd_mappings() {
    return psd_mappings;
}

std::vector<uint64_t> ProgramForm::get_channels() {
    return channels;
}

double ProgramForm::get_smooth_window_size() {
    return smooth_window_size;
}

uint64_t ProgramForm::get_downsample_rate() {
    return downsample_rate;
}

double ProgramForm::get_psd_window_size() {
    return psd_window_size;
}

uint64_t ProgramForm::get_psd_calculation_rate() {
    return psd_calculation_rate;
}

double ProgramForm::get_track_history_window_size() {
    return track_history_window_size;
}

uint64_t ProgramForm::get_timeout() {
    return timeout;
}
