/**@file
 *  Recorder Source File
 *  @author Aleksi Ikkala
 *  @date 2019/01/17
 *  @remarks
 */

#include "Recorder.h"
#include "Util.h"

#include <iomanip>
#include <sstream>
#include <thread>
#include <chrono>

#include "fmt/format.h"

#if __cplusplus < 201703L // If version of C++ is less than 17
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#else
#include <filesystem>
namespace fs = std::filesystem;
#endif


Recorder::Recorder(std::string session, std::string record_folder, bool replay) {

    // First need to get time (UTC Unix time)
    std::time_t unix_time = std::time(nullptr);
    std::tm unix_time_utc = *std::gmtime(&unix_time);

    // If we're replaying data we need to use the lattermost folder name as 'session', and
    // rest of the string as sub-folder on top of 'record_folder'
    std::ostringstream oss;
    if (replay) {
        std::vector<std::string> split = Util::split(session, '/');
        folder = fmt::format("{}{}", record_folder, session.substr(0, session.size() - (split.end()-1)->size()));
        oss << *(split.end()-1) << "-replay-";
    } else {
        // Make sure the base directory exist
        if (session == "") {
            folder = record_folder;
        } else {
            // Get current date
            std::ostringstream date_stream;
            date_stream << std::put_time(&unix_time_utc, "%Y-%m-%d");
            folder = fmt::format("{}{}/{}/", record_folder, session, date_stream.str());
        }

        fs::create_directories(folder);
    }

    // Create a specific folder so old data isn't overwritten

    // Convert into datetime and save it for later use
    std::ostringstream datetime_stream;
    datetime_stream << std::put_time(&unix_time_utc, "%Y-%m-%dT%H:%M:%SZ");
    datetime = datetime_stream.str();

    // Use the datetime as output folder's suffix
    oss << datetime << "/";
    folder = folder + oss.str();

    // Now create the sub-folder
    fs::create_directory(folder);

    // Set timeout
    timeout = 5;

    allow_new_data(true);
}

Recorder::~Recorder() {
    // Make sure files are closed even when recorder has not been run
    close_files();
}

void Recorder::run() {

    // Start accepting new data
    allow_new_data(true);

    while (get_status()) {

        std::string str = "";

        // Get queue size; make sure no thread is adding elements into queue while reading its size
        uint64_t queue_size;
        {
            std::lock_guard<std::mutex> guard(mutex);
            queue_size = queue.size();
        }

        if (queue_size > 5000) {
            printf("[Recorder] Messages are stacking up (%lu messages in queue)\n", queue_size);
        } else if (queue_size == 0) {
            std::this_thread::sleep_for(std::chrono::milliseconds(2));
            continue;
        }

        // Read a Writable from queue and write to the file
        // We're always appending to end of queue and reading from the beginning of it
        // so there shouldn't be a need for mutex, but better safe than sorry?
        Writable data;
        {
            std::lock_guard<std::mutex> guard(mutex);
            data = queue.front();
            queue.pop_front();
        }

        // Make sure the file exists
        if (files.count(data.filename) == 0) continue;

        // Write timestamp (also grab the timestamp so we have a vague idea of where we're going)
        if (data.print_timestamp) {
            str = fmt::format("{} ", data.timestamp);
        }

        // Write message first
        if (data.entry.message.size() > 0) {
            str = fmt::format("{}{} ", str, data.entry.message);
        }

        // Write vector
        if (data.entry.values.size() > 0) {
            for (uint64_t idx = 0; idx < data.entry.values.size(); ++idx) {
                str = fmt::format("{}{} ", str, data.entry.values[idx]);
            }
        }

        // Write a newline
        str = fmt::format("{}\n", str);

        // Write data into file
        files[data.filename]->write(str.c_str(), str.size());

        // Flush (not sure if this actually does anything since we're inserting a newline to the string already)
        files[data.filename]->flush();
    }

    // Finish by closing files
    close_files();
}

void Recorder::open_file(std::string filename, std::string subfolder) {

    std::string save_file;

    // Create a subfolder if requested
    if (subfolder.size() > 0) {
        fs::create_directory(folder + subfolder);
        save_file = fmt::format("{}/{}.gz", folder+subfolder, filename);
    } else {
        save_file = fmt::format("{}{}.gz", folder, filename, ".gz");
    }

    // Open file and add it to map of files
    if (files.find(filename) == files.end()) {
        files[filename] = std::unique_ptr<zstr::ofstream> (new zstr::ofstream(save_file));
    } else {
        printf("[Recorder::open_file] Warning, file with name %s already exists, will not overwrite.\n", filename.c_str());
    }
}

void Recorder::close_file(std::string filename) {
    // Note: This function only closes the file, doesn't remove filename from files (or delete the stream)

    // Get pointer to file
    std::ofstream* pntr = static_cast<std::ofstream*>(files[filename].get());

    // Flush and close
    pntr->flush();
    pntr->close();

}

void Recorder::close_files() {
    // Close all files
    std::map<std::string, std::unique_ptr<std::ostream>>::iterator it;
    for (it = files.begin(); it != files.end(); ++it) {
        close_file(it->first);
    }
    files.clear();
}

void Recorder::add_vector_to_queue(std::string filename, uint64_t timestamp, std::vector<double> values, std::string msg) {
    Writable data;
    data.filename = filename;
    data.timestamp = timestamp;
    data.entry.values = values;
    data.entry.message = msg;
    data.print_timestamp = true;
    push_writable_to_queue(data);
}

void Recorder::add_string_to_queue(std::string filename, uint64_t timestamp, std::string msg) {
    Writable data;
    data.filename = filename;
    data.timestamp = timestamp;
    std::vector<double> empty;
    data.entry.values = empty;
    data.entry.message = msg;
    data.print_timestamp = true;
    push_writable_to_queue(data);
}

void Recorder::add_string_to_queue(std::string filename, std::string msg) {
    Writable data;
    data.filename = filename;
    std::vector<double> empty;
    data.entry.values = empty;
    data.entry.message = msg;
    data.print_timestamp = false;
    push_writable_to_queue(data);
}

void Recorder::push_writable_to_queue(Writable& data) {
    // Don't add more data to queue if we're about to shut down
    if (!get_new_data_guard()) {
        return;
    } else {
        std::lock_guard<std::mutex> guard(mutex);
        queue.push_back(data);
    }
}

void Recorder::stop() {

    // Don't accept new data
    allow_new_data(false);

    // Wait until all queued messages have been written -- or until timeout
    std::chrono::steady_clock::time_point wait_started = std::chrono::steady_clock::now();
    while (queue.size() > 0) {
        std::this_thread::sleep_for(std::chrono::milliseconds(2));
        std::chrono::duration<double> elapsed = std::chrono::steady_clock::now() - wait_started;
        if (elapsed.count() > timeout) {
            break;
        } else {
            continue;
        }
    }

    set_status(false);
}

std::string Recorder::get_datetime() {
    return datetime;
}

void Recorder::allow_new_data(bool value) {
    std::lock_guard<std::mutex> lg(new_data_mutex);
    new_data_guard = value;
}

bool Recorder::get_new_data_guard() {
    std::lock_guard<std::mutex> lg(new_data_mutex);
    return new_data_guard;
}
