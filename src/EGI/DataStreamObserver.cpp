/**@file
 *  DataStreamObserver source file
 *  @author Aleksi Ikkala
 *  @date 2019/04/03
 *  @remarks Adapted from Robert Bell's "DataStreamObserver" source file (from AmpServerProSDK-2.1)
 */

#include "EGI/DataStreamObserver.h"
#include "fmt/format.h"

#include "UtilityFunctions.h"

#include <iostream>
#include <string.h>
#include <sys/time.h>

// Some useful defines
#define SIZE_OF_PACKET1_HEADER 32
#define SIZE_OF_PACKET1_SAMPLE 1152
#define SIZE_OF_PACKET2_SAMPLE 1264

// Check endianness
static inline bool is_big_endian();

DataStreamObserver::DataStreamObserver(AS_Network_Client* client, SharedBuffer* data_buffer, Recorder* recorder) :
    EGIBase::ObserverPattern::Observer() {

    // Important! Make sure decimated rate is set to 250 Hz; only this rate is currenty supported.
    // Note: the amp still outputs samples with a rate of 1000 Hz, but they're already downsampled to 250 Hz (i.e. there
    // are four samples for each timestamp)
    AS_ReturnValue* return_value = client->sendCommand(0, "cmd_SetDecimatedRate",-1,250);
    // Is there any way of checking that the command was successful? return_value->get_value() always returns "status complete"
    delete return_value;

    amp_info = client->getAmpInfo(0);
    sample_count = batch_size = previous_timestamp = 0;
    this->data_buffer = data_buffer;
    cumulative_values.resize(amp_info.numberOfChannels, 0.0);
    big_endian = is_big_endian();

    // For logging
    this->recorder = recorder;
    log_filename = "DataStreamObserver.log";
    recorder->open_file(log_filename, "log");
    recorder->add_string_to_queue(log_filename, "# timestamp message");

    // Use system time to estimate sample rate
    session_start = std::chrono::steady_clock::now();
    previous_sample_time = session_start;

    // Set the appropriate scaling factor.
    // Note: Scaling factors can be found in the document "Scaling Factors - AD unit to microvolt".
    std::string msg;
    if (amp_info.amplifierType == AS_Network_Types::naNA300) {
        msg = "Setting scaling factor for NA300 amplifier: 0.0244140625";
        scaling_factor = 0.0244140625;
    } else if (amp_info.amplifierType == AS_Network_Types::naNA400) {
        msg = "Setting scaling factor for NA400 amplifier: 0.00015522042";
        scaling_factor = 0.00015522042;
    } else if (amp_info.amplifierType == AS_Network_Types::naNA410) {
        msg = "Setting scaling factor for NA410 amplifier: 0.00009636188";
        scaling_factor = 0.00009636188;
    } else {
        msg = "Error: could not determine appropriate amplifier scaling factor, all values will be zero!";
        scaling_factor = 0.0;
    }
    recorder->add_string_to_queue(log_filename, 0, msg);

}

DataStreamObserver::~DataStreamObserver() {
}

void DataStreamObserver::update(EGIBase::ObserverPattern::Observable* o, EGIBase::EGIObject* eObject) {

    eObject->retain();
    AmpDataObject* aDO = dynamic_cast<AmpDataObject*>(eObject);
    char* data_ptr = aDO->getData(true);

    // Check PacketFormat; get number of samples in this packet
    long length = aDO->getSizeOfData();
    unsigned int number_of_samples;
    if (amp_info.packetType == AS_Network_Types::pkType1) {
        number_of_samples = length / SIZE_OF_PACKET1_SAMPLE;
    } else if (amp_info.packetType == AS_Network_Types::pkType2) {
        number_of_samples = length / SIZE_OF_PACKET2_SAMPLE;
    } else {
        printf("Unknown packet type! Cannot parse data!");
        return;
    }

    // Loop through received samples
    for (uint64_t sample = 0; sample < number_of_samples; ++sample) {

        // Increase sample count
        ++sample_count;

        // Get a pointer to data and timestamp
        void* data;
        uint64_t timestamp;
        if (amp_info.packetType == AS_Network_Types::pkType1) {
            data = reinterpret_cast<AS_Network_Types::PacketFormat1*>(data_ptr)[sample].eeg;
            timestamp = (sample_count-1)/4 * 4000;
        } else {
            AS_Network_Types::PacketFormat2* src = reinterpret_cast<AS_Network_Types::PacketFormat2*>(data_ptr);
            data = src[sample].eegData;
            timestamp = src[sample].timeStamp;
        }

        if (sample_count % 30000 == 0) {
            // Log estimated sample rate
            std::chrono::steady_clock::time_point current_time = std::chrono::steady_clock::now();
            std::chrono::duration<double> diff = current_time - previous_sample_time;
            double estimated_sample_rate = 30000/diff.count();
            recorder->add_string_to_queue(log_filename, timestamp,
                                          fmt::format("[DataStreamObserver] Estimated sample rate from past 30000 samples: {}", estimated_sample_rate));
            previous_sample_time = current_time;

            // Print a warning if estimated sample rate is far from expected sample rate
            if (abs(estimated_sample_rate-SAMPLING_RATE) > 0.1*SAMPLING_RATE) {
                std::string msg;
                if (amp_info.packetType == AS_Network_Types::pkType1) {
                    msg = fmt::format("[DataStreamObserver] A VERY SEVERE WARNING! If you see this warning message, something is seriously wrong and "
                                      "you shouldn't trust what this software outputs. Reason for warning: amplifier's estimated sample rate ({} Hz) is different from expected sample rate ({} Hz)",
                                      estimated_sample_rate, SAMPLING_RATE);
                } else {
                    msg = fmt::format("[DataStreamObserver] Warning: Amplifier's estimated sampling rate ({} Hz) is different from expected sample rate ({}) Hz)",
                                      estimated_sample_rate, SAMPLING_RATE);
                }
                printf("\n%s\n", msg.c_str());
                recorder->add_string_to_queue(log_filename, timestamp, msg);
            }
        }

        // Check whether it's time to upload samples into data_buffer and notify subscribers
        if (timestamp > previous_timestamp) {

            // Make sure we aren't dividing by zero
            if (batch_size == 0) {
                batch_size = 1;
            }

            // Upload samples into data_buffer
            for (uint16_t channel = 0; channel < amp_info.numberOfChannels; ++channel) {
                data_buffer->add_sample(channel, cumulative_values[channel]/batch_size);
                cumulative_values[channel] = 0;
            }
            batch_size = 0;

            // Notify subscribers
            data_buffer->notify(previous_timestamp);

            // Update previous timestamp
            previous_timestamp = timestamp;

        } else if (timestamp < previous_timestamp) {
            // Ignore late samples (this should be very rare when decimated rate is set to 250 Hz)
            continue;
        }

        // Loop over channels
        for (uint16_t channel = 0; channel < amp_info.numberOfChannels; ++channel) {

            // Parse channel's value
            double value;
            if (amp_info.packetType == AS_Network_Types::pkType1) {
                // Check if we need to swap
                float float_value = static_cast<float*>(data)[channel];
                if (!big_endian) {
                    EGIBase::UtilityFunctions::swapUINT32_t(reinterpret_cast<uint32_t *>(&float_value));
                }
                value = static_cast<double>(float_value);
            } else {
                int32_t int_value = static_cast<int32_t*>(data)[channel];
                value = static_cast<double>(int_value);
            }

            // We'll downsample with a running average
            cumulative_values[channel] += value * scaling_factor;
        }
        ++batch_size;
    }
}

// Check whether this system/cpu is using big endian coding
bool is_big_endian() {
    union {
        uint32_t i;
        uint8_t c[4];
    } bint = { 0x01020304 };

    return bint.c[0] == 1;
}
