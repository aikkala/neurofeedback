/**@file
 *  Experiment Source File
 *  @author Aleksi Ikkala
 *  @date 2018/12/14
 *  @remarks
 */

#include <chrono>

#include "Experiment.h"

#include "fmt/format.h"


#if __cplusplus < 201703L // If version of C++ is less than 17
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#else
#include <filesystem>
namespace fs = std::filesystem;
#endif

// Forward declaration of the callback function
static int pa_callback(const void* input_buffer, void* output_buffer,
                       unsigned long frames_per_buffer,
                       const PaStreamCallbackTimeInfo* time_info,
                       PaStreamCallbackFlags status_flags,
                       void* user_data );

// Ignore given input
static inline void ignore_result(FILE*  unused_result) {
    (void) unused_result;
}

Experiment::Experiment(SharedBuffer* track_buffer, PlotterForm* plotter, Recorder* recorder, ProgramForm* pf) {

    this->recorder = recorder;
    this->track_buffer = track_buffer;
    this->plotter = plotter;
    tracked = pf->get_measure();
    audio_output = pf->get_audio_output();

    log_filename = "Experiment.log";
    recorder->open_file(log_filename, "log");
    recorder->add_string_to_queue(log_filename, "# timestamp message");

    // Save amplitudes
    amplitude_filename = "Amplitude.dat";
    recorder->open_file(amplitude_filename, "dat");
    recorder->add_string_to_queue(amplitude_filename, "# timestamp carrier_target_amplitude");

    // We want to update audio every time tracked measure has been updated
    update_interval = 1;
    wav_sample_idx = 0;
    wav_file_finished = true;
    elapsed_time = 0;
    wav_next = wav_interval = 150;

    // Stop experiment after five minutes
    experiment_duration = 300;

    this->timeout = pf->get_timeout();

    // Set carrier frequency and amplitude
    DEFAULT_CARRIER_FREQUENCY = pf->get_sound_frequency();
    car_target_frequency = car_current_frequency = DEFAULT_CARRIER_FREQUENCY;
    DEFAULT_CARRIER_AMPLITUDE = pf->get_sound_amplitude();
    car_current_amplitude = car_target_amplitude = DEFAULT_CARRIER_AMPLITUDE;
    car_angle_delta = car_current_angle = 0;
    amplitude_altered = pf->get_amplitude_alteration();
    lower_boundary = upper_boundary = 0;

    // Set modulator frequency and amplitude
    DEFAULT_MODULATOR_FREQUENCY = pf->get_amplitude_modulation();
    mod_current_frequency = mod_target_frequency = DEFAULT_MODULATOR_FREQUENCY;
    mod_angle_delta = mod_current_angle = 0;
    amplitude_modulated = DEFAULT_MODULATOR_FREQUENCY > 0 ? true : false;
    mod_freq_altered = false;

    update_angle_delta(&car_angle_delta, &car_current_frequency);
    update_angle_delta(&mod_angle_delta, &mod_current_frequency);

    // Get boundaries for min and max volume
    upper_boundary = pf->get_audio_upper_boundary();
    lower_boundary = pf->get_audio_lower_boundary();
    midpoint = pf->get_audio_midpoint();

    // Let's suppress prints to error stream when initialising portaudio
    // Note: You should allow prints to error stream when running on new audio system to get all warnings/errors
    // Note: We need to ignore the returned FILE* when doing a release build -- otherwise we get an unused return value error
    ignore_result(freopen("/dev/null", "w", stderr));
    PaError err = Pa_Initialize();
    ignore_result(freopen("/dev/tty", "w", stderr));
    if (check_for_error(err)) {
        return;
    }

    //Open an audio stream
    err = Pa_OpenDefaultStream( &stream,
                                0,          // no input channels
                                1,          // mono output
                                paFloat32,  // 32 bit floating point output
                                SAMPLE_RATE,
                                FRAMES_PER_BUFFER,        // frames per buffer, i.e. the number
                                //of sample frames that PortAudio will
                                //request from the callback. Many apps
                                //may want to use
                                //paFramesPerBufferUnspecified, which
                                //tells PortAudio to pick the best,
                                //possibly changing, buffer size.
                                pa_callback, // this is your callback function
                                this ); //This is a pointer that will be passed to your callback
    if (check_for_error(err))
        return;


    // Check whether we're running a randomise condition
    randomise = pf->get_randomise();
    if (randomise) {
        // If we're running randomise condition we need to read randomised data from a file.
        // If the file doesn't exist we can't run experiment
        if (!read_randomised_data(pf->get_randomised_data_location()))
            return;

        // Set also random seed and initialise the random integer generator
        generator.seed(static_cast<unsigned long>(std::chrono::system_clock::now().time_since_epoch().count()));
        uniform_distribution.param(std::uniform_int_distribution<unsigned int>::param_type(0, static_cast<unsigned int>(randomised_data.size()-1)));
    }

    // Try to read the wav file
    if (!read_wav_file(pf->get_wav_file_location()))
        return;

    initialised = true;
}

Experiment::~Experiment() {

    sf_close(file);

    // Try to close everything even if they give errors
    stop_audio_stream();

    PaError err;
    err = Pa_CloseStream( stream );
    check_for_error(err);

    err = Pa_Terminate();
    check_for_error(err);
}

bool Experiment::read_wav_file(std::string wav_file) {

    // Open file
    file = sf_open(wav_file.c_str(), SFM_READ, &info);
    if (sf_error(file) != SF_ERR_NO_ERROR) {
        std::string msg = fmt::format("[Experiment::read_wav_file] Error: Could not open wav file {}", wav_file);
        printf("%s\n", msg.c_str());
        recorder->add_string_to_queue(log_filename, track_buffer->get_timestamp(), msg);
        return false;
    }

    // Initialise space into a buffer
    std::vector<double> wav_buffer_tmp(info.frames*info.channels);

    // Read the file
    sf_read_double(file, &wav_buffer_tmp[0], info.frames*info.channels);

    // Get rid of every other sample since we're playing mono sound, but add also
    // 2 sec silence for subject to answer
    wav_buffer.resize(info.frames+SAMPLE_RATE*2, 0.0);
    for (uint64_t sample_idx = 0; sample_idx < wav_buffer_tmp.size(); sample_idx+=info.channels) {
        wav_buffer[sample_idx/info.channels] = wav_buffer_tmp[sample_idx];
    }

    return true;
}

bool Experiment::read_randomised_data(std::string randomised_data_location) {

    // Check that the file exists
    if (!fs::exists(randomised_data_location))
    {
        std::string msg = fmt::format("[Experiment::read_randomised_data] Error: File {} does not exist!", randomised_data_location);
        printf("%s\n", msg.c_str());
        recorder->add_string_to_queue(log_filename, track_buffer->get_timestamp(), msg);
        return false;
    }

    // Open file
    std::ifstream ifs;
    ifs.open(randomised_data_location, std::ifstream::in);

    // Read data into this string
    std::string temp;

    // Loop through rows and add them into randomised_data vector
    while (std::getline(ifs, temp)) {
        std::istringstream buffer(temp);
        std::vector<double> line((std::istream_iterator<double>(buffer)),
                                std::istream_iterator<double>());
        randomised_data.push_back(line);
    }

    // Set some parameters
    current_epoch = 0;
    current_sample = randomised_data[current_epoch].size();

    return true;
}

double Experiment::read_randomised_value() {
    // Switch epoch if it has ended
    if (current_sample == randomised_data[current_epoch].size()) {
        current_epoch = uniform_distribution(generator);
        current_sample = 0;
    }

    return randomised_data[current_epoch][++current_sample];
}

bool Experiment::check_for_error(PaError err) {
    if (err != paNoError) {
        recorder->add_string_to_queue(log_filename, track_buffer->get_timestamp(), fmt::format("[Experiment::check_for_error] PortAudio error: {}", Pa_GetErrorText(err)));
        printf("PortAudio error: %s\n", Pa_GetErrorText( err ));
        return true;
    } else {
        return false;
    }
}


bool Experiment::start_audio_stream() {

    // If stream is already running there's no need to start anything
    if (!Pa_IsStreamStopped(stream)) {
        return true;
    }

    // Start the stream
    PaError err;
    err = Pa_StartStream(stream);
    if (check_for_error(err))
        return false;
    else
        return true;
}

bool Experiment::stop_audio_stream() {

    // If stream is not running there's no need to stop anything
    if (Pa_IsStreamStopped(stream)) {
        return true;
    }

    // Stop the stream
    PaError err = Pa_StopStream(stream);
    if (check_for_error(err))
        return false;
    else
        return true;
}

std::vector<double> Experiment::get_experiment_values() {
    return collected.get_vector();
}

void Experiment::set_duration_display(QLabel* duration_display) {
    this->duration_display = duration_display;
}

void Experiment::set_stop_button(QPushButton* stop_button) {
    this->stop_button = stop_button;
}

void Experiment::set_elapsed_time() {
    std::string elapsed_msg;
    if (experiment_duration < std::numeric_limits<double>::min()) {
        elapsed_msg = fmt::format("{:.1f}", elapsed_time);
    } else {
        elapsed_msg = fmt::format("{:.1f} / {:.0f}", elapsed_time, experiment_duration);
    }
    duration_display->setText(QString::fromStdString(elapsed_msg));
}

bool Experiment::is_initialised() {
    return initialised;
}

void Experiment::run() {

    // Exit if initialisation has failed
    if (!initialised) {
        std::string msg = "[Experiment::run] Error: Initialisation has failed, exiting Experiment";
        printf("%s\n", msg.c_str());
        recorder->add_string_to_queue(log_filename, track_buffer->get_timestamp(), msg);
        return;
    }

    // Reset all target values / current values
    car_current_frequency = car_target_frequency = DEFAULT_CARRIER_FREQUENCY;
    mod_current_frequency = mod_target_frequency = DEFAULT_MODULATOR_FREQUENCY;

    // Start amplitude from zero when altering volume
    if (amplitude_altered) {
        car_current_amplitude = car_target_amplitude = 0;
    } else {
        car_current_amplitude = car_target_amplitude = DEFAULT_CARRIER_AMPLITUDE;
    }

    // Plot these boundaries so we have some idea where they are
    if (audio_output && amplitude_altered) {
        plotter->set_amplitude_boundaries(lower_boundary, upper_boundary);
    }

    // Start audio stream
    if (!start_audio_stream())
        return;

    // Ignore previous experiment values
    collected.clear();

    // Initialise next_update_timestamp
    uint64_t next_update_timestamp = 0;

    // Signal that tracking has started
    timestamp = track_buffer->get_timestamp();
    recorder->add_string_to_queue(log_filename, timestamp, "Experiment has started");
    uint64_t experiment_start = timestamp;
    plotter->start_experiment();

    // Stop when interrupt has been triggered
    while (get_status()) {

        // Wait until tracked value has been updated
        if (!track_buffer->wait_for_update(next_update_timestamp, update_interval, timeout)) {
            std::string msg = fmt::format("[Experiment::run] Error: Timeout reached, track_buffer hasn't been updated in {} seconds.", timeout);
            recorder->add_string_to_queue(log_filename, track_buffer->get_timestamp(), msg);
            issue_timeout();
            stop();
        }

        // Get latest tracked value, or, if randomisation condition, read latest value from randomised_data
        double value = track_buffer->latest_sample(tracked);

        // Grab timestamp
        timestamp = track_buffer->get_timestamp();

        // Allow track_buffer to update again
        track_buffer->resume_updates();

        // Collect values for calculating median / statistical testing
        collected.insert(value);

        // Update plot's title so we know how long we've been running this experiment (in seconds)
        elapsed_time = (timestamp - experiment_start) / 1000000.0;
        set_elapsed_time();

        // Stop experiment when time has come
        if (elapsed_time > experiment_duration) {
            // Stop via GUI if stop_button has been set
            if (stop_button)
                stop_button->click();
            else
                stop();
        }

        // Set title and median (baseline)
        plotter->set_experiment_baseline(collected.get_median());

        // If we're running a randomised condition overwrite track_buffer's value with a value read
        // from randomised_data
        if (randomise)
            value = read_randomised_value();

        // Do we want to change frequency of amplitude modulator?
        if (mod_freq_altered) {
            // Not used currently; notice that we could change the frequency of the carrier signal also
        }

        // Do we want to change sound amplitude?
        if (amplitude_altered) {
            //car_target_amplitude = std::min( std::max(0.0, value - lower_boundary) / interval, 1.0 );

            if (value > midpoint) {
                car_target_amplitude = DEFAULT_CARRIER_AMPLITUDE/2 - std::min(1.0, (value-midpoint)/(upper_boundary-midpoint))*(DEFAULT_CARRIER_AMPLITUDE/2);
            } else {
                car_target_amplitude = DEFAULT_CARRIER_AMPLITUDE/2 + std::min(1.0, (midpoint-value)/(midpoint-lower_boundary))*(DEFAULT_CARRIER_AMPLITUDE/2);
            }

            if (tracked == "alpha")
                car_target_amplitude = DEFAULT_CARRIER_AMPLITUDE - car_target_amplitude;

            // Amplitude output should already be constrained to [0,1], but maybe better check here again so we don't get extremely loud sounds
            if (car_target_amplitude > DEFAULT_CARRIER_AMPLITUDE)
                car_target_amplitude = DEFAULT_CARRIER_AMPLITUDE;
        }

        recorder->add_vector_to_queue(amplitude_filename, timestamp, std::vector<double>{car_target_amplitude});
    }

    stop_audio_stream();

    // Log that experiment has ended
    elapsed_time = (timestamp - experiment_start) / 1000000.0;
    recorder->add_string_to_queue(log_filename, timestamp,
                                  fmt::format("Experiment has stopped after {} seconds (this timestamp inclusive), median is {}", elapsed_time, collected.get_median()));

    // Remove amplitude boundaries from plot
    plotter->hide_amplitude_boundaries();

}


// This callback will be called by the PortAudio engine when audio is needed.
// It may be called at interrupt level on some machines so don't do anything
// that could mess up the system like calling malloc() or free().
static int pa_callback( const void* input_buffer, void* output_buffer,
                        unsigned long frames_per_buffer,
                        const PaStreamCallbackTimeInfo* time_info,
                        PaStreamCallbackFlags status_flags,
                        void* user_data )
{
    Experiment* experiment = static_cast<Experiment*>(user_data);
    return experiment->callback(static_cast<float*>(output_buffer), frames_per_buffer);
}

void Experiment::update_angle_delta(double* angle, double* frequency) {
    double cycles_per_sample = *frequency / SAMPLE_RATE;
    *angle = cycles_per_sample * 2.0 * M_PI;
}

int Experiment::callback(float* out, uint64_t frames_per_buffer) {

    // Trigger "sleepiness number" wav file after wav_interval seconds has passed
    if (elapsed_time >= wav_next && wav_file_finished) {
        wav_file_finished = false;
        wav_next = elapsed_time + wav_interval;
        recorder->add_string_to_queue(log_filename, timestamp, "'Sleepiness Number' triggered");
    }

    // Check if carrier amplitude has changed
    double car_amplitude_increment = (car_target_amplitude - car_current_amplitude) / static_cast<double>(frames_per_buffer);

    // Check if carrier frequency has changed
    double car_frequency_increment = (car_target_frequency - car_current_frequency) / static_cast<double>(frames_per_buffer);

    // Check if modulator frequency has changed
    double mod_frequency_increment = (mod_target_frequency - mod_current_frequency) / static_cast<double>(frames_per_buffer);

    for (uint64_t sample_idx = 0; sample_idx < frames_per_buffer; ++sample_idx) {

        // Get sample (either sine wave or wav file)
        if (!wav_file_finished && wav_sample_idx < wav_buffer.size()) {

            // Output sample
            *out++ = static_cast<float>(wav_buffer[++wav_sample_idx]);

        } else {

            // Don't output audio feedback if audio output is switched off
            if (!audio_output) {
                *out++ = 0;
            } else {

                // Update carrier frequency and amplitude (if necessary)
                if (abs(car_frequency_increment) > 1e-7) {
                    car_current_frequency += car_frequency_increment;
                    update_angle_delta(&car_angle_delta, &car_current_frequency);
                }
                if (abs(car_amplitude_increment) > 1e-7)
                    car_current_amplitude += car_amplitude_increment;
                car_current_angle += car_angle_delta;

                // Get a new sample
                double sample = std::sin(car_current_angle) * car_current_amplitude;

                // Do amplitude modulation if required
                if (amplitude_modulated) {

                    // Update modulator frequency (if necessary)
                    if (abs(mod_frequency_increment) > 1e-7) {
                        mod_current_frequency += mod_frequency_increment;
                        update_angle_delta(&mod_angle_delta, &mod_current_frequency);
                    }

                    // Update modulator angle
                    mod_current_angle += mod_angle_delta;

                    // Get modulator sample
                    double mod_sample = std::sin(mod_current_angle);

                    // Normalise mod_sample to range [0,1]
                    mod_sample = (mod_sample+1)/2;

                    // Apply modulation on sample
                    sample *= mod_sample;
                }

                *out++ = static_cast<float>(sample);
            }
        }
    }

    // Wrap current_angle with 2*pi
    if (car_current_angle > 2.0*M_PI) {
        uint64_t mult = static_cast<uint64_t>(car_current_angle / (2.0*M_PI));
        car_current_angle -= mult*2.0*M_PI;
    }

    // Reset wav file if we've finished playing it
    if (!wav_file_finished && wav_sample_idx >= wav_buffer.size()) {
        wav_file_finished = true;
        wav_sample_idx = 0;
        recorder->add_string_to_queue(log_filename, timestamp, "'Sleepiness Number' finished (this timestamp inclusive)");
    }

    return 0;
}
