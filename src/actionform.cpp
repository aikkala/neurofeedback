#include "actionform.h"
#include "ui_actionform.h"

#include "Module.h"

ActionForm::ActionForm(QWidget* parent, QWidget* plot_widget) :
    QStackedWidget(parent),
    ui(new Ui::ActionForm)
{
    ui->setupUi(this);
    widget = parent;
    this->plot_widget = plot_widget;
}

ActionForm::~ActionForm()
{
    delete ui;
}

void ActionForm::activate_window(Experiment* experiment) {
    set_connections();
    this->experiment = experiment;
    experiment_in_progress = false;

    // Set correct page for default
    this->setCurrentWidget(ui->action_chooser);

    widget->show();
}

void ActionForm::set_connections() {

    // Set experiment button
    QObject::connect(ui->experiment_button, SIGNAL(clicked()), this, SLOT(start_experiment()));

    // Set stop experiment button
    QObject::connect(ui->stop_button, SIGNAL(clicked()), this, SLOT(stop_experiment()));

    // Stop experiment if parent widget is destroyed
    QObject::connect(widget, SIGNAL(destroyed()), SLOT(stop_experiment()));

    // Set quit button
    QObject::connect(ui->quit_button, SIGNAL(clicked()), widget, SLOT(close()));
    QObject::connect(ui->quit_button, SIGNAL(clicked()), plot_widget, SLOT(close()));

    // Allow all Modules to quit this program
    Module::set_quit_button(ui->quit_button);
}

void ActionForm::start_experiment() {
    // Switch page to action_breaker
    this->setCurrentWidget(ui->action_breaker);

    // Set duration_display so we can show elapsed time
    experiment->set_duration_display(ui->duration_display);

    // Set stop button so Experiment can stop when it wants
    experiment->set_stop_button(ui->stop_button);

    // Start experiment in another thread
    experiment_thread = std::thread(&Experiment::deploy, experiment);
    experiment_in_progress = true;
}

void ActionForm::stop_experiment() {
    // Stop experiment and switch page to action_chooser
    if (experiment_in_progress) {
        experiment->stop();
        experiment_thread.join();
        experiment_in_progress = false;
    }
    this->setCurrentWidget(ui->action_chooser);
}
