#include "plotterform.h"
#include "ui_plotterform.h"

#include <QVector>

#include <thread>
#include <chrono>

PlotterForm::PlotterForm(QWidget* parent) :
    QWidget(parent),
    ui(new Ui::PlotterForm)
{
    ui->setupUi(this);
    this->widget = parent;
    widget->show();
}

void PlotterForm::setup_psd_plot(PSDCalculator* psd_calculator) {
    // Plot only frequencies 0-20 for PSD
    double max_frequency = 20;
    std::vector<double> frequencies = psd_calculator->get_frequencies();
    ui->psd_plot->xAxis->setRange(-0.5, max_frequency+0.5);
    ui->psd_plot->yAxis->setRange(0, 10);
    ui->psd_plot->xAxis->setLabel("Hz");
    ui->psd_plot->yAxis->setLabel("Power");

    // Allow zooming/dragging psd_plot
    ui->psd_plot->setInteraction(QCP::iRangeZoom, true);
    ui->psd_plot->axisRect()->setRangeZoom(Qt::Vertical);
    ui->psd_plot->setInteraction(QCP::iRangeDrag, true);

    // Plot average PSD as a bar plot
    bars = new QCPBars(ui->psd_plot->xAxis, ui->psd_plot->yAxis);
    bars->setAntialiased(false);
    bars->setStackingGap(1);
    bars->setWidth(0.35);

    // Pre-allocate psd_plot's data container with data
    QVector<QCPBarsData> init_data(frequencies.size());
    for (int i = 0; i < init_data.size(); ++i) {
        init_data[i].key = frequencies[i];
        init_data[i].value = 0;
    }
    bars->data()->set(init_data);

}

void PlotterForm::setup_track_plot(ProgramForm* pf) {

    // Use a little arrow to indicate whether goal is to increase or decrease
    std::string ylabel;
    if (!pf->get_audio_output())
        ylabel = tracked;
    else if (pf->get_goal() == "decrease")
        ylabel = fmt::format("<- {}", tracked);
    else
        ylabel = fmt::format("{} ->", tracked);
    ui->track_plot->yAxis->setLabel(QString::fromStdString(ylabel));

    // Set ranges and remove ticks
    ui->track_plot->xAxis->setTicks(false);
    ui->track_plot->xAxis->setRange(0, 2*track_size);
    if (tracked == "theta-alpha variance")
        ui->track_plot->yAxis->setRange(0, 1);
    else
        ui->track_plot->yAxis->setRange(0, 2);

    // Allow zooming/dragging track_plot
    ui->track_plot->setInteraction(QCP::iRangeDrag, true);
    ui->track_plot->axisRect()->setRangeDrag(Qt::Vertical);
    ui->track_plot->setInteraction(QCP::iRangeZoom, true);
    ui->track_plot->axisRect()->setRangeZoom(Qt::Vertical);

    // Plot history of tracked values as a line
    ui->track_plot->addGraph(); // graph number 1

    // Pre-allocate track_plot's data container with data
    QVector<QCPGraphData> init_data(track_size);
    for (int i = 0; i < init_data.size(); ++i) {
        init_data[i].key = i;
        init_data[i].value = 0;
    }
    ui->track_plot->graph(0)->data()->set(init_data);

    // Set graph's color and width
    QPen pen;
    pen.setColor(QColor(0, 0, 255, 150));
    pen.setWidth(2);
    ui->track_plot->graph(0)->setPen(pen);

    // Plot a marker also
    ui->track_plot->addGraph();

    // Pre-allocate marker
    QVector<double> x(1), y(1);
    x[0] = track_size;
    y[0] = 0;
    ui->track_plot->graph(1)->setData(x,y);
    ui->track_plot->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 6));

    // Plot experiment baseline (and hide it for now)
    ui->track_plot->addGraph(); // graph number 2
    ui->track_plot->graph(2)->setVisible(false);
    ui->track_plot->graph(2)->setPen(QPen(QColor(204,8,8),2));

    // Pre-allocate baselines
    QVector<QCPGraphData> baseline(2);
    baseline[0].key = 0;
    baseline[1].key = 2*track_size;
    baseline[0].value = baseline[1].value = 0;
    ui->track_plot->graph(2)->data()->set(baseline);

    // Plot also amplitude boundaries so we have some idea what's the amplitude of audio output
    ui->track_plot->addGraph(); // graph number 3
    ui->track_plot->addGraph(); // graph number 4
    hide_amplitude_boundaries();

    // Pre-allocate boundaries
    QVector<QCPGraphData> upper_boundary_vector(2), lower_boundary_vector(2);
    upper_boundary_vector[0].key = lower_boundary_vector[0].key = 0;
    upper_boundary_vector[1].key = lower_boundary_vector[1].key = 2*track_size;
    lower_boundary_vector[0].value = lower_boundary_vector[1].value = 0;
    upper_boundary_vector[0].value = upper_boundary_vector[1].value = 0;
    ui->track_plot->graph(3)->data()->set(upper_boundary_vector);
    ui->track_plot->graph(4)->data()->set(lower_boundary_vector);

    // Fill the area between boundaries; use a different color when randomise is on
    ui->track_plot->graph(3)->setPen(Qt::NoPen);
    ui->track_plot->graph(4)->setPen(Qt::NoPen);
    QColor color(0,0,0,10);
    if (pf->get_randomise())
        color.setRed(255);
    else
        color.setBlue(255);
    ui->track_plot->graph(3)->setBrush(QBrush(color));
    ui->track_plot->graph(3)->setChannelFillGraph(ui->track_plot->graph(4));

}

PlotterForm::~PlotterForm()
{
    delete ui;
    delete signal_emitter;

    // The program crashes if we try to delete bars here. I'm fairly sure QCustomPlot takes care of deleting that object.
    //delete bars;
}

void PlotterForm::activate_window(SharedBuffer* psd_buffer, SharedBuffer* track_buffer, PSDCalculator* psd_calculator, ProgramForm* pf) {

    // Use session name as window's name
    std::string session = pf->get_session();
    if (session == "")
        session = fmt::format("Unnamed session (cap: {})", pf->get_cap());
    else
        session = fmt::format("Session: {} (cap: {})", session, pf->get_cap());
    parentWidget()->setWindowTitle(QString::fromStdString(session));

    // Get track buffer's size
    track_size = track_buffer->get_buffer_size();

    // Get PSD buffer's size
    psd_size = psd_buffer->get_buffer_size();

    // Set buffers
    this->psd_buffer = psd_buffer;
    this->track_buffer = track_buffer;

    // We need tracked measure's name
    tracked = pf->get_measure();

    // experiment tells us whether experiment baseline should be plotted
    experiment = false;

    // Setup graphs
    setup_track_plot(pf);
    setup_psd_plot(psd_calculator);

    // Send signals when plot should be updated
    signal_emitter = new QTimer(this);
    connect(signal_emitter, SIGNAL(timeout()), this, SLOT(update_plot()));

    // Update every 20 milliseconds
    signal_emitter->start(20);
}

void PlotterForm::start_experiment() {
    experiment = true;
    ui->track_plot->graph(2)->setVisible(true);
}

void PlotterForm::set_experiment_baseline(double experiment_baseline) {
    std::lock_guard<std::mutex> guard(mutex);
    ui->track_plot->graph(2)->data()->begin()->value = experiment_baseline;
    (ui->track_plot->graph(2)->data()->begin()+1)->value = experiment_baseline;
}

void PlotterForm::set_amplitude_boundaries(double lower_boundary, double upper_boundary) {
    std::lock_guard<std::mutex> guard(mutex);
    ui->track_plot->graph(3)->data()->begin()->value = upper_boundary;
    (ui->track_plot->graph(3)->data()->begin()+1)->value = upper_boundary;
    ui->track_plot->graph(4)->data()->begin()->value = lower_boundary;
    (ui->track_plot->graph(4)->data()->begin()+1)->value = lower_boundary;
    ui->track_plot->graph(3)->setVisible(true);
    ui->track_plot->graph(4)->setVisible(true);
}

void PlotterForm::hide_amplitude_boundaries() {
    ui->track_plot->graph(3)->setVisible(false);
    ui->track_plot->graph(4)->setVisible(false);
}

void PlotterForm::update_plot() {

    // Copy data from psd_buffer into psd_plot
    psd_buffer->template copy_channel_into_qcp<QCPBarsData>(tracked, bars->data());

    // Copy data from track_buffer into track_plot
    track_buffer->template copy_channel_into_qcp<QCPGraphData>(tracked, ui->track_plot->graph(0)->data());

    // Update marker location
    ui->track_plot->graph(1)->data()->begin()->value = (ui->track_plot->graph(0)->data()->end()-1)->value;

    // Replot (mutexed so that we don't update baselines while replotting)
    {
        std::lock_guard<std::mutex> guard(mutex);
        ui->track_plot->replot();
        ui->psd_plot->replot();
    }

}
