cmake_minimum_required(VERSION 3.10.2)
set(PROJECT_NAME NeuroFeedback)
project(${PROJECT_NAME})

# Set release build if build type has not been defined
if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release)
endif()

# Include project's headers
include_directories(include)

# Add sources
file(GLOB SOURCES "src/*.cpp")

# Include EGI's "base" and "sdk" headers and sources if building with EGI
option(BUILD_WITH_EGI "Build with EGI's source code and libraries" OFF) # OFF by default
if (BUILD_WITH_EGI)
    message("-- Building with EGI")
    include_directories(include/EGI/base include/EGI/sdk)
    set(SOURCES ${SOURCES} "src/EGI/DataStreamObserver.cpp")
else()
    message("-- Not building with EGI")
endif()

# Find FFTW, link float library and include headers
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${PROJECT_SOURCE_DIR}/cmake/findFFTW")
find_package(FFTW REQUIRED)

# Find PortAudio, link library and include headers
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${PROJECT_SOURCE_DIR}/cmake/findPortaudio")
find_package(Portaudio REQUIRED)

# Find libsndfile
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${PROJECT_SOURCE_DIR}/cmake/findLibSndFile")
find_package(LibSndFile REQUIRED)

# Find zlib
find_package(ZLIB REQUIRED)

# Find fmt
find_package(fmt REQUIRED)

# Set some variables needed for Qt, and find Qt5
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
find_package(Qt5Widgets CONFIG REQUIRED)
find_package(Qt5PrintSupport REQUIRED)

# Need to add Qt headers into executable, otherwise moc won't parse them
set(QT_HEADERS ${PROJECT_SOURCE_DIR}/include/programform.h ${PROJECT_SOURCE_DIR}/include/actionform.h ${PROJECT_SOURCE_DIR}/include/qcustomplot.h ${PROJECT_SOURCE_DIR}/include/plotterform.h)

# Set flags
set(CMAKE_CXX_FLAGS " ${CMAKE_CXX_FLAGS} -Wno-multichar -Wall -Werror -std=c++11 ")

# Create the executable
add_executable(${PROJECT_NAME} ${SOURCES} ${QT_HEADERS})

# Add PROJECT_SOURCE_DIR as a preprocessor macro that we can use in the program
target_compile_definitions(${PROJECT_NAME} PRIVATE PROJECT_SOURCE_DIR="${PROJECT_SOURCE_DIR}")

# Define BUILD_WITH_EGI as a preprocessor macro that we can use in the program
if (BUILD_WITH_EGI)
    target_compile_definitions(${PROJECT_NAME} PRIVATE BUILD_WITH_EGI=BUILD_WITH_EGI)
endif()

# Link required libraries
target_link_libraries(${PROJECT_NAME} -lpthread -lm -lstdc++fs)

# Link EGI's libraries if building with EGI
if (BUILD_WITH_EGI)
    target_link_libraries(${PROJECT_NAME} ${PROJECT_SOURCE_DIR}/lib/libAS_Network_Client.a ${PROJECT_SOURCE_DIR}/lib/libAS_Network_Server.a)
endif()

# Link required 3rd party libraries
target_link_libraries(${PROJECT_NAME} ${FFTW_DOUBLE_LIB} ${PORTAUDIO_LIBRARIES} ${LIBSNDFILE_LIBRARIES} ${ZLIB_LIBRARIES} fmt::fmt Qt5::Widgets Qt5::PrintSupport)

# Unset BUILD_WITH_EGI in case we forget to remove cache
unset(BUILD_WITH_EGI CACHE)
