#ifndef EGIAMP_H
#define EGIAMP_H

/**@file
 *  EGIAmp Header File
 *  @author Aleksi Ikkala
 *  @date 2019/02/15
 *  @remarks Adapted from Robert Bell's "SimpleClient" example (from AmpServerProSDK-2.1)
 */

#include "SharedBuffer.h"
#include "Source.h"

#include "AS_Network_Client_Base.h"
#include "DataStreamObserver.h"

#include "fmt/format.h"

/**
 * Define a Source class for retrieving data from an EGI amplifier
 */
class EGIAmp : public Source {

public:

    /**
     *  Constructor
     */
    EGIAmp(Recorder* recorder) {

        // Set up recorder for logging
        this->recorder = recorder;
        log_filename = "EGIAmp.log";
        recorder->open_file(log_filename, "log");
        recorder->add_string_to_queue(log_filename, "# timestamp message");

        // Not initialised until we've successfully connected to an Amp Server
        initialised = false;

        // List all known IP addresses of acquisition Macs here
        ip_addresses.push_back("10.0.0.42");
        ip_addresses.push_back("10.10.10.51"); // this is the ip address of the N400 amplifier, not the mac

        // Connect to AMP server
        if (!detect_amp_server() || !connect_to_amp_server()) {
            printf("Couldn't connect to Amp Server. Make sure that you're connected to the acquisition Mac via an Ethernet cable, and that you're using the correct Ethernet "
                   "network: 'Acquisition Mac N300' or 'Acquisition Mac N400' in the desktop computer. The subnet IP 'Acquisition Mac N300' uses is 10.0.0.43, and the "
                   "subnet IP 'Acquisition Mac N400' uses is 10.10.10.42. The subnet IPs of respective acquisition Macs should be 10.0.0.42 and 10.10.10.41 (you can check "
                   "them through System Preferences -> Network); these IPs should match those defined in EGIAmp's constructor.\n");
            initialised = false;
            return;
        }
        set_num_channels(static_cast<uint64_t>(amp_info.numberOfChannels));

    }

    /**
     *  Destructor
     */
    ~EGIAmp() {
        // Make sure we've removed all data stream observers
        client->deleteAllDataStreamObservers();

        // Free memory
        RELEASE(client);
        delete client;
    }

    /**
     * Detect which Amp Server to use
     */
    bool detect_amp_server() {

        // Creat a client
        client = new AS_Network_Client();

        // Create detection preferences
        AS_Network_Types::DetPrefsRef det_prefs = AS_Network_Types::createDetPrefs();

        // Loop over all known IP addresses
        bool found = false;
        for (const std::string& ip_address : ip_addresses) {

            // Set new address
            delete[] det_prefs->serverAddress;
            det_prefs->serverAddress = new char[strlen(ip_address.c_str()) + 1];
            strcpy(det_prefs->serverAddress, ip_address.c_str());

            // Detect if there's an Amp Server in this address
            int64_t timeout = 200000;
            if (client->detectAmpServer(det_prefs, timeout)) {
                // A server was found
                amp_server_address = ip_address;
                found = true;
                break;
            }

        }

        // De-allocate memory
        delete[] det_prefs->serverAddress;
        delete det_prefs;

        return found;
    }

    /**
     * Try to connect to an Amp Server
     */
    bool connect_to_amp_server() {

        // Create a connection prefs instance and change the server address
        AS_Network_Types::ConnPrefsRef conn_prefs = AS_Network_Types::createConnPrefs();
        delete[] conn_prefs->serverAddress;
        conn_prefs->serverAddress = new char[strlen(amp_server_address.c_str()) + 1];
        strcpy(conn_prefs->serverAddress, amp_server_address.c_str());

        // We don't care about notifications (what about commands?)
        conn_prefs->notificationProtocol = AS_Network_Types::AS_NONE;

        recorder->add_string_to_queue(log_filename, 0, fmt::format("Attempting to connect to Amp Server at address {} ...", amp_server_address));
        if (client->connect(conn_prefs) != 0) {
            recorder->add_string_to_queue(log_filename, 0, "Failed to connect to Amp Server");
        } else {
            initialised = true;
            recorder->add_string_to_queue(log_filename, 0, "Connected to Amp Server");
        }

        // De-allocate ConnPrefs
        delete[] conn_prefs->serverAddress;
        delete conn_prefs;

        // Return if connection was unsuccessful
        if (!initialised)
            return false;

        recorder->add_string_to_queue(log_filename, 0, "Looking for amplifiers ...");
        if (client->getNumberOfAmps() == 0) {
            recorder->add_string_to_queue(log_filename, 0, "Failed, no amplifiers detected");
            return false;
        } else if (client->getNumberOfAmps() > 1) {
            recorder->add_string_to_queue(log_filename, 0, fmt::format("Too many amplifiers ({}) detected!", client->getNumberOfAmps()));
            return false;
        } else {
            recorder->add_string_to_queue(log_filename, 0, "Success, amplifier detected");
        }

        // Get amplifier information
        amp_info = client->getAmpInfo(0);
        recorder->add_string_to_queue(log_filename, 0, fmt::format("Detected amplifier's serial number is {}", amp_info.serialNumber));
        recorder->add_string_to_queue(log_filename, 0, fmt::format("Number of EEG channels is {}", amp_info.numberOfChannels));

        if (amp_info.packetType == AS_Network_Types::pkType1) {
            recorder->add_string_to_queue(log_filename, 0, "Amplifier is using packet type 1");
        } else if (amp_info.packetType == AS_Network_Types::pkType2) {
            recorder->add_string_to_queue(log_filename, 0, "Amplifier is using packet type 2");
        } else {
            recorder->add_string_to_queue(log_filename, 0, "Error: could not determine amplifier packet type");
            return false;
        }

        return true;
    }


    /**
     * Starts to upload samples into data_buffer
     */
    void run() {

        // Make sure buffer and sample_rate have been defined
        if (!buffer || !sample_rate) {
            std::string msg = "Buffer or sample rate has been not set, will not run EGIAmp";
            printf("%s\n", msg.c_str());
            recorder->add_string_to_queue(log_filename, 0, msg);
            set_status(false);
            return;
        } else if (sample_rate != 250) {
            std::string msg = fmt::format("EGIAmp currently supports only 250 Hz sample rate, but a sample rate of {} Hz was given, will not run EGIAmp", sample_rate);
            printf("%s\n", msg.c_str());
            recorder->add_string_to_queue(log_filename, 0, msg);
            set_status(false);
            return;
        }

        // Set up the observer
        recorder->add_string_to_queue(log_filename, 0, "Adding data observer");
        observer = new DataStreamObserver(client, buffer, recorder);
        client->addDataStreamObserver(observer, 0, NULL);

    }

    /**
     * Stop DataStreamObserver
     */
    void stop() {
        client->deleteDataStreamObserver(observer);
        delete observer;
        set_status(false);
    }

private:

    // Params for connecting to EGI amplifier
    std::string amp_server_address;
    AS_Network_Client* client = nullptr;
    DataStreamObserver* observer = nullptr;
    AS_Network_Types::AmpInfo amp_info;

    // This list holds all known IP addresses of acquisition Macs
    std::vector<std::string> ip_addresses;

    // For logging
    std::string log_filename;
    Recorder* recorder;

};

#endif // EGIAMP_H
