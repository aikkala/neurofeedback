#ifndef DATA_STREAM_OBSERVER_H
#define DATA_STREAM_OBSERVER_H

/**@file
 *  DataStreamObserver header file
 *  @author Aleksi Ikkala
 *  @date 2019/04/03
 *  @remarks Adapted from Robert Bell's "DataStreamObserver" header file (from AmpServerProSDK-2.1)
 */

#include "AS_Network_Client_Base.h"

#include "SharedBuffer.h"
#include "Recorder.h"

#include <iostream>
#include <pthread.h>
#include <sched.h>

class DataStreamObserver: public EGIBase::ObserverPattern::Observer {

public:

    /**
     * Constructor
     */
    DataStreamObserver(AS_Network_Client* client, SharedBuffer* sharedBuffer, Recorder* recorder);

	/**
     *  Destructor
	 */
	virtual ~DataStreamObserver();

	/**
     *  This method is called when the object being observed calls its notify method
	 */
    virtual void update(EGIBase::ObserverPattern::Observable *o, EGIBase::EGIObject *eObject);

private:

    // Both NA300 and NA400 amps seem to output samples with a rate of 1000 Hz regardless of what decimated rate is set
    const uint64_t SAMPLING_RATE = 1000;

    AS_Network_Types::AmpInfo amp_info;
    double scaling_factor;

    // Use sample count as a timestamp for N300 (not ideal, causes problems if sampling rate is not 1000 Hz)
    uint64_t sample_count;

    // We need to track when next update is due, and how many samples we're downsampling
    uint64_t previous_timestamp;
    uint64_t batch_size;
    std::vector<double> cumulative_values;

	// A shared buffer
    SharedBuffer* data_buffer;

    // Log sample rates and other important messages
    std::chrono::steady_clock::time_point previous_sample_time, session_start;
    std::string log_filename;
	Recorder* recorder;

	// Flag for big endianness
	bool big_endian;

};

#endif // DATA_STREAM_OBSERVER_H
