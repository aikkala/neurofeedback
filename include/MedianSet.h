/*
 * MedianSet.h
 *
 * A class to calculate median / percentiles of collected values
 *
 *  Created on: 01 Feb 2019
 *      Author: Aleksi Ikkala
 */

#ifndef INCLUDE_MEDIANSET_H_
#define INCLUDE_MEDIANSET_H_

#include <set>
#include <iostream>
#include <mutex>
#include <vector>

class MedianSet {

public:

	/**
	 *  Contructor
	 */
	MedianSet() {
		size = 0;
	};

	/**
	 *  Destructor
	 */
	virtual ~MedianSet() {
	};

	/**
	 * Insert a value
	 */
    void insert(const double& value) {

		std::lock_guard<std::mutex> guard(mutex);

		// Insert new value
		set_values.insert(value);
		vector_values.push_back(value);
    }

	/**
	 * Return median value
	 */
    double get_median() {
		std::lock_guard<std::mutex> guard(mutex);

		// Return zero if there are no values in the set
		if (set_values.size() == 0) {
			return 0;
		}

		// Get iterator to median sample (or upper bound of median if there is even number of values)
        std::multiset<double>::iterator median_it = std::next(set_values.begin(), set_values.size() / 2);

		if (set_values.size() % 2 == 0) {
			return (*median_it + *std::prev(median_it, 1))/2;
		} else {
			return *median_it;
		}
	}

	/**
	 * Clear all values
	 */
	void clear() {
		std::lock_guard<std::mutex> guard(mutex);

		vector_values.clear();
		set_values.clear();
	}

	/**
	 * Return set values
	 */
    std::multiset<double> get_set() {
		std::lock_guard<std::mutex> guard(mutex);
		return set_values;
	}

	/**
	 * Return vector values
	 */
    std::vector<double> get_vector() {
		std::lock_guard<std::mutex> guard(mutex);
		return vector_values;
    }

	/**
	 * Return set size
	 */
	uint64_t get_size() {
		return set_values.size();
    }

	/**
	 * Return percentiles
	 */
    std::vector<double> get_percentiles(std::vector<double> prctile) {
		std::lock_guard<std::mutex> guard(mutex);

		// Initialise a vector
        std::vector<double> values;
		values.reserve(prctile.size());

		// Make sure set is not empty
		if (set_values.size()== 0) {
			return values;
		}

		// Find queried percentiles
        for (const double& prc : prctile) {
			// Percentile must be between 0 and 100
			if (prc < 0 || prc > 100) {
				values.push_back(0);
			} else {
				// Find percentile
				uint64_t idx = (prc/100)*(set_values.size()-1);
                std::set<double>::iterator it = set_values.begin();
				std::advance(it, idx);
				values.push_back(*it);
			}
		}

		return values;
	}

private:
	std::mutex mutex;

	// Window size
	uint64_t size;

	// Set containing the values (in sorted order); multiset allows duplicate values
    std::multiset<double> set_values;

	// Let's save the values in chronological order as well
    std::vector<double> vector_values;
};




#endif /* INCLUDE_MEDIANSET_H_ */
