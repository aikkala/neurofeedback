#ifndef PROGRAMFORM_H
#define PROGRAMFORM_H

#include <QWidget>

#include "fmt/format.h"

#include "Source.h"
#include "Recorder.h"

namespace Ui {
class ProgramForm;
}

class ProgramForm : public QWidget
{
    Q_OBJECT

public:
    explicit ProgramForm(QWidget* parent = nullptr);
    ~ProgramForm();
    void set_connections();
    void activate_window();

    // Register alternative options
    void register_sources(std::vector<std::string> sources);
    void register_measures(std::vector<std::string> measures);
    void register_caps(std::vector<std::string> caps);

    // Write all condition and program related info into a file
    void write_condition_info(Recorder* recorder);
    void write_session_end(Recorder* recorder);

    // Getters
    double get_amplitude_modulation();
    bool get_amplitude_alteration();
    bool get_audio_output();
    bool get_randomise();
    std::string get_randomised_data_location();
    std::string get_wav_file_location();
    std::string get_session();
    std::string get_source();
    std::string get_cap();
    std::string get_measure();
    std::string get_record_data_folder();
    std::string get_goal();
    double get_audio_upper_boundary();
    double get_audio_lower_boundary();
    double get_audio_midpoint();
    std::vector<std::string> get_psd_mappings();
    std::vector<uint64_t> get_channels();
    double get_smooth_window_size(); // in seconds
    double get_sound_frequency();
    double get_sound_amplitude();
    uint64_t get_downsample_rate();
    double get_psd_window_size();
    uint64_t get_psd_calculation_rate();
    double get_track_history_window_size();
    uint64_t get_timeout();

private slots:
    void set_amplitude_modulation();
    void switch_amplitude_alteration();
    void switch_audio_output();
    void switch_randomise();
    void set_session();
    void set_source(const QString& text);
    void set_measure(const QString& text);
    void set_goal(const QString& text);
    void set_cap(const QString& text);

private:
    void set_measure_params();
    void set_channels();

private:
    // Needed for GUI
    Ui::ProgramForm* ui;
    QWidget* widget;

    // Write condition info into a log file
    std::string log_filename;

    // Needed for program
    std::string session;
    bool amplitude_alteration, audio_output, randomise;
    std::string randomised_data_location, wav_file_location;
    double amplitude_modulation;
    std::string source;
    std::string measure;
    std::string program_data_folder;
    std::string record_data_folder;
    std::string goal;
    std::string cap;

    // PSD calculation related parameters (these would ideally be somewhere else, maybe in a measure
    // related base-class / interface; perhaps in the future)
    uint64_t psd_channels;
    std::vector<std::string> psd_mappings;
    double smooth_window_size; // in seconds
    std::vector<uint64_t> channels;
    double sound_frequency, randomised_sound_frequency, sound_amplitude, randomised_sound_amplitude;

    // Experiment / audio related parameters
    double audio_upper_boundary, audio_lower_boundary, audio_midpoint;

    // Let's collect all program related parameters here; these should be held fixed unless you know what you're doing
    const uint64_t downsample_rate = 250; // in Hz
    const double psd_window_size = 2; // in seconds
    // Note: "randomise" condition assumes tracked measure is being calculated with psd_calculation_rate = 50,
    // (or the data files have been recorded with that rate), so if you change psd_calculation_rate you have
    // to re-do those data files (with matlab-script "epoch_measure_for_randomisation.m")
    uint64_t psd_calculation_rate = 50; // in Hz

    // How many past seconds are plotted for tracked value
    double track_history_window_size = 5;

    // If no new data is uploaded in 'timeout' seconds the program exits
    uint64_t timeout;
};

#endif // PROGRAMFORM_H
