#ifndef EXPERIMENT_H
#define EXPERIMENT_H

/**@file
 *  Experiment Header File
 *  @author Aleksi Ikkala
 *  @date 2018/12/14
 *  @remarks
 */

#include <QLabel>

#include <condition_variable>
#include <random>

#include "sndfile.h"

#include "Module.h"
#include "MedianSet.h"
#include "portaudio.h"
#include "SharedBuffer.h"
#include "Recorder.h"
#include "plotterform.h"
#include "programform.h"

/**
 *
 */
class Experiment : public Module {

public:

    /**
     *  Contructor
     */
    Experiment(SharedBuffer* track_buffer, PlotterForm* plotter, Recorder* recorder, ProgramForm* pf);

    /**
     *  Destructor
     */
    ~Experiment();

    /**
     * Returns tracked values collected during experiment
     */
    std::vector<double> get_experiment_values();

    /**
     * Callback for generating audio signal to be fed into audio buffer
     */
    int callback(float* output_buffer, uint64_t frames_per_buffer);

    /**
     * Set duration display to be called
     */
    void set_duration_display(QLabel* duration_display);

    /**
     * Set stop_button so we can stop the experiment via GUI if needed
     */
    void set_stop_button(QPushButton* stop_button);

    /**
     * Returns true if initialised has been successful
     */
    bool is_initialised();

private:
    /**
     * Runs the experiment. Don't call this to run, call Module::deploy()
     */
    void run();

    /**
     * Set elapsed time in ActionForm
     */
    void set_elapsed_time();

    /**
     * Update how much angle should increase after each sample
     */
    void update_angle_delta(double* angle, double* frequency);

    /**
     * Check for error, return true if error has occurred
     */
    bool check_for_error(PaError err);

    /**
     * Start audio stream, returns true if stream has been opened successfully
     */
    bool start_audio_stream();

    /**
     * Stop audio stream, returns true if stream has been closed successfully
     */
    bool stop_audio_stream();

    /**
     * Read randomised data if we're running a randomised condition
     */
    bool read_randomised_data(std::string randomised_data_location);

    /**
     * Read randomised value from randomised_data
     */
    double read_randomised_value();

    /**
     * Read a wav file
     */
    bool read_wav_file(std::string wav_file);

private:

    // Audio stream
    PaStream *stream;
    bool audio_output;

    // For playing a wav file ("Sleepiness Number")
    SNDFILE* file;
    SF_INFO info;
    std::vector<double> wav_buffer;
    uint64_t wav_sample_idx;
    bool wav_file_finished;
    double wav_interval, wav_next, elapsed_time;
    std::string data_folder;

    // For observing updates
    uint64_t update_interval;
    SharedBuffer* track_buffer;
    std::string tracked;
    uint64_t timestamp;

    // Stop after timeout if no new data has been observed
    uint64_t timeout;

    // Experiment duration in seconds
    double experiment_duration;

    // Allow Experiment to control ActionForm's stop_button
    QPushButton* stop_button;

    // Collect tracked values during experiment for statistical testing
    MedianSet collected;

    // Variables for changing amplitude or frequency of carrier signal
    double car_current_amplitude, car_target_amplitude;
    double car_target_frequency, car_current_frequency;
    double car_angle_delta, car_current_angle;
    bool amplitude_altered;
    double upper_boundary, lower_boundary, midpoint;

    // Variables for changing amplitude of modulator signal
    double mod_target_frequency, mod_current_frequency;
    double mod_angle_delta, mod_current_angle;
    bool amplitude_modulated, mod_freq_altered;

    // A bunch of default values that should not be changed
    const uint64_t SAMPLE_RATE = 44100;
    const uint64_t FRAMES_PER_BUFFER = 882;
    double DEFAULT_CARRIER_FREQUENCY;
    double DEFAULT_CARRIER_AMPLITUDE;
    double DEFAULT_MODULATOR_FREQUENCY;
    const std::vector<double> DEFAULT_MODULATOR_FREQUENCY_LIMITS{3, 12};

    // For randomisation condition
    bool randomise;
    std::default_random_engine generator;
    std::uniform_int_distribution<unsigned int> uniform_distribution;
    std::vector<std::vector<double>> randomised_data;
    std::string randomised_percentiles_location;
    uint64_t current_epoch;
    uint64_t current_sample;

    // Indicates whether this object has been properly initialised
    bool initialised;

    // For recording start/end time of experiment, data, and/or errors
    Recorder* recorder;
    std::string log_filename, amplitude_filename;

    // For changing plot titles / plotting experiment baseline
    PlotterForm* plotter;
    QLabel* duration_display;

};

#endif // EXPERIMENT_H
