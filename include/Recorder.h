#ifndef RECORDER_H
#define RECORDER_H

/**@file
 *  Recorder Header File
 *  @author Aleksi Ikkala
 *  @date 2019/01/17
 *  @remarks
 */

#include <mutex>
#include <deque>
#include <string>
#include <fstream>
#include <vector>
#include <map>
#include <memory>

// Let's use zlib to write into a compressed file
#include "zstr/zstr.hpp"

#include "Module.h"


/**
 *
 */
class Recorder : public Module {

    // Forward declaration of Writable
    struct Writable;

public:

	/**
	 *  Contructor
	 */
    Recorder(std::string session_name, std::string record_folder, bool replay);

	/**
	 *  Destructor
	 */
	~Recorder();

	/**
	 * Opens a file for writing
	 */
	void open_file(std::string filename, std::string subfolder);

	/**
	 * Add a vector into queue
	 */
    void add_vector_to_queue(std::string filename, uint64_t timestamp, std::vector<double> values, std::string msg="");

	/**
     * Add a string into queue
	 */
	void add_string_to_queue(std::string filename, uint64_t timestamp, std::string msg);

    /**
     * Add a string into queue without timestamp
     */
    void add_string_to_queue(std::string filename, std::string msg);

    /**
     * Stop the recorder when all messages have been written
     */
    void stop();

    /**
     * Return session start time
     */
    std::string get_datetime();

    /**
     * Close all files
     */
    void close_files();

    /**
     * Closes a file
     */
    void close_file(std::string filename);

private:
    /**
     * Waits for data to be pushed into the buffer, and then writes them to file
     */
    void run();

    /**
     * Push a writable piece of data to queue
     */
    void push_writable_to_queue(Writable& data);

    /**
     * Allow incoming data to be written or not
     */
    void allow_new_data(bool value);

    /**
     * Check whether we're allowed to write new data or not
     */
    bool get_new_data_guard();

private:

	struct Writable {
		struct Entry {
            std::vector<double> values;
			std::string message;
		} entry;
		uint64_t timestamp;
		std::string filename;
        bool print_timestamp;
	};

    std::mutex mutex, new_data_mutex;

    // Boolean to indicate whether recorder is accepting new incoming data
    bool new_data_guard;

    // Keep the messages in a queue
	std::deque<Writable> queue;

    // Timeout for waiting until messages have been written when recorder is stopped (in seconds)
    uint64_t timeout;

    // Start time in UTC Unix time. Note: this isn't the actual time when recording starts;
    // recording starts when a Recorder object is deployed, which is typically one or two seconds after the object has been created
    std::string datetime;

    // Folder and files
	std::string folder;
    std::map<std::string, std::unique_ptr<std::ostream>> files;
};

#endif // RECORDER_H
