#ifndef REPLAY_H
#define REPLAY_H

/**@file
 *  Replay Header File
 *  @author Aleksi Ikkala
 *  @date 2019/02/15
 *  @remarks
 */


#include "SharedBuffer.h"
#include "Source.h"

#include "zstr/zstr.hpp"

#include <chrono>
#include <thread>


/**
 *
 */
class Replay : public Source {

public:

    /**
     *  Constructor
     */
    Replay(std::string filename) : stream(new zstr::ifstream(filename)) {

        // Read number of channels
        read_num_channels();

        initialised = true;
    }

    /**
     * Read number of channels from the input file
     */
    void read_num_channels() {

        // First we need to skip all lines that start with a hash tag (header)
        std::string s;
        getline(*stream, s);
        while (s.size() > 0 && s[0] == '#')
            getline(*stream, s);

        // Parse first line of data and check how many channels there are
        std::stringstream ss(s);

        // Read timestamp
        ss >> s;
        first_timestamp = std::stoi(s);

        // Read string
        ss >> s;

        // Read data
        uint64_t channel_idx = 0;
        while (ss >> s) {
            ++channel_idx;
        }

        set_num_channels(channel_idx);
    }

    /**
     * Starts to upload samples into data_buffer
     */
    void run() {

        // Make sure buffer and sample_rate have been defined
        if (!buffer || !sample_rate) {
            printf("Buffer or sample rate has been not set, will not run Replay\n");
            set_status(false);
            return;
        }

        // We need a reference timepoint
        std::chrono::steady_clock::time_point session_start = std::chrono::steady_clock::now();

        // Start reading data from the file
        std::string s;
        while (get_status() && getline(*stream, s)) {

            // Parse the input line into numbers (and a string)
            std::stringstream ss(s);

            // Read timestamp
            ss >> s;
            uint64_t timestamp = std::stoi(s);

            // Wait until we've passed this timestamp
            while (true) {
                uint64_t elapsed = std::chrono::duration_cast<std::chrono::microseconds> (std::chrono::steady_clock::now() - session_start).count();
                if (elapsed >= timestamp - first_timestamp)
                    break;
                else
                    std::this_thread::sleep_for(std::chrono::microseconds(((timestamp-first_timestamp) - elapsed)/2));
            }

            // Read string (don't need it for anything)
            ss >> s;

            // Read data
            uint64_t channel_idx = 0;
            while (ss >> s) {
                buffer->add_sample(channel_idx++, std::stof(s));
            }

            // Notify data_buffer's subscribers that it has been updated
            buffer->notify(timestamp);

        }

        // End of samples
        set_status(false);
    }

private:
    std::unique_ptr<zstr::ifstream> stream;
    uint64_t first_timestamp;
};

#endif // REPLAY_H
