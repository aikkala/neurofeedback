#ifndef MODULE_H
#define MODULE_H

/**@file
 *  Module Header File
 *  @author Aleksi Ikkala
 *  @date 2019/03/11
 *  @remarks
 */

#include <mutex>
#include <QPushButton>

/**
 * This class defines a runnable and stoppable module
 */
class Module {

public:

    /**
     * Constructor
     */
    Module() {
        set_status(false);
    }

    /**
     * Destructor
     */
    virtual ~Module() {}

    /**
     * Start running module after setting status to true
     */
    virtual void deploy() {
        set_status(true);
        run();
    }

    /**
     * Stops the module
     */
    virtual void stop() {
        set_status(false);
    }

    /**
     * Get status
     */
    bool get_status() {
        std::lock_guard<std::mutex> guard(mutex);
        return status;
    }

    /**
     * Set a quit button
     */
    static void set_quit_button(QPushButton* quit_button) {
        Module::quit_button = quit_button;
    }

protected:
    /**
     * Set status
     */
    void set_status(bool status) {
        std::lock_guard<std::mutex> guard(mutex);
        this->status = status;
    }

    /**
     * Quit the program if any module issues a timeout
     */
    void issue_timeout() {
        if (quit_button) {
            printf("Timeout has been triggered, exiting the program. Check logs to see which Module triggered the timeout.\n");
            quit_button->click();
        }
    }

private:
    /**
     * Start running this module
     */
    virtual void run() = 0;

private:
    bool status;
    std::mutex mutex;

    // A quit button for quitting the complete program
    static QPushButton* quit_button;

};

#endif // MODULE_H
