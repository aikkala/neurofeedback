/*
 * LeastSquares.h
 *
 *  Created on: 20 Dec 2018
 *      Author: Aleksi Ikkala
 */

#ifndef INCLUDE_LEASTSQUARES_H_
#define INCLUDE_LEASTSQUARES_H_

#include <cstring>
#include <iostream>

class LeastSquares {

public:

	/**
	 *  Contructor
	 */
    LeastSquares(uint64_t size, double* y = NULL, double* x = NULL) {
		num_points = size;
		sumX = sumXX = sumY = sumXY = 0;

		// If x vector is not provided use [0:size); otherwise copy x
		this->x = new double[size];
		if (!x) {
			for (uint64_t k = 0; k < size; ++k) {
				this->x[k] = k;
			}
		} else {
            std::memcpy(this->x, x, sizeof(double)*size);
		}

		// Calculate X related sums
		for (uint64_t k = 0; k < size; ++k) {
			sumX += this->x[k];
			sumXX += this->x[k] * this->x[k];
		}
		denominator = (num_points * sumXX) - (sumX * sumX);

		// Calculate Y related sums, as well as slope and intercept, if y is given
		if (y) {
			for (uint64_t k = 0; k < size; ++k) {
				sumY += y[k];
				sumXY += y[k] * this->x[k];
			}

			// Calculate slope and intercept
			calculate_slope();
			calculate_intercept();
		}

	};

	/**
	 *  Destructor
	 */
	virtual ~LeastSquares() {
		delete [] x;
	};

	// Calculate slope and intercept again with new y values
    void inline update(double* y) {
		sumXY = sumY = 0;
		for (uint64_t k = 0; k < num_points; ++k) {
			sumY += y[k];
			sumXY += y[k] * x[k];
		}
		calculate_slope();
		calculate_intercept();
	}

	double inline get_intercept() {
		return intercept;
	}

	double inline get_slope() {
		return slope;
	}


private:

	uint64_t num_points;
	double* x;

	double sumX, sumY, sumXX, sumXY, denominator;

	double slope;
	double intercept;

	void inline calculate_intercept() {
		intercept = ((sumY * sumXX) - (sumX * sumXY)) / denominator;
	};

	void inline calculate_slope() {
		if (denominator < 1e-10) {
			slope = 0;
		} else {
			slope = ((num_points * sumXY) - (sumX * sumY)) / denominator;
		}
	};
};




#endif /* INCLUDE_LEASTSQUARES_H_ */
