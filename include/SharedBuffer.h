#ifndef SHARED_BUFFER_H
#define SHARED_BUFFER_H

/**@file
 *  SharedBuffer Header File
 *  @author Aleksi Ikkala
 *  @date 2018/12/14
 *  @remarks
 */

#include <vector>
#include <iostream>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <set>

#include "qcustomplot.h"

#include "Recorder.h"


/**
 *  This class defines a shared buffer that can by used safely from multiple threads
 */
class SharedBuffer {

public:

    /**
     *  Contructor
     */
    SharedBuffer(const uint64_t num_channels, const uint64_t buffer_size, const std::string buffer_name, Recorder* recorder=nullptr);

    /**
     *  Destructor
     */
    ~SharedBuffer();

    /**
     * Add a new sample to buffer
     */
    void add_sample(const uint64_t& channel, const double& sample);

    /**
     * Add a new sample to buffer. Note: assumes there exists a mapping for this name
     */
    void add_sample(const std::string& channel_name, const double& sample);

    /**
     * Copy channel into given array (array must have been allocated before calling this function)
     */
    void copy_channel(const uint64_t& channel, double* copy, const std::string process = "none");

    /**
     * Copy channel into given array (array must have been allocated before calling this function).
     * Note: assumes a mapping exists for this name
     */
    void copy_channel(const std::string& channel_name, double* copy, const std::string process = "none");

    /**
     * Copy channel into given QCustomPlot data container (must've been allocated before calling this function)
     */
    template <class T>
    void copy_channel_into_qcp(const uint64_t& channel, QSharedPointer<QCPDataContainer<T>> copy, const std::string process = "none") {
        // Set mutex guard to make sure other threads aren't writing to buffer while reading it
        std::lock_guard<std::mutex> guard(mutex);

        // Call private method now that we have set the mutex
        _copy_channel_into_qcp(channel, copy, process);
    }

    /**
     * Copy channel into a QCustomPlot data container (must've been allocated before calling this function).
     * Note: assumes a mapping exists for this name
     */
    template <class T>
    void copy_channel_into_qcp(const std::string& channel_name, QSharedPointer<QCPDataContainer<T>> copy, const std::string process = "none") {
        copy_channel_into_qcp(mappings[channel_name], copy, process);
    };

    /**
     * Returns one sample in queried position
     */
    double get_sample(const uint64_t& channel, const uint64_t& sample);

    /**
     * Returns latest sample
     */
    double latest_sample(const uint64_t& channel);

    /**
     * Returns latest sample. Note: assumes that a mapping exists for given name
     */
    double latest_sample(const std::string& channel_name);

    /**
     * Copy buffer into a given 2D array (array must have been allocated before calling this function)
     */
    void copy_buffer(double** copy, std::string process = "none");

    /**
     * Update all samples at once in a channel
     */
    void update(const uint64_t& channel, const std::vector<double>& samples);

    /**
     * Update all samples at once in a channel. Note: assumes there exists a mapping for this name
     */
    void update(const std::string& channel_name, const std::vector<double>& samples);

    /**
     * Returns sum of elements in a channel
     */
    double get_sum(const uint64_t& channel);

    /**
     * Returns sum of elements in a channel. Note: assumes a mapping exists for this name
     */
    double get_sum(const std::string& channel_name);

    /**
     * Returns average of elements in a channel
     */
    double get_average(const uint64_t& channel);

    /**
     * Returns average of elements in a channel. Note: assumes a mapping exists for given name
     */
    double get_average(const std::string& channel_name);

    /**
     * Returns weighted average of elements in a channel
     */
    double get_weighted_average(const uint64_t& channel);

    /**
     * Returns weighted average of elements in a channel. Note: assumes a mapping exits for given name
     */
    double get_weighted_average(const std::string& channel_name);

    /**
     * Returns number of channels
     */
    uint64_t get_num_channels() {
        return num_channels;
    }

    /**
     * Returns size of buffer
     */
    uint64_t get_buffer_size() {
        return buffer_size;
    }

    /**
     * Subscribe to this buffer, i.e., get a condition_variable that is notified when a sample is updated
     */
    std::condition_variable* subscribe() {
        return &cv;
    }

    /**
     * Send a notification that samples have been updated; this should be called always right after samples have been updated
     */
    void notify(const uint64_t& timestamp);

    /**
     * Start recording. By default record new timestamps and not channels. If labels are given use them in the header.
     */
    void start_recording(const std::string type, std::vector<std::string> labels = std::vector<std::string>{});

    /**
     * Get historic sample count (how many samples encountered so far)
     */
    uint64_t get_sample_count() {
        std::lock_guard<std::mutex> guard(timestamp_mutex);
        return sample_count;
    }

    /**
     * Return mutex
     */
    std::mutex* get_cv_mutex() {
        return &cv_mutex;
    }

    /**
     * Return timestamp of latest sample. Note: sample_count and timestamp can be different, timestamp is propagated from
     * data_buffer, i.e., all other buffers are synchronized to data_buffer
     */
    uint64_t get_timestamp();

    /**
     * Set a timestamp
     */
    //void set_timestamp(const uint64_t& timestamp);

    /**
     * Set mappings. Note: There must be one string for each channel
     */
    void set_mappings(const std::vector<std::string> channel_names);
    void set_mappings(const std::set<std::string> channel_names);

    /**
     * Add a mapping. Note: Existing mappings will be overwritten
     */
    void add_mapping(const uint64_t channel_idx, const std::string name);

    /**
     * Returns true if mapping exists
     */
    bool has_mapping(const std::string& name);

    /**
     * Allow user to block data updates (for copying buffer etc.). For every call to block_updates there must be a
     * call to resume_updates until data updates are allowed.
     */
    void block_updates();
    void resume_updates();

    /**
     * Return number of blocks
     */
    uint64_t get_num_blocks();

    /**
     * Block caller thread until enough new datapoints have been uploaded.
     * NOTE! IMPORTANT! After calling this function updates have been blocked,
     * and the user must call resume_updates() (after copying data etc)
     */
    bool wait_for_update(uint64_t& next_update_timestamp, const uint64_t& update_interval, const uint64_t& timeout);

    /**
     * Set expected sample rate
     */
    void set_expected_sample_rate(const uint64_t& expected_sample_rate);


private:
    const std::string buffer_name;
    const uint64_t num_channels;
    const uint64_t buffer_size;
    uint64_t expected_sample_rate;

    double** buffer;
    std::mutex mutex, cv_mutex, timestamp_mutex, block_mutex;
    double* running_sum;
    std::vector<double> weights;
    std::condition_variable cv;
    uint64_t sample_count, timestamp, num_blocks;

    // Map channel names to indices
    std::map<std::string, uint64_t> mappings;
    std::vector<std::string> mappings_keys;

    // For debugging statistics
    uint64_t previous_log_time, log_interval;
    uint64_t previous_sample_count;

    // For logging
    Recorder* recorder;
    bool record, record_channels;
    std::string log_filename, dat_filename;

    /**
     * Copy data in a channel. Note: Lock guard must be set before calling this function. Also note: you want
     * to call public functions "copy_channel" or "copy_buffer" instead of this function.
     */
    void _copy_channel(const uint64_t& channel, double* copy, const std::string process);

    /**
     * Copy data from a channel into a QCustomPlot container. Note: Lock guard must be set before calling this function.
     */
    template <class T>
    void _copy_channel_into_qcp(const uint64_t& channel, QSharedPointer<QCPDataContainer<T>> copy, const std::string process) {
        // Note: lock guard should be set before calling this function

        // Subtract mean or divide with sum if necessary
        double div = 1;
        double mean = 0;
        if (process == "zero_mean") {
            mean = running_sum[channel] / buffer_size;
        } else if (process == "sum_normalise" && abs(running_sum[channel]) > 1e-10) {
            div = running_sum[channel];
        }

        for (uint64_t sample = 0; sample < buffer_size; ++sample) {
            (copy->begin()+sample)->value = (buffer[channel][sample] - mean) / div;
        }
    };

};

#endif // SHARED_BUFFER_H
