#ifndef ACTIONFORM_H
#define ACTIONFORM_H

#include <QStackedWidget>

#include <thread>

#include "fmt/format.h"

#include "Experiment.h"

namespace Ui {
class ActionForm;
}

class ActionForm : public QStackedWidget
{
    Q_OBJECT

public:
    explicit ActionForm(QWidget* parent, QWidget* plot_widget);
    ~ActionForm();

    void set_connections();
    void activate_window(Experiment* experiment);

private slots:
    void start_experiment();
    void stop_experiment();

private:
    Ui::ActionForm* ui;
    QWidget* widget;
    QWidget* plot_widget;
    Experiment* experiment;
    std::thread experiment_thread;
    bool experiment_in_progress;
};

#endif // ACTIONFORM_H
