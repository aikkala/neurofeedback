#ifndef PLOTTERFORM_H
#define PLOTTERFORM_H

#include <QWidget>
#include "qcustomplot.h"

#include "Module.h"
#include "SharedBuffer.h"
#include "PSDCalculator.h"
#include "programform.h"

namespace Ui {
class PlotterForm;
}

class PlotterForm : public QWidget
{
    Q_OBJECT

public:
    explicit PlotterForm(QWidget* parent);
    ~PlotterForm();

    /**
     * Start plotting experiment baseline
     */
    void start_experiment();

    /**
     * Set experiment baseline
     */
    void set_experiment_baseline(double baseline);

    /**
     * Set amplitude boundaries so we have some idea what kind of audio is output (in "theta-alpha ratio")
     */
    void set_amplitude_boundaries(double lower_boundary, double upper_boundary);

    /**
     * Hide amplitude boundaries while experiment is not on
     */
    void hide_amplitude_boundaries();

    /**
     * Start plotting
     */
    void activate_window(SharedBuffer* psd_buffer, SharedBuffer* track_buffer, PSDCalculator* psd_calculator, ProgramForm* pf);

private:
    /**
     * Set up track plot
     */
    void setup_track_plot(ProgramForm* pf);

    /**
     * Set up PSD plot
     */
    void setup_psd_plot(PSDCalculator* psd_calculator);

private slots:
    void update_plot();

private:
    Ui::PlotterForm *ui;
    QWidget* widget;

    // Emit signals when plot should be updated
    QTimer* signal_emitter;

    // Data buffers
    SharedBuffer* psd_buffer;
    SharedBuffer* track_buffer;
    uint64_t track_size, psd_size;

    // We need mutex to protect replot from setting baselines from another thread
    std::mutex mutex;

    // Which value is being tracked
    std::string tracked;

    // Copy of psd and at buffers
    QCPBars* bars;

    // Indicate whether experiment baseline is plotted
    bool experiment;

};

#endif // PLOTTERFORM_H
