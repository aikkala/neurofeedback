#ifndef SOURCE_H
#define SOURCE_H

/**@file
 *  Source Header File
 *  @author Aleksi Ikkala
 *  @date 2019/02/15
 *  @remarks
 */

#include "SharedBuffer.h"
#include "Module.h"

/**
 *
 */
class Source : public Module {

public:

	/**
	 * Constructor
	 */
    Source() {
		num_channels = 0;
        initialised = false;
    }

	/**
	 * Destructor
	 */
    ~Source() {}

    /**
     * Set buffer
     */
    void set_buffer(SharedBuffer* buffer) {
        this->buffer = buffer;
    }

    /**
     * Set sample rate
     */
    void set_sample_rate(uint64_t sample_rate) {
        this->sample_rate = sample_rate;
    }

	/**
	 * Return number of channels
	 */
	uint64_t get_num_channels() {
		return num_channels;
    }

	/**
     * Set number of channels
	 */
	void set_num_channels(const uint64_t num_channels) {
		this->num_channels = num_channels;
    }

    /**
     * Returns a boolean that indicates whether this source has been correctly initialised
     */
    bool is_initialised() {
        return initialised;
    }

protected:
    uint64_t num_channels, sample_rate;
    SharedBuffer* buffer;
    bool initialised;
};

#endif // SOURCE_H
