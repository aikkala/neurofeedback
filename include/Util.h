#ifndef UTIL_H
#define UTIL_H

/**@file
 *  UtilityFunctions Header File
 *  @author Aleksi Ikkala
 *  @date 2019/04/06
 *  @remarks
 */

#include <vector>
#include <sstream>

/**
 * A bunch of helper functions that might be useful
 */
namespace Util {

/**
 * A function to split strings
 */
inline std::vector<std::string> split(const std::string& str, const char delim)
{
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream token_stream(str);
    while (std::getline(token_stream, token, delim))
    {
        tokens.push_back(token);
    }
    return tokens;
}

}

#endif // UTIL_H
