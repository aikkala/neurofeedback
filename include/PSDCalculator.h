#ifndef PSD_CALCULATOR_H
#define PSD_CALCULATOR_H

/**@file
 *  PSDCalculator Header File
 *  @author Aleksi Ikkala
 *  @date 2019/01/14
 *  @remarks
 */

#include <mutex>
#include <condition_variable>
#include <algorithm>
#include <set>
#include <unordered_map>

// FFT related libraries. Remember, we can parallelise FFTW3 calculations if necessary
#include <complex>
#include <cmath>
#include <fftw3.h>

#include "SharedBuffer.h"
#include "programform.h"
#include "LeastSquares.h"
#include "Module.h"

/**
 *
 */
class PSDCalculator : public Module {

public:

    /**
     *  Contructor
     */
    PSDCalculator(SharedBuffer* data_buffer, SharedBuffer* psd_buffer, SharedBuffer* track_buffer, Recorder* recorder, ProgramForm* pf);

    /**
     *  Destructor
     */
    ~PSDCalculator();

    /**
     * Do necessary initialisations
     */
    void initialise();

    /**
     * Returns initialisation state
     */
    bool is_initialised() {
        return initialised;
    }

    /**
     * Find index corresponding to queried frequency
     */
    uint64_t find_index(double frequency);

    /**
     * Return frequencies
     */
    std::vector<double> get_frequencies();

    /**
     * Return frequencies as strings
     */
    std::vector<std::string> get_frequencies_string();


private:
    /**
     * Starts to listen to samples uploaded into data_buffer, and calculates PSD
     */
    void run();

    /**
     * Set channels
     */
    void set_channels(std::vector<uint64_t> input_channels);

    /**
     * Get Hamming window
     */
    std::vector<double> get_hamming_window(uint64_t size);

    /**
     * Calculate band power in given indices
     */
    double calculate_band_power(std::vector<double>& mean_psd, std::vector<uint64_t>& band_idxs);

    /**
     * Calculate DFT frequencies
     */
    void calculate_frequencies();

    /**
     * Calculate average PSD over channels
     */
    std::vector<double> calculate_mean_psd();

    /**
     * Functions to calculate measures
     */
    double calculate_ta_variance();
    double calculate_ta_ratio();
    double calculate_alpha();
    double calculate_theta();



private:

    SharedBuffer* data_buffer;
    SharedBuffer* psd_buffer;
    SharedBuffer* track_buffer;
    uint64_t sampling_frequency;

    std::string tracked;

    uint64_t buffer_size, psd_size;
    LeastSquares* ls;
    std::vector<double> window;
    uint64_t stft_noverlap, stft_window_size, window_size;
    double norm_coeff;

    // Copy of data_buffer
    double** buffer_copy;

    // Needed for FFT calculations
    double* fft_in;
    std::complex<double>* fft_out;
    fftw_plan fft_plan;
    uint64_t stft_nwindows;

    // Needed for temporal smoothing of tracked value (running average smoothed)
    uint64_t track_window_size; // in samples

    // Needed for PSD calculations
    uint64_t update_interval; // in microseconds
    std::vector<uint64_t> channels;

    // A map of function pointers
    std::unordered_map<std::string, double (PSDCalculator::*)()> calculations;

    // Needed for plotting
    std::vector<double> frequencies;

    // Needed for power calculations
    std::vector<uint64_t> alpha_indices, theta_indices, broadband_indices;

    // A bool to indicate whether initialisation has been successful
    bool initialised = false;

    uint64_t timeout;

    // For writing data into files
    Recorder* recorder;
    std::string log_filename;

};

#endif // PSD_CALCULATOR_H
