#ifndef VIRTUAL_SENSOR_H
#define VIRTUAL_SENSOR_H

/**@file
 *  VirtualSensor Header File
 *  @author Aleksi Ikkala
 *  @date 2019/01/15
 *  @remarks
 */

#include "Source.h"

#include <mutex>
#include <thread>
#include <cmath>
#include <random>


/**
 *
 */
class VirtualSensor : public Source {

public:

    /**
     *  VirtualSensor
     */
    VirtualSensor() {
        set_num_channels(128);
        initialised = true;
    }

    /**
     * Starts to upload samples into data_buffer
     */
    void run() {

        // Make sure buffer and sample_rate have been defined
        if (!buffer || !sample_rate) {
            printf("Buffer or sample rate has been not set, will not run VirtualSensor\n");
            set_status(false);
            return;
        }

        // Update interval in microseconds
        uint64_t update_interval = 1000000/sample_rate;

        std::chrono::steady_clock::time_point session_start = std::chrono::steady_clock::now();

        double frequency1 = 5;
        double frequency2 = 12;
        double frequency3 = 18;
        double coef1 = 10;
        double coef2 = 5;
        double coef3 = 20;
        double noise_enabled = 0;
        uint64_t i = 1;

        // Input data into selected channels only; input strong higher frequency data into other channels so that
        // we see if we're processing non-default channels
        std::set<uint64_t> selected_channels{44, 107, 101, 114, 99, 32, 121, 10, 69, 74, 82, 35, 103, 89};
        //std::set<uint64_t> selected_channels{32, 121, 10, 69, 74, 82};
        std::vector<double> samples(get_num_channels(), 0.0);

        std::default_random_engine generator;
        generator.seed(static_cast<unsigned long>(std::chrono::system_clock::now().time_since_epoch().count()));
        std::normal_distribution<double> distribution(0.0, 25);

        uint64_t timestamp = 0;
        uint64_t next_update_timestamp = 0;
        uint64_t sample_count = 0;

        while (get_status()) {

            // Sleep until it's time to send more samples
            while (true) {
                timestamp = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - session_start).count();
                if (timestamp >= next_update_timestamp) {
                    uint64_t increase_coeff = ((timestamp - next_update_timestamp)/update_interval) + 1;
                    next_update_timestamp += increase_coeff*update_interval;
                    break;
                } else {
                    std::this_thread::sleep_for(std::chrono::microseconds(update_interval/20));
                }
            }
            ++sample_count;

            // Turn first component off and on every X seconds
            if (sample_count > i*2000 && sample_count < (i+1)*2000) {
                coef1 = 0;
            } else if (sample_count == (i+1)*2000) {
                coef1 = 10;
                i += 2;
            }

            // Generate first sine component
            double idx = static_cast<double>(sample_count) / sample_rate;
            double sine1 = coef1*sin(frequency1*2*M_PI*idx);
            double sine2 = coef2*sin(frequency2*2*M_PI*idx);
            double sine3 = coef3*sin(frequency3*2*M_PI*idx);

            // Upload samples to buffer
            for (uint64_t channel = 0; channel < get_num_channels(); ++channel) {
                double value;
                if (selected_channels.count(channel))
                    value = sine1 + sine2 + noise_enabled*distribution(generator);
                else
                    value = sine3;
                buffer->add_sample(channel, value);
            }

            buffer->notify(timestamp);

        }
    }

};

#endif // VIRTUAL_SENSOR_H
