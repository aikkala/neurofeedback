# NeuroFeedback

This repository contains source code for a neurofeedback project. The software estimates signals' frequency information in real-time; this information is used to determine the audio signal (amplitude and amplitude modulation) that is played back to the subject. This software has been developed to work with EGI's NA300/NA400 amplifiers (using AmpServerProSDK-2.1), but it can easily be extended to work with other types of amplifiers as well. Note that in order to build this project with EGI support you need the AmpServerProSDK-2.1.

This software has been tested on Ubuntu 18.04, but all prerequisites and code are cross-platform.

![Example figure of the software](data/program/example_image.png)

## Installation

### Prerequisites

- Note: you probably need to run commands given below with `sudo`

- **FFTW3**: Download source code from [their website](http://www.fftw.org/download.html) (tested with version 3.3.8) and do `./configure && make && make install`

- **PortAudio**: Install alsa development kit: `apt install libasound-dev`, then download source code from [their website](http://www.portaudio.com/download.html) (tested with version 190600_20161030) and do `./configure && make && make install`

- **libsndfile**: Download source code from [their website](http://www.mega-nerd.com/libsndfile/#Download) (tested with version 1.0.28) and do `./configure && make && make install`

- **ZLIB**: `apt install zlib1g-dev` (tested with version 1.2.11)

- **fmt**: Download source code from [their website](http://fmtlib.net/5.3.0/) (tested with version 5.3.0), and do `mkdir build && cd build && cmake -DBUILD_SHARED_LIBS=TRUE .. && make && make install`

- **Qt5**: `apt install qt5-default` (tested with version 5.9.5)

- **Compiler and CMake**: `apt install build-essential` (tested with CMake version 3.10.2 and CXX compiler GNU 7.4.0)

### Build the software

- Clone this repository, `cd` into the cloned directory and do `mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=RELEASE .. && make`

- If you want to build with EGI support you need AmpServerProSDK (version 2.1) 

    - Copy `base` and `sdk` header folders from AmpServerProSDK into [the EGI header folder](https://gitlab.com/aikkala/neurofeedback/tree/master/include/EGI)
    - Create a `lib` folder under project root path, and copy `libAS_Network_Client.a` and `libAS_Network_Server.a` from AmpServerProSDK into the created folder
    - Build with `mkdir build && cd build && cmake -DBUILD_WITH_EGI=ON -DCMAKE_BUILD_TYPE=RELEASE .. && make` 

## Running the software

- Type `./NeuroFeedback` to run the program

- When running the software, one can choose a few options

| Option               | Effect                                                                                    |
|----------------------|-------------------------------------------------------------------------------------------|
| Session name         | Subject's name (or previous recording's name if replaying old data)                       |
| Measure              | Choose which measure is used                                                              |
| Source               | Data source                                                                               |
| Audio                | Toggle audio on / off                                                                     |
| Randomise            | When on, generates randomised audio output (output independent of measured frequency info)|
| Amplitude alteration | When on, increases / decreases audio output's volume depending on measured frequency info |
| Amplitude modulation | When > 0, modulates amplitude of audio output with given frequency (input in Hz)          |

- Records raw (downsampled) data, chosen measure, power spectral distribution (average over used channels), log files ...
- Data will be saved into `${PROJECT_SOURCE_DIR}/data/recorded/[session_name]/[date]/[datetime]`, where `${PROJECT_SOURCE_DIR}` is where you cloned this repository, and `[session_name]` is the given session name. `[date]` is date of recording (in format `YYYY-MM-DD`), and `[datetime]` is current datetime (in format `YYYY-MM-DDTHH:mm:SSZ`). If no session name is given data will be saved into `${PROJECT_SOURCE_DIR}/data/recorded/[datetime]`.
